import sys
from ast import literal_eval
import subprocess
import io
import re

from gen import proto_pb2

a = sys.stdin.read()
#print(a)


m = re.match("^b'(.*)'$", a)
if m is not None:
    d = m.group(1)
    val = literal_eval(a)
    #print(val, type(val))

    msg = proto_pb2.APIMessage()
    msg.ParseFromString(val)

    print("-------------------------")
    print("       APIMessage        ")
    print("-------------------------")
    print(msg)

    if msg.HasField("event"):
        print("-------------------------")
        print("{0:^25s}".format(msg.event.name))
        print("-------------------------")

        type = msg.event.name

        event_obj = getattr(proto_pb2, type)()
        event_obj.ParseFromString(msg.event.payload)

        print(event_obj)

