#!/bin/bash

cd $(dirname "$0")

protoc \
    --proto_path ../ \
    --csharp_out ../csharp/ \
    --python_out ../py/ \
    ../proto_base.proto

cat <<EOF > ../ts/src/proto_base_pb2_json.ts
let proto_base_pb2_json = $(pbjs -t json ../proto_base.proto);
export default proto_base_pb2_json;
EOF
