class HandshakeError(Exception):
    pass


class TransportException(Exception):
    pass


class MethodNotFoundException(Exception):
    pass


class MethodNotImplementedException(Exception):
    pass


class GeneralErrorException(Exception):
    pass


class UserErrorException(Exception):
    def __init__(self, message: str = None):
        super().__init__()
        self.message = message
