import enum
from typing import Any, Callable, Type

from .query import APIQueryFilter


class APIField:
    def __init__(self, name, id, base_model, type_id: str):
        self.name = name
        self.id = id
        self.base_cls = base_model
        self.type_id = type_id

        self.field_query_path = "{}.{}.{}".format(base_model, name, id)

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return "[Field {}]".format(self.name)

    def __repr__(self):
        return 'Field("{}")'.format(self.name)

    def __eq__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$eq", self.field_query_path, other])

    def __ne__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$ne", self.field_query_path, other])

    def __gt__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$gt", self.field_query_path, other])

    def __ge__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$ge", self.field_query_path, other])

    def __lt__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$lt", self.field_query_path, other])

    def __le__(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$le", self.field_query_path, other])

    def in_(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$in", self.field_query_path, other])

    def icontains(self, other: Any) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$icontains", self.field_query_path, other])


class APIFieldEnum(APIField):
    def __init__(self, name, id, base_model, type_id: str, enum_type_creator: Callable[[], Type[enum.Enum]]):
        super().__init__(name, id, base_model, type_id)
        self.enum_type_creator = enum_type_creator


class APIFieldList(APIField):
    pass


class APIFieldMap(APIField):
    pass


class APIFieldModel(APIField):
    pass


class APIFieldInteger(APIField):
    pass


class APIFieldInteger64(APIField):
    pass


class APIFieldString(APIField):
    pass


class APIFieldDecimal(APIField):
    pass


class APIFieldFloat(APIField):
    pass


class APIFieldDate(APIField):
    pass


class APIFieldDateTime(APIField):
    pass


class APIFieldTime(APIField):
    pass


class APIFieldJSON(APIField):
    pass


class APIFieldBSON(APIField):
    pass


class APIFieldUUID(APIField):
    pass


class APIFieldBool(APIField):
    pass


class APIFieldQuery(APIField):
    pass


class APIFieldBinary(APIField):
    pass


__all__ = [
    "APIField",
    "APIFieldEnum",
    "APIFieldList",
    "APIFieldMap",
    "APIFieldModel",
    "APIFieldInteger",
    "APIFieldInteger64",
    "APIFieldString",
    "APIFieldDecimal",
    "APIFieldFloat",
    "APIFieldDate",
    "APIFieldDateTime",
    "APIFieldTime",
    "APIFieldJSON",
    "APIFieldBSON",
    "APIFieldUUID",
    "APIFieldBool",
    "APIFieldQuery",
    "APIFieldBinary",
]
