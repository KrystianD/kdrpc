import logging
from typing import List, Dict, Tuple, Optional, TYPE_CHECKING
from abc import abstractmethod

import shortuuid

from . import exceptions
from . import proto_base_pb2
from .iapi_request_context import IAPIRequestContext

log_request = logging.getLogger("api.request")

if TYPE_CHECKING:
    from google.protobuf.message import Message


class MethodCallException(Exception):
    pass


class APIEndpointMethodMeta:
    def __init__(self,
                 response_type,
                 response_type_proto,
                 params_type_proto,
                 params_from_proto,
                 response_to_proto,
                 default_return):
        self.response_type = response_type
        self.response_type_proto = response_type_proto
        self.params_type_proto = params_type_proto

        self.params_from_proto = params_from_proto
        self.response_to_proto = response_to_proto

        self.default_return = default_return


class APIEndpoint:
    @abstractmethod
    def get_methods_meta(self) -> Dict[str, APIEndpointMethodMeta]:
        pass

    def __init__(self, endpoint_name: str, methods: List[str]) -> None:
        self.endpoint_name = endpoint_name
        self.methods = methods


class IBaseAPIClient:
    def __init__(self) -> None:
        pass

    async def call(self, endpoint_name: str, method_name: str, params_proto: 'Message', timeout: int, no_expire: bool,
                   no_ack: bool):
        # proto_pb2 = self.get_module_proto_pb2()
        # converters = self.get_module_converters()
        #
        # proto_obj = getattr(proto_pb2, "{}{}Params".format(endpoint_name, method_name))()
        # proto_response_type = getattr(proto_pb2, "{}{}Response".format(endpoint_name, method_name))
        #
        # to_proto_converter = getattr(converters, "{}{}Params_to_proto".format(endpoint_name, method_name))
        # from_proto_converter = getattr(converters, "{}{}Response_from_proto".format(endpoint_name, method_name))
        #
        # to_proto_converter(proto_obj, params)

        log_request.info("calling {}.{}(...)".format(endpoint_name, method_name))

        msg = proto_base_pb2.APIMessage()
        msg.request.id = shortuuid.uuid()
        msg.request.endpoint = endpoint_name
        msg.request.method = method_name
        msg.request.no_ack = no_ack
        msg.request.params.Pack(params_proto)
        api_response_bytes = await self.send_request(msg.SerializeToString(),
                                                     endpoint_name=endpoint_name,
                                                     timeout=timeout,
                                                     no_expire=no_expire,
                                                     no_ack=no_ack)
        if no_ack:
            return None
        else:
            msg = proto_base_pb2.APIMessage()
            msg.ParseFromString(api_response_bytes)

            if msg.response.status == 0:
                log_request.info("call to {}.{} succeed".format(endpoint_name, method_name))
                return msg.response.response
            elif msg.response.status == 1:
                raise exceptions.MethodNotFoundException()
            elif msg.response.status == 2:
                raise exceptions.MethodNotImplementedException()
            elif msg.response.status == 3:
                raise exceptions.GeneralErrorException()
            elif msg.response.status == 4:
                log_request.info("user error: {}".format(msg.response.errorMessage))
                raise exceptions.UserErrorException(msg.response.errorMessage)
            else:
                raise Exception

    @abstractmethod
    async def send_request(self, request: bytes, endpoint_name: str, timeout: int, no_expire: bool, no_ack: bool) \
            -> bytes:
        pass


class IAPIServer:
    @abstractmethod
    def register(self, obj: APIEndpoint):
        pass

    @abstractmethod
    async def process_message(self, message: bytes, ctx: IAPIRequestContext) -> Optional[bytes]:
        pass


class BaseAPIServer(IAPIServer):
    def __init__(self) -> None:
        self.endpoints: List[APIEndpoint] = []
        self.endpoint_method_lookup: Dict[Tuple[str, str], APIEndpoint] = {}

    def register(self, obj: APIEndpoint):
        self.endpoints.append(obj)

        for method in obj.methods:
            self.endpoint_method_lookup[(obj.endpoint_name, method)] = obj

    async def executor_wrapper(self, fn, *args, **kwargs):
        return await fn(*args, **kwargs)

    def report_current_error(self):
        pass

    async def process_message(self, message: bytes, ctx: IAPIRequestContext = None) -> Optional[bytes]:
        req = proto_base_pb2.APIMessage()
        req.ParseFromString(message)

        key = (req.request.endpoint, req.request.method)
        endpoint = self.endpoint_method_lookup.get(key)
        if endpoint is None:
            api_msg = proto_base_pb2.APIMessage()
            api_msg.response.id = req.request.id
            api_msg.response.status = 1
            api_msg.response.errorMessage = "Method not found"
            return api_msg.SerializeToString() if not req.request.no_ack else None

        else:
            endpoint_name = endpoint.endpoint_name
            method_name = req.request.method

            meta = endpoint.get_methods_meta()[method_name]

            try:
                log_request.info("{}.{}".format(endpoint_name, method_name))

                packed_params = meta.params_type_proto()
                req.request.params.Unpack(packed_params)
                params = meta.params_from_proto(packed_params)

                fn = getattr(endpoint, method_name)
                if fn is None:
                    api_msg = proto_base_pb2.APIMessage()
                    api_msg.response.id = req.request.id
                    api_msg.response.status = 2
                    api_msg.response.errorMessage = "Method not implemented"
                    return api_msg.SerializeToString() if not req.request.no_ack else None
                else:
                    try:
                        resp = await self.executor_wrapper(fn, ctx, params)
                        if req.request.no_ack:
                            return None
                    except exceptions.UserErrorException as e:
                        log_request.info("user exception occurred while processing command: {}".format(e.message))
                        api_msg = proto_base_pb2.APIMessage()
                        api_msg.response.id = req.request.id
                        api_msg.response.status = 4
                        api_msg.response.errorMessage = e.message
                        return api_msg.SerializeToString() if not req.request.no_ack else None
                    except:
                        log_request.exception("general exception occurred while processing command")
                        api_msg = proto_base_pb2.APIMessage()
                        api_msg.response.id = req.request.id
                        api_msg.response.status = 3
                        api_msg.response.errorMessage = "General exception occurred"
                        return api_msg.SerializeToString() if not req.request.no_ack else None

                    p = meta.response_type_proto()
                    if len(p.DESCRIPTOR.fields) > 0:
                        if hasattr(p, "DEFAULT_RETURN_FIELD_NUMBER"):
                            py_class = meta.response_type()
                            py_class.default_return = resp
                            meta.response_to_proto(p, py_class)
                        else:
                            meta.response_to_proto(p, resp)

                    api_msg = proto_base_pb2.APIMessage()
                    api_msg.response.id = req.request.id
                    api_msg.response.status = 0
                    api_msg.response.errorMessage = ""
                    api_msg.response.response.Pack(p)
                    return api_msg.SerializeToString() if not req.request.no_ack else None

            except:
                log_request.exception("internal exception occurred while processing command")
                self.report_current_error()

                api_msg = proto_base_pb2.APIMessage()
                api_msg.response.id = req.request.id
                api_msg.response.status = 3
                api_msg.response.errorMessage = "General exception occurred"
                return api_msg.SerializeToString() if not req.request.no_ack else None


class BaseEndpointClientStub:
    def __init__(self, endpoint_name: str, client: IBaseAPIClient) -> None:
        self.endpoint_name = endpoint_name
        self.client = client
