from .iapi_request_context import IAPIRequestContext


class ITransportContext(IAPIRequestContext):
    pass


__all__ = ["ITransportContext"]
