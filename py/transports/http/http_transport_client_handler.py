from typing import TYPE_CHECKING

from .http_transport_context import HTTPTransportContext
from ...proto import IAPIServer

if TYPE_CHECKING:
    from aiohttp.web_request import Request


# noinspection PyPep8Naming
class HTTPTransportClientHandler:
    def __init__(self) -> None:
        self.ctx: HTTPTransportContext = None
        self.api_server: IAPIServer = None
        self.handshake_done = False

    def SetAPIServer(self, api_server: IAPIServer):
        self.api_server = api_server

    async def OnMessage(self, request: 'Request'):
        from aiohttp.web_response import Response

        data = await request.read()
        resp = await self.api_server.process_message(data, self.ctx)

        return Response(body=resp,
                        headers={
                            "Access-Control-Allow-Origin": "*"
                        })


__all__ = ["HTTPTransportClientHandler"]
