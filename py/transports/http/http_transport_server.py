from abc import abstractmethod
from asyncio import AbstractEventLoop, CancelledError
from typing import Type, TYPE_CHECKING

from .http_transport_client_handler import HTTPTransportClientHandler
from .http_transport_context import HTTPTransportContext
from ...proto import IAPIServer

if TYPE_CHECKING:
    import aiohttp.web


# noinspection PyPep8Naming
class HTTPTransportServer:
    def __init__(self, ioloop: AbstractEventLoop, api_server: IAPIServer):
        self.ioloop = ioloop
        self.api_server = api_server
        self.web_server: 'aiohttp.web.Application' = None

    async def run(self, port: int):
        import aiohttp

        self.web_server = aiohttp.web.Application(middlewares=[
            # SentryMiddleware(self.sentry),
            # ResponseMiddleware(),
        ])
        self.web_server.router.add_route("POST", '/', self.handler)
        self.web_server.router.add_route("OPTIONS", '/', self.handler_options)

        await self.ioloop.create_server(self.web_server.make_handler(), '0.0.0.0', port)

    async def handler(self, request: 'aiohttp.web.Request'):
        client_handler = self.create_client_handler()
        client_handler.ctx = self.create_context(self, request)
        client_handler.SetAPIServer(self.api_server)

        return await client_handler.OnMessage(request)

    async def handler_options(self, request: 'aiohttp.web.Request'):
        import aiohttp.web

        return aiohttp.web.Response(headers={
            "Access-Control-Allow-Origin": "*"
        })

    @abstractmethod
    def create_context(self, *args) -> HTTPTransportContext:
        pass

    @abstractmethod
    def create_client_handler(self) -> HTTPTransportClientHandler:
        pass


__all__ = ["HTTPTransportServer"]
