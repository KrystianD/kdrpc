from typing import TYPE_CHECKING

from ...transport_context import ITransportContext

if TYPE_CHECKING:
    import aiohttp.web
    from .http_transport_server import HTTPTTransportServer


def get_client_ip(request: 'aiohttp.web.Request', trusted_proxies=('127.0.0.1',)):
    def get_peername_host():
        peername = request.transport.get_extra_info('peername')

        if peername is not None:
            host, port = peername
            return host
        else:
            return None

    client_ip = get_peername_host()

    if trusted_proxies:
        if client_ip not in trusted_proxies:
            raise Exception("not trusted proxy: {}".format(client_ip))

        if "X-Forwarded-For" in request.headers:
            return request.headers["X-Forwarded-For"]

    return client_ip


class HTTPTransportContext(ITransportContext):
    def __init__(self,
                 http_server: 'HTTPTTransportServer',
                 request: 'aiohttp.web.Request'):
        self._http_server = http_server
        self._request = request

        self._client_ip = get_client_ip(request)

    @property
    def http_server(self) -> 'HTTPTTransportServer':
        return self.http_server

    def get_client_ip(self):
        return self._client_ip


__all__ = ["HTTPTransportContext"]
