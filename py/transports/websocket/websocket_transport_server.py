from abc import abstractmethod
from asyncio import AbstractEventLoop, CancelledError
from typing import Type, TYPE_CHECKING

from .websocket_transport_client_handler import WebSocketTransportClientHandler
from .websocket_transport_context import WebSocketTransportContext
from ...proto import IAPIServer

if TYPE_CHECKING:
    import aiohttp.web


# noinspection PyPep8Naming
class WebSocketTransportServer:
    def __init__(self, ioloop: AbstractEventLoop, api_server: IAPIServer):
        self.ioloop = ioloop
        self.api_server = api_server
        self.web_server: 'aiohttp.web.Application' = None

    async def run(self, port: int):
        import aiohttp.web

        self.web_server = aiohttp.web.Application(middlewares=[
            # SentryMiddleware(self.sentry),
            # ResponseMiddleware(),
        ])
        self.web_server.router.add_get('/', self.handler)

        await self.ioloop.create_server(self.web_server.make_handler(), '0.0.0.0', port)

    async def handler(self, request: 'aiohttp.web.Request'):
        from aiohttp import web

        ws = web.WebSocketResponse(compress=True)
        await ws.prepare(request)

        client_handler = self.create_client_handler()
        client_handler.ctx = self.create_context(self, request, ws)
        client_handler.SetAPIServer(self.api_server, ws)

        client_handler.OnOpen()

        try:
            async for packet in ws:
                await client_handler.OnMessage(packet)
        except CancelledError:
            pass
        finally:
            client_handler.OnClose()

        return ws

    @abstractmethod
    def create_context(self, *args) -> WebSocketTransportContext:
        pass

    @abstractmethod
    def create_client_handler(self) -> WebSocketTransportClientHandler:
        pass


__all__ = ["WebSocketTransportServer"]
