from abc import abstractmethod
from typing import Dict, Any, TYPE_CHECKING
import json

if TYPE_CHECKING:
    from aiohttp import WSMessage
    from aiohttp.web_ws import WebSocketResponse

from .websocket_transport_context import WebSocketTransportContext
from ...proto import IAPIServer


# noinspection PyPep8Naming
class WebSocketTransportClientHandler:
    def __init__(self) -> None:
        self.ctx: WebSocketTransportContext = None
        self.api_server: IAPIServer = None
        self.ws: 'WebSocketResponse' = None
        self.handshake_done = False

    def SetAPIServer(self, api_server: IAPIServer, ws: 'WebSocketResponse'):
        self.api_server = api_server
        self.ws = ws

    def OnClose(self):
        print("OnClose")
        pass

    def OnOpen(self):
        print("OnOpen")
        pass

    async def OnMessage(self, packet: 'WSMessage'):
        from aiohttp import WSMsgType

        if packet.type == WSMsgType.TEXT:
            data = json.loads(packet.data)
            resp = await self.ProcessCommand(data)
            await self.ws.send_json(resp)

        elif packet.type == WSMsgType.BINARY:
            resp = await self.ProcessAPIMessage(packet.data)
            await self.ws.send_bytes(resp)

    async def ProcessCommand(self, data: Dict[str, Any]):
        packet_type = data["type"]
        assert packet_type in ("handshake", "ping")

        if packet_type == "handshake":
            res = await self.PerformHandshake(self.ctx, data["data"])
            if res is None or res is True:
                resp_data = "OK"
                self.handshake_done = True
            else:
                resp_data = "FAIL"

            return {
                "type": "handshake_response",
                "result": "ok",
                "data": resp_data,
            }

        if packet_type == "ping":
            return {
                "type": "pong",
            }

    async def ProcessAPIMessage(self, data):
        if not self.handshake_done:
            await self.ws.close(code=3000, message="no handshake")
            return None

        await self.OnBeforeRequest(self.ctx)

        return await self.api_server.process_message(data, self.ctx)

    @abstractmethod
    async def PerformHandshake(self, ctx, handshake_data):
        pass

    async def OnBeforeRequest(self, ctx):
        pass


__all__ = ["WebSocketTransportClientHandler"]
