from typing import Set, Any, Type, Callable
import enum

from .query import APIQueryFilter

from .field import APIField

class Model:
    def __init__(self):
        super().__init__()
        self._existing_fields: Set[APIField] = set()

    def has_field(self, field: APIField):
        return field in self._existing_fields

    def __repr__(self):
        defined_fields = []
        for field in self.DESCRIPTOR.get_fields():
            if self.has_field(field):
                defined_fields.append([field.name, repr(getattr(self, field.name))])

        return "{}({})".format(
                self.__class__.__name__,
                ", ".join("{0[0]}={0[1]}".format(x) for x in defined_fields))

    def print(self):
        print(repr(self))
        # for field in self.DESCRIPTOR.get_fields():
        #     if self.has_field(field):
        #         print("{}={}\n".format(field.name, repr(getattr(self, field.name))))


class Event:
    def __init__(self):
        super().__init__()
        self._existing_fields: Set[APIField] = set()

    def has_field(self, field: APIField):
        return field in self._existing_fields

    def __repr__(self):
        return "{}({})".format(
                self.__class__.__name__,
                ", ".join("{}={}".format(k, repr(getattr(self, k))) for k in self.DESCRIPTOR.get_field_names()))
