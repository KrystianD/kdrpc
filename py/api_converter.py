import datetime
import enum
from typing import List, Type, Dict, Tuple, Any, TYPE_CHECKING

import decimal

from .base import APIField, Model

if TYPE_CHECKING:
    from hashids import Hashids
    import sqlalchemy


class APIConverterContext:
    def __init__(self):
        self._whitelist = set()
        self._blacklist = set()

        self._check_whitelist = lambda field: field in self._whitelist
        self._check_blacklist = lambda field: field not in self._blacklist
        self._check_fn = lambda field: True

    def whitelist(self, field: APIField):
        self._whitelist.add(field)
        self._check_fn = self._check_whitelist

    def blacklist(self, field: APIField):
        self._blacklist.add(field)
        self._check_fn = self._check_blacklist

    def can_convert(self, field: APIField):
        return self._check_fn(field)


class BaseAPIConverterObject:
    pass


class APIConverterKDRPCObject(BaseAPIConverterObject):
    def __init__(self, conv: 'APIConverter', src_type: Type[Model], dst_type: Type[Model]):
        self.conv = conv
        self.src_type = src_type
        self.dst_type = dst_type
        self.mapping: List[Tuple[APIField, APIField, Any, Any]] = []

    def add_mapping(self, src, dst):
        self.mapping.append((src, dst, lambda x: x, lambda x: x))
        return self

    def add_mapping_secret(self, src, dst):
        self.mapping.append((src, dst,
                             lambda x: self.conv.hashids.encode(x) if x is not None else None,
                             lambda x: self.conv.hashids.decode(x)[0] if x is not None else None))
        return self

    def convert(self, ctx: APIConverterContext, obj):
        inst = self.dst_type()
        for src_field, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue

            src_value = getattr(obj, src_field.name)
            dst_value = conv_to(src_value)
            if src_field.type_id != dst_field.type_id:
                dst_value = self.conv.convert(dst_value, ctx=ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst

    def convert_back(self, ctx: APIConverterContext, obj, inst=None):
        if inst is None:
            inst = self.src_type()
        for src_field, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue

            src_value = getattr(obj, src_field.name)
            dst_value = conv_from(src_value)
            if src_field.type_id != dst_field.type_id:
                dst_value = self.conv.convert_back(dst_value, ctx=ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst


class APIConverterSQLAlchemyObject(BaseAPIConverterObject):
    def __init__(self, conv: 'APIConverter', src_type: Any, dst_type: Type[Model]):
        self.conv = conv
        self.src_type = src_type
        self.dst_type = dst_type
        self.mapping: List[Tuple[APIField, str, APIField, Any, Any]] = []

    def add_mapping(self, src, dst):
        self.mapping.append((src, dst, lambda x: x, lambda x: x))
        return self

    # def add_mapping_secret(self, src, dst):
    #     self.mapping.append((src, dst,
    #                          lambda x: self.conv.hashids.encode(x) if x is not None else None,
    #                          lambda x: self.conv.hashids.decode(x)[0] if x is not None else None))
    #     return self

    def convert(self, ctx: APIConverterContext, obj):
        inst = self.dst_type()
        for src_field, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue
            src_value = getattr(obj, src_field.name)
            src_value = conv_to(src_value)
            dst_value = self.conv.convert(src_value, ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst

    def convert_back(self, ctx: APIConverterContext, obj, inst=None):
        if inst is None:
            inst = self.src_type()
        for src_field, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue

            src_value = getattr(obj, src_field.name)
            dst_value = conv_from(src_value)
            dst_value = self.conv.convert_back(dst_value, ctx=ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst


class APIConverterPythonObject(BaseAPIConverterObject):
    def __init__(self, conv: 'APIConverter', src_type: Type, dst_type: Type[Model]):
        self.conv = conv
        self.src_type = src_type
        self.dst_type = dst_type
        self.mapping: List[Tuple[str, APIField, Any, Any]] = []

    def add_mapping(self, field_name, dst):
        self.mapping.append((field_name, dst, lambda x: x, lambda x: x))
        return self

    # def add_mapping_secret(self, src, dst):
    #     self.mapping.append((src, dst,
    #                          lambda x: self.conv.hashids.encode(x) if x is not None else None,
    #                          lambda x: self.conv.hashids.decode(x)[0] if x is not None else None))
    #     return self

    def convert(self, ctx: APIConverterContext, obj):
        inst = self.dst_type()
        for src_field_name, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue
            src_value = getattr(obj, src_field_name)
            src_value = conv_to(src_value)
            dst_value = self.conv.convert(src_value, ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst

    def convert_back(self, ctx: APIConverterContext, obj, inst=None):
        if inst is None:
            inst = self.src_type()
        for src_field_name, dst_field, conv_to, conv_from in self.mapping:
            if not ctx.can_convert(dst_field):
                continue

            src_value = getattr(obj, src_field_name)
            dst_value = conv_from(src_value)
            dst_value = self.conv.convert_back(dst_value, ctx=ctx)
            setattr(inst, dst_field.name, dst_value)
        return inst


class APIConverterEnum(BaseAPIConverterObject):
    def __init__(self, conv: 'APIConverter', src_type: Type[enum.Enum], dst_type: Type[enum.Enum]):
        self.conv = conv
        self.src_type = src_type
        self.dst_type = dst_type

    def convert(self, ctx: APIConverterContext, obj):
        return self.dst_type[obj.name]

    def convert_back(self, ctx: APIConverterContext, obj):
        return self.src_type[obj.name]


class APIConverter:
    def __init__(self, hashids: 'Hashids'):
        self.hashids = hashids
        self.objects = []
        self.src_type_to_object: Dict[Type[Model], APIConverterKDRPCObject] = {}
        self.dst_type_to_object: Dict[Type[Model], APIConverterKDRPCObject] = {}

    def convert(self, obj: Any, ctx: APIConverterContext = APIConverterContext()):
        if obj is None:
            return None
        if isinstance(obj, (int, str, float, datetime.date, datetime.datetime, datetime.time, decimal.Decimal)):
            return obj

        if isinstance(obj, list):
            return [self.convert(x, ctx) for x in obj]

        src_type = type(obj)
        o = self.src_type_to_object[src_type]
        return o.convert(ctx, obj)

    def convert_back(self, obj: Any, ctx: APIConverterContext = APIConverterContext()):
        if obj is None:
            return None
        if isinstance(obj, (int, str, float, datetime.date, datetime.datetime, datetime.time, decimal.Decimal)):
            return obj

        if isinstance(obj, list):
            return [self.convert_back(x, ctx) for x in obj]

        src_type = type(obj)
        o = self.dst_type_to_object[src_type]
        return o.convert_back(ctx, obj)

    def convert_back_into(self, obj: Any, dst_obj: Any, ctx: APIConverterContext = APIConverterContext()):
        if obj is None:
            return
        if isinstance(obj, (int, str, float, datetime.date, datetime.datetime, datetime.time, decimal.Decimal)):
            raise ValueError("cannot convert back into scalar type")

        if isinstance(obj, list):
            raise ValueError("cannot convert back into list")

        src_type = type(obj)
        o = self.dst_type_to_object[src_type]
        return o.convert_back(ctx, obj, dst_obj)

    def add_object(self, src_type: Type[Model], dst_type: Type[Model]) -> APIConverterKDRPCObject:
        o = APIConverterKDRPCObject(self, src_type, dst_type)
        self.src_type_to_object[src_type] = o
        self.dst_type_to_object[dst_type] = o
        self.objects.append(o)
        return o

    def add_sqlalchemy_object(self, src_type: 'sqlalchemy.Column', dst_type: Type[Model]) -> APIConverterSQLAlchemyObject:
        o = APIConverterSQLAlchemyObject(self, src_type, dst_type)
        self.src_type_to_object[src_type] = o
        self.dst_type_to_object[dst_type] = o
        self.objects.append(o)
        return o

    def add_python_object(self, src_type: Type, dst_type: Type[Model]) -> APIConverterPythonObject:
        o = APIConverterPythonObject(self, src_type, dst_type)
        self.src_type_to_object[src_type] = o
        self.dst_type_to_object[dst_type] = o
        self.objects.append(o)
        return o

    def add_enum(self, src_type: Type[enum.Enum], dst_type: Type[enum.Enum]) -> APIConverterEnum:
        o = APIConverterEnum(self, src_type, dst_type)
        self.src_type_to_object[src_type] = o
        self.dst_type_to_object[dst_type] = o
        self.objects.append(o)
        return o


__all__ = [
    "APIConverterContext",
    # "APIConverterObject",
    # "APIConverterSQLAlchemyObject",
    # "APIConverterPythonObject",
    "APIConverter",
]
