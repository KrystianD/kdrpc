import asyncio
import sys

from beeprint import pp
import os
from sqlalchemy.orm import Load, raiseload, load_only, defer, noload

from services.DBApi.lib.converters import DBUser2api

script_dir = os.path.dirname(os.path.realpath(__file__))

sys.path.append(script_dir + "/..")
sys.path.append(script_dir + "/../src")

import interfaces
import models
from common import db
from common.init import common_init
from gen.endpoints import *
from proto import *
from py_user.User import User
from query import QueryPGSQLTraverser

server: 'MyAPIServer' = None
client: 'MyAPIClient' = None

test_user1 = User()
test_user1.full_name = "A"
test_user1.value = Decimal("2.5")


class UsersEndpointImplementation(UsersEndpoint):

    @abstractmethod
async def GetUsersByQuery(self, params: UsersEndpointGetUsersByQueryParams) -> Optional[List[User]]:
        q = db.session.query(models.User)

        tr = QueryPGSQLTraverser()
        tr.add_mapping(interfaces.User.DESCRIPTOR.id, models.User.id)
        tr.add_mapping(interfaces.User.DESCRIPTOR.email, models.User.email)
        tr.add_mapping(interfaces.User.DESCRIPTOR.full_name, models.User.full_name)

        q = q.options(noload(models.User.supplier_info))
        q = q.options(noload(models.User.affiliate_info))
        q = q.options(load_only(models.User.roles))
        q = q.options(load_only(models.User.permissions))
        q = tr.apply_to_query(q)

        print(q)
        users = q.all()

        return [DBUser2api(x, tr) for x in users]


class YRGEndpointImplementation(YRGEndpoint):

    async def GetProjectById(self, params: YRGEndpointGetProjectByIdParams) -> Optional[YRGProject]:
        p = YRGProject()
        p.accounting_status = YRGProjectAccountingStatus.AdditionalChargesInvoiceNotSent
        return p


class MyAPIServer(BaseAPIServer):
    def __init__(self):
        super().__init__()

        self.register(UsersEndpointImplementation())
        self.register(YRGEndpointImplementation())


class MyAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__()

    async def send_request(self, request: bytes, endpoint_name: str, timeout: int, no_ack: bool, no_expire: bool):
        print(len(request), request)  # 403
        resp = await server.process_message(request)
        return resp


async def main():
    common_init(no_apimanager=True, db_debug=True)
    global server, client
    server = MyAPIServer()
    client = MyAPIClient()

    # a = User.DESCRIPTOR.id.in_([1, 3, 4, 5, 1, 2, 3, 4, 5, ]) & (User.DESCRIPTOR.email == "QWE")
    a = User.DESCRIPTOR.id.in_([1, 5]) | User.DESCRIPTOR.email.icontains("kr")
    q = APIQuery(a)
    q.add_field(User.DESCRIPTOR.id)
    q.add_field(User.DESCRIPTOR.email)
    q.add_field(User.DESCRIPTOR.full_name)

    ww = await client.UsersEndpoint.GetUsersByQuery(q)
    pp(ww)

    # ww = await client.YRGEndpoint.GetProjectById(1)
    # pp(ww)


asyncio.get_event_loop().run_until_complete(main())
exit(0)
