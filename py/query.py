import datetime
from typing import Union, Tuple, Dict, TYPE_CHECKING, List

if TYPE_CHECKING:
    import sqlalchemy
    import sqlalchemy.orm
    from .base import APIField


class APIQueryFilter:
    def __init__(self, value=None):
        self.value = value

    def __and__(self, other: 'APIQueryFilter') -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$and", self.value, other.value])

    def __or__(self, other: 'APIQueryFilter') -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$or", self.value, other.value])

    def __invert__(self) -> 'APIQueryFilter':
        return APIQueryFilter.from_condition(["$not", self.value])

    @staticmethod
    def from_condition(tree) -> 'APIQueryFilter':
        q = APIQueryFilter()
        q.value = tree
        return q


class APIQueryFieldsList:
    def __init__(self, value=None):
        if value is None:
            value = []
        self.value = value


class APIQuery:
    def __init__(self, src: Union[APIQueryFilter, APIQueryFieldsList] = None):
        self.filter = APIQueryFilter()
        self.fields_list = APIQueryFieldsList()

        if isinstance(src, APIQueryFilter):
            self.filter = src
        elif isinstance(src, APIQueryFieldsList):
            self.fields_list = src

    def add_field(self, field: 'APIField'):
        self.fields_list.value.append(field.field_query_path)


class QueryPGSQLTraverser:
    def __init__(self):
        self.field_map: Dict[Tuple[str, int], Tuple['APIField', 'sqlalchemy.Column']] = {}

    def add_mapping(self, f: 'APIField', db_field: 'sqlalchemy.Column'):
        self.field_map[(f.base_cls, f.id)] = (f, db_field)

    def create(self, q: APIQuery):
        return QueryPGSQLTraverserInstance(self, q)


class QueryPGSQLTraverserInstance:
    def __init__(self, base: QueryPGSQLTraverser, q: APIQuery):
        self.base = base
        self.q = q

        query_fields = [self._get_field(x) for x in q.fields_list.value or []]
        if len(query_fields) == 0:
            self.query_fields = None
        else:
            self.query_fields = {field: dbcol for field, dbcol in query_fields}

    def build_query(self):
        return self._traverse(self.q.filter.value)

    def apply_to_query(self, sa_query: 'sqlalchemy.orm.Query'):
        import sqlalchemy.orm

        filters = self._traverse(self.q.filter.value)
        if filters is not None:
            sa_query = sa_query.filter(filters)

        if self.query_fields is not None:
            sa_query = sa_query.options(sqlalchemy.orm.raiseload("*"))
            for field, dbcolumn in self.query_fields.items():
                sa_query = sa_query.options(sqlalchemy.orm.load_only(dbcolumn))

        return sa_query

    def has_field(self, f: 'APIField'):
        return self.query_fields is None or f in self.query_fields

    def _get_field(self, name: str) -> Tuple['APIField', 'sqlalchemy.Column']:
        model_name, field_name, field_id = name.split(".")
        field_id = int(field_id)
        return self.base.field_map[(model_name, field_id)]

    def _get_all_fields(self) -> List[Tuple['APIField', 'sqlalchemy.Column']]:
        return list(self.base.field_map.values())

    def _traverse(self, tree):
        import sqlalchemy

        from .field import APIFieldDateTime, APIFieldDate, APIFieldEnum

        TREE_CONDITIONS = {"$and", "$or", "$not"}

        if tree is None:
            return

        op = tree[0]

        if op in TREE_CONDITIONS:
            if op == "$and":
                left = tree[1]
                right = tree[2]
                return sqlalchemy.and_(self._traverse(left), self._traverse(right))
            if op == "$or":
                left = tree[1]
                right = tree[2]
                return sqlalchemy.or_(self._traverse(left), self._traverse(right))
            if op == "$not":
                v = tree[1]
                return sqlalchemy.not_(self._traverse(v))
        else:
            field, dbcolumn = self._get_field(tree[1])

            val = tree[2]

            if isinstance(field, APIFieldDate):
                val = datetime.datetime.fromtimestamp(val / 1000.0).date()

            if isinstance(field, APIFieldDateTime):
                val = datetime.datetime.fromtimestamp(val / 1000.0)

            if isinstance(field, APIFieldEnum):
                val = field.enum_type_creator()(val).name

            if op == "$in":
                return dbcolumn.in_(val)
            if op == "$icontains":
                return dbcolumn.ilike("%{}%".format(val.replace("%", "\%")))
            if op == "$eq":
                return dbcolumn == val
            if op == "$ne":
                return dbcolumn != val
            if op == "$gt":
                return dbcolumn > val
            if op == "$lt":
                return dbcolumn < val
            if op == "$ge":
                return dbcolumn >= val
            if op == "$le":
                return dbcolumn <= val

            raise NotImplementedError("operator {} not implemented".format(op))
