import * as protobuf from 'protobufjs';
import { Observable } from 'rxjs';

import proto_base_pb2_json from './proto_base_pb2_json';
import { APIEndpointMethodMeta, IAPIEndpoint, IAPIRequestContext } from './interfaces';

let proto_base_root = protobuf.Root.fromJSON(proto_base_pb2_json);

function isPromise(obj: any) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

export class BaseAPIServer<T extends IAPIRequestContext> {
  private endpointMethods = new Map<string, APIEndpointMethodMeta>();
  private endpoints = new Map<string, IAPIEndpoint>();

  public RegisterEndpoint(endpoint: IAPIEndpoint) {
    endpoint.MethodsMeta.forEach((value, key) => {
      let methodName = `${endpoint.Name}${key}`;
      this.endpointMethods.set(methodName, value);
      this.endpoints.set(methodName, endpoint);
    });
  }

  public ProcessMessage(request: Uint8Array, ctx: T): Observable<Uint8Array> {
    let APIMessageType = proto_base_root.lookupType("proto_base_pb2.APIMessage");
    let APIResponseType = proto_base_root.lookupType("proto_base_pb2.APIResponse");

    let msg = APIMessageType.decode(request) as any;

    let endpointName = msg["request"]["endpoint"];
    let methodName = msg["request"]["method"];

    let endpointMethodMeta = this.endpointMethods.get(`${endpointName}${methodName}`);
    let endpoint = this.endpoints.get(`${endpointName}${methodName}`) as any;

    let ParamsType = endpointMethodMeta.paramsTypeProto;

    let paramsProto = ParamsType.decode(msg["request"]["params"]["value"]);
    let params = endpointMethodMeta.paramsFromProto(paramsProto);

    let fn = endpoint[methodName];

    if (fn === null)
      throw new Error("Method not found");

    let fnRet = fn(ctx, params);

    if (isPromise(fnRet))
      fnRet = Observable.fromPromise(fnRet);

    return fnRet.flatMap((response: any) => {
      if (endpointMethodMeta.defaultReturn) {
        let r = endpointMethodMeta.responseType();
        r.DefaultReturn = response;
        response = r;
      }

      return Observable.fromPromise(endpointMethodMeta.responseToProto(response)).map((responseProto: any) => {
        let AnyType = proto_base_root.lookupType("google.protobuf.Any");
        let responseUrl = `type.googleapis.com/proto_hub_pb2.${endpointName}${methodName}Response`;
        let packedResponse = AnyType.create({
          "type_url": responseUrl,
          "value": responseProto.$type.encode(responseProto).finish()
        });

        let apiMessage = APIMessageType.create({
          "response": APIResponseType.create({
            "id": msg["request"]["id"],
            "status": 0,
            "response": packedResponse,
          }),
        });

        return apiMessage.$type.encode(apiMessage).finish();
      });
    });
  }
}
