import * as protobuf from 'protobufjs';
import * as Rx from 'rxjs';

import { APIEnum } from "./enum";
import { APIFieldType, copyField } from "./field_types";
import { BasePackageDescriptor } from "./base_descriptor";

export interface IEndpointClientStub {
  Client: IAPIClient;
  Name: string;
}

export interface IAPIClient {
  GenerateMessageId(): string;

  Call(msgId: string, endpointName: string, methodName: string, packedParams: any,
       timeout: number, noExpire: boolean, noAck: boolean): Rx.Observable<any>;

  BeforeRequest(msgId: string, endpointName: string, methodName: string, paramsProto: protobuf.Message<any>, params: APIModel): void;

  AfterRequest(msgId: string, endpointName: string, methodName: string, responseProto: protobuf.Message<any>, response: APIModel): void;

  OnEvent(eventProto: protobuf.Message<any>, event: APIModel): void;
}

export interface IAPIEndpoint {
  Name: string;
  MethodsMeta: Map<string, APIEndpointMethodMeta>;
}

export interface IAPIRequestContext {
}

export class APIEndpointMethodMeta {
  constructor(public responseType: any,
              public paramsTypeProto: any,
              public paramsFromProto: any,
              public responseToProto: any,
              public defaultReturn: boolean) {
  }
}


export class APIField {
  constructor(public name: string,
              public nameCS: string,
              public id: number,
              public baseModel: string,
              public type: APIFieldType) {
  }
}

export interface IAPIModelDescriptor {
  GetFields(): APIField[];

  GetName(): string;

  GetTypeName(): string;
}

function ConvertToObject(fi: any): any {
  let isList = Array.isArray(fi);
  let isMap = fi instanceof Map;
  let isModel = fi instanceof APIModel;
  let isEnum = fi instanceof APIEnum;

  if (isModel) {
    return fi.ToObject();
  }
  else if (isList) {
    let list = [];
    for (let item of fi)
      list.push(ConvertToObject(item));
    return list;
  }
  else if (isMap) {
    let map = new Map<any, any>();
    for (let [key, value] of fi)
      map.set(ConvertToObject(key), ConvertToObject(value));
    return map;
  }
  else if (isEnum) {
    return fi.getName();
  }
  else if (typeof(fi) == typeof(true)) {
    return fi;
  }
  else if (typeof(fi) == typeof(0)) {
    return fi;
  }
  else {
    return fi ? fi.toString() : null;
  }
}

function copy(parent: any, name: string): any {
  let f = eval('function ' + name + '(){};' + name);
  f.prototype = parent;
  return new f();
}

export abstract class APIModel {
  protected existingFields = new Map<APIField, boolean>();

  public abstract GetDescriptor(): IAPIModelDescriptor;

  public HasAnyField(): boolean {
    return this.existingFields.size > 0;
  }

  public HasField(field: APIField): boolean {
    return this.existingFields.has(field);
  }

  public SetField(field: APIField, value: any) {
    (this as any)[field.name] = value;
    this.existingFields.set(field, true);
  }

  public GetField(field: APIField): any {
    return (this as any)[field.name];
  }

  protected CloneInternal() {
    let obj = Object.create(this) as APIModel;

    let d = this.GetDescriptor();
    for (let field of d.GetFields())
      if (this.HasField(field))
        obj.SetField(field, copyField(field.type, this.GetField(field)));

    return obj;
  }

  public ToObject() {
    let d = this.GetDescriptor();
    let f = d.GetFields();

    if (f.length == 1 && f[0].name == "default_return")
      return ConvertToObject(this.GetField(f[0]));

    let obj = {};
    obj = copy(obj, d.GetName());
    for (let field of f)
      if (this.HasField(field))
        (obj as any)[field.nameCS] = ConvertToObject(this.GetField(field));
    return obj;
  }
}

export abstract class APIEvent extends APIModel {
}

