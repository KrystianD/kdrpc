import * as protobuf from 'protobufjs';
import { Observable, ReplaySubject, Subject } from 'rxjs';

import proto_base_pb2_json from './proto_base_pb2_json';
import { APIModel, IAPIClient } from './interfaces';
import { BasePackageDescriptor } from './base_descriptor';
import {
  GeneralErrorException, MethodNotFoundException, MethodNotImplementedException, TransportException, UserErrorException
} from './exceptions';

let proto_base_root = protobuf.Root.fromJSON(proto_base_pb2_json);

let APIMessageType = proto_base_root.lookupType("proto_base_pb2.APIMessage");
let APIRequestType = proto_base_root.lookupType("proto_base_pb2.APIRequest");

enum MessageStatus {
  Success = 0,
  MethodNotFound = 1,
  MethodNotImplemented = 2,
  GeneralError = 3,
  UserError = 4,
}

export abstract class InternalAPIClient implements IAPIClient {
  private listeners = new Map<string, ReplaySubject<protobuf.Message<any>>>();

  private eventListener = new Subject<any>();

  public get EventListener() {
    return this.eventListener;
  }

  protected abstract GetPackageDescriptor(): BasePackageDescriptor;

  public Call(msgId: string, endpointName: string, methodName: string, packedParams: any,
              timeout: number, noExpire: boolean, noAck: boolean): Observable<any> {
    if (timeout === -1)
      timeout = 60000;

    let apiRequestProto = APIMessageType.create({
      "request": APIRequestType.create({
        "id": msgId,
        "endpoint": endpointName,
        "method": methodName,
        "noAck": noAck,
        "params": packedParams,
      }),
    }) as any;

    let s = this.SendRequestInternal(apiRequestProto, msgId, endpointName, methodName, timeout, noExpire, noAck);
    if (!noAck)
      s = s.flatMap((apiResponseMsgProto: protobuf.Message<any>) => {
        switch ((apiResponseMsgProto as any)["response"]["status"]) {
          case MessageStatus.Success:
            return Observable.of((apiResponseMsgProto as any)["response"]["response"]);
          case MessageStatus.MethodNotFound:
            return Observable.throw(new MethodNotFoundException(endpointName, methodName, msgId));
          case MessageStatus.MethodNotImplemented:
            return Observable.throw(new MethodNotImplementedException(endpointName, methodName, msgId));
          case MessageStatus.GeneralError:
            return Observable.throw(new GeneralErrorException(endpointName, methodName, msgId));
          case MessageStatus.UserError:
            let errMsg = (apiResponseMsgProto as any)["response"]["errorMessage"] as string;
            return Observable.throw(new UserErrorException(errMsg, endpointName, methodName, msgId));
        }
      });
    s = s.catch(err => {
      this.OnError(err, msgId, endpointName, methodName);
      return Observable.throw(err);
    });
    return s;
  }

  public CancelListeners(error: string) {
    // console.log("CancelListeners");

    for (let subject of Array.from(this.listeners.values())) {
      subject.error(new Error(error));
      // subject.complete();
    }
    this.listeners.clear();
  }

  public BeforeRequest(msgId: string, endpointName: string, methodName: string, paramsProto: protobuf.Message<any>, params: APIModel): void {
    console.debug(`%c[${msgId}] -> ${endpointName}/${methodName}`, "color: green", params === null ? "(null)" : params.ToObject());
  }

  public AfterRequest(msgId: string, endpointName: string, methodName: string, responseProto: protobuf.Message<any>, response: APIModel): void {
    console.debug(`%c[${msgId}] ${endpointName}/${methodName} ->`, "color: red", response === null ? "(null)" : response.ToObject());
  }

  public OnEvent(eventProto: protobuf.Message<any>, event: APIModel): void {
    console.debug(`%c[Event] ->`, "color: orange", event === null ? "(null)" : event.ToObject());
  }

  protected OnError(error: Error, msgId: string, endpointName: string, methodName: string) {
  }

  public abstract GenerateMessageId(): string;

  private SendRequestInternal(apiRequestProto: protobuf.Message<any>, msgId: string, endpointName: string, methodName: string,
                              timeout: number, noExpire: boolean, noAck: boolean): Observable<protobuf.Message<any>> {
    let apiRequestBytes = apiRequestProto.$type.encode(apiRequestProto).finish();

    if (noAck) {
      return this.SendRequest(apiRequestBytes, msgId, endpointName, methodName, timeout, noExpire, true)
        .map(() => null);
    }
    else {
      return this.SendRequest(apiRequestBytes, msgId, endpointName, methodName, timeout, noExpire, false)
        .flatMap(() => {
          let listener = new ReplaySubject<protobuf.Message<any>>();
          this.listeners.set(msgId, listener);
          return listener;
        })
        .timeout(timeout)
        .finally(() => this.listeners.delete(msgId));
    }
  }

  protected abstract SendRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string,
                                 timeout: number, noExpire: boolean, noAck: boolean): Observable<void>;


  public ProcessMessage(apiResponseBytes: Uint8Array) {
    let apiMessageProto = APIMessageType.decode(apiResponseBytes) as protobuf.Message<any>;

    let protoEvent = (apiMessageProto as any)["event"];
    let protoResponse = (apiMessageProto as any)["response"];

    if (protoResponse !== null) {
      let msgId = protoResponse["id"];
      let listener = this.listeners.get(msgId);
      if (listener) {
        listener.next(apiMessageProto);
        listener.complete();
      }
    }
    else if (protoEvent !== null) {
      let descriptor = this.GetPackageDescriptor();
      let event = descriptor.EventFromMessage(apiMessageProto);
      this.OnEvent(apiMessageProto, event);
      this.eventListener.next(event);
    }
  }

  public ReportError(msgId: string, errorMessage: string) {
    let listener = this.listeners.get(msgId);
    if (listener) {
      listener.error(new TransportException(errorMessage));
      // listener.complete();
    }
  }
}
