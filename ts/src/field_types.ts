import moment from 'moment';
import Decimal from 'decimal.js';
import { List } from 'kdlib';

import { APIEnum } from "./enum";
import { APIModel } from "./interfaces";
import { UUID } from "./types";

export abstract class APIFieldType {
  constructor(public id: string) { }

  abstract copy(value: any): any;
}

export class APITypeInteger extends APIFieldType {
  public copy(value: any) { return value;}
}

export class APITypeInteger64 extends APIFieldType {
  public copy(value: any) { return value;}
}

export class APITypeString extends APIFieldType {
  public copy(value: any) { return value;}
}

export class APITypeDecimal extends APIFieldType {
  public copy(value: any) { return value; }
}

export class APITypeFloat extends APIFieldType {
  public copy(value: any) { return value;}
}

export class APITypeDate extends APIFieldType {
  public copy(value: any) { return moment.utc(value); }
}

export class APITypeDateTime extends APIFieldType {
  public copy(value: any) { return moment.utc(value); }
}

export class APITypeTime extends APIFieldType {
  public copy(value: any) { return moment.utc(value); }
}

export class APITypeJSON extends APIFieldType {
  public copy(value: any) { JSON.parse(JSON.stringify(value)); }
}

export class APITypeBSON extends APIFieldType {
  public copy(value: any) { JSON.parse(JSON.stringify(value)); }
}

export class APITypeBool extends APIFieldType {
  public copy(value: any) { return value;}
}

export class APITypeQuery extends APIFieldType {
  public copy(value: any) { throw new Error("not implemented"); }
}

export class APITypeBinary extends APIFieldType {
  public copy(value: any) { throw new Error("not implemented"); }
}

export class APITypeUUID extends APIFieldType {
  public copy(value: UUID) { return value; }
}

export class APITypeEnum<T extends APIEnum> extends APIFieldType {
  constructor(id: string) {
    super(id);
  }

  public copy(value: any) { return value; }
}

export function copyField<T>(type: APIFieldType, x: T): T {
  if (x === null)
    return null;
  return type.copy(x);
}

export class APITypeModel<T extends APIModel> extends APIFieldType {
  constructor(id: string) {
    super(id);
  }

  public copy(value: APIModel) {
    let obj = Object.create(value) as APIModel;
    let d = value.GetDescriptor();
    for (let field of d.GetFields())
      if (value.HasField(field))
        obj.SetField(field, copyField(field.type, value.GetField(field)));
    return obj;
  }
}

export class APITypeList extends APIFieldType {
  constructor(id: string, public itemType: APIFieldType) {
    super(id);
  }

  public copy(value: List<any>) {
    let newArr = new List<any>();
    for (let item of value)
      newArr.push(copyField(this.itemType, item));
    return newArr;
  }
}

export class APITypeMap extends APIFieldType {
  constructor(id: string, public keyType: APIFieldType, public valueType: APIFieldType) {
    super(id);
  }

  public copy(value: Map<any, any>) {
    let newArr = new Map<any, any>();
    value.forEach((value, key) => {
      newArr.set(copyField(this.keyType, key), copyField(this.valueType, value));
    });
    return newArr;
  }
}
