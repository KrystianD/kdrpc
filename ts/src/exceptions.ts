type PlainObjectOf<T> = { [name: string]: T };

export class HandshakeError extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, HandshakeError.prototype);
  }
}

export class TransportException extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, TransportException.prototype);
  }
}

export class MethodNotFoundException extends Error {
  constructor(public endpoint: string, public method: string, public msgId: string) {
    super(`MethodNotFoundException (${endpoint}/${method})`);
    Object.setPrototypeOf(this, MethodNotFoundException.prototype);
  }

  public getExtraData() {
    return {
      "endpoint": this.endpoint,
      "method": this.method,
      "msgId": this.msgId,
    };
  }
}

export class MethodNotImplementedException extends Error {
  constructor(public endpoint: string, public method: string, public msgId: string) {
    super(`MethodNotImplementedException (${endpoint}/${method})`);
    Object.setPrototypeOf(this, MethodNotImplementedException.prototype);
  }

  public getExtraData() {
    return {
      "endpoint": this.endpoint,
      "method": this.method,
      "msgId": this.msgId,
    };
  }
}

export class GeneralErrorException extends Error {
  constructor(public endpoint: string, public method: string, public msgId: string) {
    super(`GeneralErrorException (${endpoint}/${method})`);
    Object.setPrototypeOf(this, GeneralErrorException.prototype);
  }

  public getExtraData() {
    return {
      "endpoint": this.endpoint,
      "method": this.method,
      "msgId": this.msgId,
    };
  }
}

export class UserErrorException extends Error {
  constructor(public errorMessage: string, public endpoint: string = null, public method: string = null, public msgId: string = null) {
    super((endpoint && method && msgId) ? `UserErrorException (${endpoint}/${method}): ${errorMessage}` : `UserErrorException`);
    Object.setPrototypeOf(this, UserErrorException.prototype);
  }

  public getExtraData() {
    return {
      "endpoint": this.endpoint,
      "method": this.method,
      "msgId": this.msgId,
      "errorMessage": this.errorMessage,
    };
  }
}
