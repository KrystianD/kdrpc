export class APIEnum {
  constructor(protected name: string,
              protected value: number,
              protected display: string,
              protected protoValue: object) {
  }

  public get Name() { return this.name;}

  public get Value() { return this.value;}

  public get Display() { return this.display;}

  public getValue() {
    return this.value;
  }

  public getName() {
    return this.name;
  }

  public getDisplay() {
    return this.display;
  }

  public toString() {
    return this.name;
  }

  public valueOf() {
    return this.name;
  }
}
