import { APIEvent, APIModel } from './interfaces';

export abstract class BasePackageDescriptor {
  public abstract EventFromMessage(msg: any): APIEvent;

  public abstract ModelToBytes(model: APIModel): Uint8Array;

  public abstract ModelFromBytes(data: Uint8Array, modelTypeName: string): APIModel;
}
