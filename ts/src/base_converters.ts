import * as protobuf from 'protobufjs';
import Decimal from 'decimal.js';
import moment, { Moment } from 'moment';
import msgpack from 'msgpack-lite';

import proto_base_pb2_json from './proto_base_pb2_json';

import { APIEnum } from './enum';
import { UUID } from "./types";

let proto_base_root = protobuf.Root.fromJSON(proto_base_pb2_json);

function dateUseUTC() {
  return (window as any).KDRPC_LOCALDATES !== true;
}

export class BaseConverters {
  public static String_to_proto(x: string): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.String");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x;

    return ProtoType.create(obj);
  }

  public static Int32_to_proto(x: number): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Int32");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = Math.floor(x);

    return ProtoType.create(obj);
  }

  public static Int64_to_proto(x: number): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Int64");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = Math.floor(x);

    return ProtoType.create(obj);
  }

  public static Decimal_to_proto(x: any): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Decimal");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x.toString();

    return ProtoType.create(obj);
  }

  public static Float_to_proto(x: number): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Float");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x;

    return ProtoType.create(obj);
  }

  public static Date_to_proto(x: Moment): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Date");

    let obj: any = {};
    if (x === null || x === undefined) {
      obj["isNull"] = true;
    }
    else {
      if (!x.isUTC() && dateUseUTC())
        throw new Error("API dates must be in UTC timezone");
      obj["year"] = x.year();
      obj["month"] = x.month() + 1;
      obj["day"] = x.date();
    }

    return ProtoType.create(obj);
  }

  public static Time_to_proto(x: Moment): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Time");

    let obj: any = {};
    if (x === null || x === undefined) {
      obj["isNull"] = true;
    }
    else {
      if (!x.isUTC() && dateUseUTC())
        throw new Error("API dates must be in UTC timezone");
      obj["hour"] = x.hour();
      obj["minute"] = x.minute();
      obj["second"] = x.second();
      obj["microsecond"] = x.millisecond() * 1000;
      obj["timezone"] = 0;
    }

    return ProtoType.create(obj);
  }

  public static DateTime_to_proto(x: Moment): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.DateTime");

    let obj: any = {};
    if (x === null || x === undefined) {
      obj["isNull"] = true;
    }
    else {
      if (!x.isUTC() && dateUseUTC())
        throw new Error("API dates must be in UTC timezone");
      obj["date"] = BaseConverters.Date_to_proto(x);
      obj["time"] = BaseConverters.Time_to_proto(x);
    }

    return ProtoType.create(obj);
  }

  public static Binary_to_proto(x: Uint8Array): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Binary");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x;

    return ProtoType.create(obj);
  }

  public static Boolean_to_proto(x: boolean): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.Boolean");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x;

    return ProtoType.create(obj);
  }

  public static Enum_to_proto(x: APIEnum, ProtoEnumType: protobuf.Type): any {
    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x.getValue();

    return ProtoEnumType.create(obj);
  }

  public static BSON_to_proto(x: any): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.BSON");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = msgpack.encode(x);

    return ProtoType.create(obj);
  }

  public static JSON_to_proto(x: any): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.JSON");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = JSON.stringify(x);

    return ProtoType.create(obj);
  }

  public static UUID_to_proto(x: UUID): any {
    let ProtoType = proto_base_root.lookupType("proto_base_pb2.UUID");

    let obj: any = {};
    if (x === null || x === undefined)
      obj["isNull"] = true;
    else
      obj["value"] = x.toBytes();

    return ProtoType.create(obj);
  }

  public static String_from_proto(x: any): string {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Int32_from_proto(x: any): number {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Int64_from_proto(x: any): number {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Decimal_from_proto(x: any): any {
    if (x.isNull)
      return null;
    return new Decimal(x.value);
  }

  public static Float_from_proto(x: any): number {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Date_from_proto(x: any): Moment {
    if (x.isNull)
      return null;
    let obj = dateUseUTC() ? moment.utc() : moment();
    obj.year(x["year"]);
    obj.month(x["month"] - 1);
    obj.date(x["day"]);
    return obj;
  }

  public static Time_from_proto(x: any): Moment {
    if (x.isNull)
      return null;
    let obj = dateUseUTC() ? moment.utc() : moment();
    obj.hour(x["hour"]);
    obj.minute(x["minute"]);
    obj.second(x["second"]);
    obj.millisecond(x["microsecond"] / 1000);
    return obj;
  }

  public static DateTime_from_proto(x: any): Moment {
    if (x.isNull)
      return null;
    let obj = dateUseUTC() ? moment.utc() : moment();
    obj.year(x["date"]["year"]);
    obj.month(x["date"]["month"] - 1);
    obj.date(x["date"]["day"]);
    obj.hour(x["time"]["hour"]);
    obj.minute(x["time"]["minute"]);
    obj.second(x["time"]["second"]);
    obj.millisecond(x["time"]["microsecond"] / 1000);
    return obj;
  }

  public static Binary_from_proto(x: any): Uint8Array {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Boolean_from_proto(x: any): boolean {
    if (x.isNull)
      return null;
    return x.value;
  }

  public static Enum_from_proto(x: any, T: any): APIEnum {
    if (x.isNull)
      return null;
    return T.GetById(x.value);
  }

  public static BSON_from_proto(x: any): any {
    if (x.isNull)
      return null;

    return msgpack.decode(x.value);
  }

  public static JSON_from_proto(x: any): any {
    if (x.isNull)
      return null;

    return JSON.parse(x.value);
  }

  public static UUID_from_proto(x: any): UUID {
    if (x.isNull)
      return null;
    return UUID.fromBytes(x.value);
  }
}
