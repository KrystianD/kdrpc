import { Observable, Observer } from 'rxjs';

import { InternalAPIClient } from '../internal_api_client';

import * as http from "http";
import * as https from "https";
import { IncomingMessage } from "http";

export class HTTPNodeTransportClient {
  constructor(private client: InternalAPIClient, private url: string) {
  }


  // KDRPC api processing
  public SendAPIRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string,
                        timeout: number, noExpire: boolean, noAck: boolean): Observable<void> {
    return new Observable((observer: Observer<void>) => {
      const chunks: Buffer[] = [];

      let url = this.url;
      if (url.indexOf("?") === -1)
        url += `?p=${endpointName}/${methodName}`;
      else
        url += `&p=${endpointName}/${methodName}`;

      let client = http;
      if (url.toString().indexOf("https") === 0)
        client = https as any;

      const headers = <any>{
        'Content-Type': 'application/octet-stream',
        'Content-Length': request.length,
      };

      const req = (client as any).request(url, {
          method: "POST",
          timeout: timeout,
          headers: headers,
        },
        (res: IncomingMessage) => {
          res.on('data', (d) => chunks.push(d));
          res.on("end", () => {
            if (res.statusCode === 200) {
              this.client.ProcessMessage(Buffer.concat(chunks));
            }
            else {
              this.client.ReportError(msgId, "no error info");
            }
          });
        }
      );

      req.on("error", () => {
        this.client.ReportError(msgId, "no error info");
      });

      req.write(Buffer.from(request));
      req.end();

      observer.next(null);
      observer.complete();
    });
  }

  private Log(txt: any, ...optionalParams: any[]) {
    // if (this.isDebugMode())
    console.log.apply(null, [`[WS] ${txt}`].concat(...optionalParams));
  }
}
