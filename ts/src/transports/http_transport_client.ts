import { Observable, Observer } from 'rxjs';

import { InternalAPIClient } from '../internal_api_client';


export class HTTPTransportClient {
  constructor(private client: InternalAPIClient, private url: string) {
  }


  // KDRPC api processing
  public SendAPIRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string,
                        timeout: number, noExpire: boolean, noAck: boolean): Observable<void> {
    return Observable.create((observer: Observer<void>) => {
      let url = this.url;
      if (url.indexOf("?") === -1)
        url += `?p=${endpointName}/${methodName}`;
      else
        url += `&p=${endpointName}/${methodName}`;

      let http = new XMLHttpRequest();
      http.responseType = 'arraybuffer';
      http.timeout = timeout;
      http.open("POST", url, true);

      if (!noAck) {
        http.ontimeout = (ev: ProgressEvent) => {
          this.client.ReportError(msgId, "HTTP timeout");
        };
        http.onreadystatechange = (ev: Event) => {
          if (http.readyState === 4) { // DONE
            // console.log(http.readyState, http.status, http.statusText, http.response);
            if (http.status == 200) {
              this.client.ProcessMessage(new Uint8Array(http.response));
            }
            else {
              setTimeout(() => {
                this.client.ReportError(msgId, http.statusText.length === 0 ? "(no error info)" : http.statusText);
              }, 10); // To allow ontimeout to be fired if error was caused by timeout
            }
          }
        };
      }

      http.send(request);

      observer.next(null);
      observer.complete();
    });
  }

  private Log(txt: any, ...optionalParams: any[]) {
    // if (this.isDebugMode())
    console.log.apply(null, [`[WS] ${txt}`].concat(...optionalParams));
  }
}
