import { BehaviorSubject, Observable, Subscription } from 'rxjs';

import { InternalAPIClient } from '../internal_api_client';

export enum APIConnectionState {
  NotConnected = "NotConnected",
  Connecting = "Connecting",
  Reconnecting = "Reconnecting",
  PerformingHandshake = "PerformingHandshake",
  Connected = "Connected",
  UserError = "UserError",
  NotConnectedHandshakeError = "NotConnectedHandshakeError",
  NotConnectedServerError = "NotConnectedServerError",
  NotConnectedForceRefresh = "NotConnectedForceRefresh",
}

export enum APIConnectionHighlevelState {
  NotConnected = "NotConnected",
  Connected = "Connected",
}

class HandshakeError extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, HandshakeError.prototype);
  }
}

function generateRandomString(length: number, alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789") {
  let text = "";
  for (let i = 0; i < length; i++)
    text += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
  return text;
}

function makeWebsocketPath(path: string) {
  if (path.startsWith("ws://") || path.startsWith("wss://"))
    return path;
  else {
    let loc = window.location;
    let pathname;
    if (path.startsWith("/"))
      pathname = path;
    else if (loc.pathname.endsWith("/"))
      pathname = loc.pathname + path;
    else
      pathname = loc.pathname + "/" + path;
    return `${loc.protocol.replace("http", "ws")}//${loc.host}${pathname}`;
  }
}

const MANUAL_DISCONNECTION_CODE = 3000;
const FORCE_REFRESH_CODE = 3001;

export class WebSocketTransportClient {
  private ws: WebSocket;

  private state = new BehaviorSubject<APIConnectionState>(APIConnectionState.NotConnected);
  private hstate = new BehaviorSubject<APIConnectionHighlevelState>(APIConnectionHighlevelState.NotConnected);
  private reconnectSubscription: Subscription;
  private pingSub: Subscription = null;
  private reconnectAttempt = 0;
  private handshakeData: any;

  private sessionKey: string = null;

  private connectTimeout = 5000;
  private handshakeTimeout = 5000;
  private reconnectDelayTimeout = 2000;
  private pingInterval = 2000;
  private pingTimeout = 15000;

  private processHandshakeResponseHandler: (x: any) => void = () => null;
  private onFatalErrorHandler: (x: Error) => void = () => null;

  constructor(private client: InternalAPIClient, private url: string) {
    this.state.subscribe(x => {

      let s: APIConnectionHighlevelState = null;
      switch (x) {
        case APIConnectionState.NotConnected:
        case APIConnectionState.Connecting:
        case APIConnectionState.Reconnecting:
        case APIConnectionState.PerformingHandshake:
        case APIConnectionState.UserError:
        case APIConnectionState.NotConnectedHandshakeError:
        case APIConnectionState.NotConnectedServerError:
        case APIConnectionState.NotConnectedForceRefresh:
          s = APIConnectionHighlevelState.NotConnected;
          break;
        case APIConnectionState.Connected:
          s = APIConnectionHighlevelState.Connected;
          break;
      }

      this.Log(`state changed ${x} (high: ${s})`);

      if (s != this.hstate.getValue())
        this.hstate.next(s);
    });
  }

  public set ProcessHandshakeResponseHandler(c: (x: any) => void) {
    this.processHandshakeResponseHandler = c;
  }

  public set OnFatalErrorHandler(c: (x: Error) => void) {
    this.onFatalErrorHandler = c;
  }

  public get State(): APIConnectionState {
    return this.state.getValue();
  }

  public get HighlevelState(): APIConnectionHighlevelState {
    return this.hstate.getValue();
  }

  public GetStateSubject(): BehaviorSubject<APIConnectionState> {
    return this.state;
  }

  public GetHighlevelStateSubject(): BehaviorSubject<APIConnectionHighlevelState> {
    return this.hstate;
  }

  // deprecated
  public getStateSubject(): BehaviorSubject<APIConnectionState> {
    return this.state;
  }

  // deprecated
  public getHighlevelStateSubject(): BehaviorSubject<APIConnectionHighlevelState> {
    return this.hstate;
  }

  private CloseSocket(code: number) {
    if (this.ws) {
      this.ws.onopen = null;
      this.ws.onmessage = null;
      this.ws.onclose = null;
      this.ws.close(code);
    }
  }

  // Connecting
  public Connect(handshakeData: any) {
    this.sessionKey = generateRandomString(16);
    this.handshakeData = handshakeData;

    this.ConnectInternal();
  }

  public ConnectInternal() {
    let curState = this.state.getValue();

    switch (curState) {
      case APIConnectionState.NotConnected:
      case APIConnectionState.NotConnectedHandshakeError:
        break;
      default:
        this.DisconnectInternal("new connection");
        this.CloseSocket(MANUAL_DISCONNECTION_CODE);
        break;
    }

    this.state.next(APIConnectionState.Connecting);

    this.ws = new WebSocket(makeWebsocketPath(this.url));
    this.ws.binaryType = "arraybuffer";
    this.ws.onopen = (x) => this.OnOpen(x);
    this.ws.onmessage = (event: MessageEvent) => {
      if (typeof(event.data) === "string")
        this.ProcessCommand(event.data);
      else
        this.client.ProcessMessage(new Uint8Array(event.data as ArrayBuffer));
    };
    this.ws.onclose = (x) => this.OnClose(x);

    this.SetupReconnection(this.connectTimeout);
  }

  public Disconnect() {
    this.DisconnectInternal("force disconnect");
    this.CloseSocket(MANUAL_DISCONNECTION_CODE);
    this.state.next(APIConnectionState.NotConnected);
  }

  // WebSocket events
  private OnOpen(event: Event) {
    this.Log("connection established");

    this.reconnectAttempt = 0;

    this.state.next(APIConnectionState.PerformingHandshake);

    this.Log("sending handshake");
    let packet = {
      "type": "handshake",
      "session": this.sessionKey,
      "data": this.handshakeData,
    };
    this.ws.send(JSON.stringify(packet));

    // waiting for handshake to complete
    this.SetupReconnection(this.handshakeTimeout);
  }

  private OnClose(event: CloseEvent) {
    this.Log("onClose", event);

    if (event.code == MANUAL_DISCONNECTION_CODE)
      return;

    if (event.code == FORCE_REFRESH_CODE) {
      this.state.next(APIConnectionState.NotConnectedForceRefresh);
      this.DisconnectInternal("force refresh");
      return;
    }

    if (event.code == 1007) {
      this.state.next(APIConnectionState.NotConnectedServerError);
      this.DisconnectInternal("connection error");
      return;
    }

    this.state.next(APIConnectionState.Reconnecting);
    this.DisconnectInternal("connection closed");

    this.Log(`state: Connecting (attempts: ${this.reconnectAttempt})`);

    this.SetupReconnection(this.reconnectDelayTimeout);

    this.reconnectAttempt++;
  }

  // Connection management
  private DisconnectInternal(error: string) {
    // console.log("DisconnectInternal");
    if (this.reconnectSubscription) {
      this.reconnectSubscription.unsubscribe();
      this.reconnectSubscription = null;
    }

    if (this.pingSub) {
      this.pingSub.unsubscribe();
      this.pingSub = null;
    }

    this.client.CancelListeners(error);
  }

  // Message processing
  private ProcessCommand(data: string) {
    let packet = JSON.parse(data);
    let type = packet["type"];

    if (type != "pong")
      this.Log("new command", data);

    if (type == "handshake_response") {
      let handshakeResponseResult = packet["result"];
      let handshakeResponseData = packet["data"];

      if (this.reconnectSubscription)
        this.reconnectSubscription.unsubscribe();

      if (handshakeResponseResult == "ok") {
        try {
          this.processHandshakeResponseHandler(handshakeResponseData);
          this.SetupPing();
          this.SetupReconnection(this.pingTimeout);
          this.state.next(APIConnectionState.Connected);
        }
        catch (e) {
          this.state.next(APIConnectionState.NotConnectedHandshakeError);
          this.DisconnectInternal("handshake error: " + e);
          this.onFatalErrorHandler(new Error("handshake error: " + e));
          this.CloseSocket(MANUAL_DISCONNECTION_CODE);
        }
      }
      else {
        this.state.next(APIConnectionState.NotConnectedHandshakeError);
        this.DisconnectInternal("handshake server error");
        this.onFatalErrorHandler(new Error("handshake server error"));
        this.CloseSocket(MANUAL_DISCONNECTION_CODE);
      }
    }

    if (type == "pong") {
      this.SetupReconnection(this.pingTimeout); // reset ping timeout timer
    }
  }

  private SetupPing() {
    if (this.pingSub)
      this.pingSub.unsubscribe();

    this.pingSub = Observable.timer(this.pingInterval, this.pingInterval)
      .subscribe(x => {
        let packet = {
          "type": "ping",
        };
        this.ws.send(JSON.stringify(packet));
      });
  }

  private SetupReconnection(interval: number) {
    if (this.reconnectSubscription)
      this.reconnectSubscription.unsubscribe();

    this.reconnectSubscription = Observable.timer(interval)
      .subscribe(x => {
        this.ConnectInternal();
      });
  }

  // KDRPC api processing
  public SendAPIRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string,
                        timeout: number, noExpire: boolean, noAck: boolean) {
    let waitForConnected = this.state
      .switchMap(state => {
        switch (state) {
          case APIConnectionState.NotConnected:
            return Observable.throw("NotConnected");
          case APIConnectionState.NotConnectedServerError:
            return Observable.throw("NotConnectedServerError");
          case APIConnectionState.NotConnectedHandshakeError:
            return Observable.throw("NotConnectedHandshakeError");
          default:
            return Observable.of(state);
        }
      })
      .first(state => state == APIConnectionState.Connected);

    return waitForConnected
      .map(() => {
        // console.log("ws.send", request);
        this.ws.send(request);
      });
  }

  private Log(txt: any, ...optionalParams: any[]) {
    // if (this.isDebugMode())
    console.log.apply(null, [`[WS] ${txt}`].concat(...optionalParams));
  }
}
