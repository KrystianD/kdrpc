import { parse, unparse } from './uuid';

export class UUID {
  private b: Uint8Array;
  private s: string;

  public toString() { return this.s; }

  public toBytes(): Uint8Array {
    return this.b;
  }

  public valueOf() {
    return this.s;
  }

  public static fromBytes(b: Uint8Array): UUID {
    let u = new UUID();
    u.b = b;
    u.s = unparse(b, 0);
    return u;
  }

  public static fromString(s: string): UUID {
    return UUID.fromBytes(parse(s, null, 0));
  }

  public static createRandom(): UUID {
    return UUID.fromBytes(new Uint8Array([
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
      Math.floor(Math.random() * 255),
    ]));
  }
}
