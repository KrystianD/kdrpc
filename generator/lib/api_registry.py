import sys

import glob
import lark
import os
import itertools

from lark import Lark

from .tree_processor import TreeProcessor
from .parser_types import *
from .types import *
from .utils import read_file

script_dir = os.path.dirname(os.path.realpath(__file__))


class APIRegistry:
    def __init__(self):
        self.entries = []
        self.entry_by_path = {}

        # self.models: List[ModelDefinition] = []

        self.types: List[TypeDefinition] = []
        self.packages: List[PackageDefinition] = []
        self.namespaces: List[NamespaceDefinition] = []
        self.endpoints: List[EndpointDefinition] = []

    def register(self, path: Iterable[str], v):
        if path in self.entry_by_path:
            vv = self.entry_by_path[path]
            raise ValueError("type already exists: {}".format(vv))

        self.entries.append(v)
        self.entry_by_path[path] = v

    def register_package(self, package: PackageDefinition):
        self.packages.append(package)

    def find_package(self, name: str):
        for p in self.packages:
            if p.name == name:
                return p

    def register_namespace(self, ns: NamespaceDefinition):
        full_path = ns.get_full_path()
        for current_ns in self.namespaces:
            if current_ns.package == ns.package and current_ns.get_full_path() == full_path:
                current_ns.entries += ns.entries
                for e in ns.entries:
                    e.namespace = current_ns
                return

        self.namespaces.append(ns)

    def register_endpoint(self, ns: EndpointDefinition):
        self.endpoints.append(ns)

    def register_type(self, type_: TypeDefinition):
        if isinstance(type_, ModelDefinition):
            self.register_model(type_)
        elif isinstance(type_, EnumTypeDefinition):
            self.register_enum(type_)
        elif isinstance(type_, SimpleTypeDefinition):
            pass
        elif isinstance(type_, (ListTypeDefinition, MapTypeDefinition)):
            self.types.append(type_)
        else:
            raise ValueError("type {}".format(type(type_)))

    def register_model(self, model: ModelDefinition):
        self.types.append(model)
        self.register(model.get_full_path(), model.type)

    def register_enum(self, enum_def: EnumTypeDefinition):
        self.types.append(enum_def)
        self.register(enum_def.get_full_path(), enum_def)

    def find(self, part: tuple):
        return self.entry_by_path[part]

    def find_nested(self, parent_path: tuple, user_path: tuple, package: PackageDefinition):
        assert isinstance(parent_path, (list, tuple))
        assert isinstance(user_path, (list, tuple))
        parent_path = tuple(parent_path)
        user_path = tuple(user_path)

        v = self.find_nested_internal(parent_path, user_path, package)
        if v is not None:
            return v

        v = self.find_nested_internal(parent_path, (package.name, *user_path), package)
        if v is not None:
            return v

        raise ValueError("type {} not found".format(user_path))

    def find_nested_internal(self, parent_path: tuple, user_path: tuple, package: PackageDefinition):
        for i in range(len(parent_path), -1, -1):
            pp = (*parent_path[:i], *user_path)

            v = self.entry_by_path.get(pp)
            if v is not None:
                return v

    def get_package_types(self, package: PackageDefinition) -> List[TypeDefinition]:
        return [x for x in self.types if x.package == package]

    def get_package_enums(self, package: PackageDefinition) -> List[EnumTypeDefinition]:
        return [x for x in self.enums if x.package == package]

    def get_package_models(self, package: PackageDefinition) -> List[ModelDefinition]:
        return [x for x in self.models if x.package == package]

    def get_package_internal_types(self, package: PackageDefinition) -> List[TypeDefinition]:
        return [x for x in self.internal_types if x.package == package]

    def get_package_endpoints(self, package: PackageDefinition) -> List[EndpointDefinition]:
        return [x for x in self.endpoints if x.package == package]

    def get_package_root_entries(self, package: PackageDefinition) -> List[TypeDefinition]:
        q = itertools.chain(self.models, self.enums, self.namespaces)
        return [x for x in q if x.package == package and x.namespace is None]

    def get_namespace_entries(self, ns: NamespaceDefinition) -> List[TypeDefinition]:
        q = itertools.chain(self.models, self.enums, self.namespaces)
        return [x for x in q if x.namespace == ns]

    @property
    def enums(self) -> List[EnumTypeDefinition]:
        return [x for x in self.types if isinstance(x, EnumTypeDefinition)]

    @property
    def models(self) -> List[ModelDefinition]:
        return [x for x in self.types if isinstance(x, ModelDefinition)]

    @property
    def internal_types(self) -> List[TypeDefinition]:
        return [x for x in self.types if isinstance(x, (ListTypeDefinition, MapTypeDefinition))]

    def add_package(self, schema_dir: str, package_name: str, depends=None):
        if depends is None:
            depends = []
        depends = [self.find_package(x) for x in depends]

        assert all(x is not None for x in depends)

        package = PackageDefinition(package_name, depends)

        files = glob.glob(os.path.join(schema_dir, "**/*.kproto"), recursive=True)

        parser = Lark(read_file(script_dir + "/../schema.bnf"), start='start')

        tree_processor = TreeProcessor(package)

        for f in files:
            proto2 = read_file(f)
            proto2 = re.sub(r"\/\/.*", "", proto2, flags=re.MULTILINE)
            proto2 = re.sub(r"\/\*.*?\*\/", "", proto2, flags=re.DOTALL)
            try:
                tree = parser.parse(proto2)
            except lark.common.ParseError as e:
                print(f"Unable to parse file {f}: " + str(e), file=sys.stderr)
                exit(1)

            tree_processor.process_tree(tree)

        # Register types
        self.register_package(package)

        for ns in tree_processor.namespaces:
            self.register_namespace(ns)

        for type_ in tree_processor.types:
            self.register_type(type_)

        for ep in tree_processor.endpoints:
            self.register_endpoint(ep)

    def finalize(self):

        # Replace placeholders in models and endpoints
        for model in self.types:
            if isinstance(model, ModelDefinition):
                for member in model.members:
                    member.variable_definition.type = \
                        self._fix_placeholders(model.package, member.variable_definition.type, model_def=model)

        # Add NotSet to enums
        for enum_def in self.enums:
            has_default = any(x.id == 0 for x in enum_def.values)
            if not has_default:
                enum_def.values.insert(0, EnumValueDefinition("NotSet", 0, OptionsCollection()))
            enum.values = sorted(enum_def.values, key=lambda x: x.id)

        # Create endpoint method params/response models
        for endpoint in self.endpoints:
            for method in endpoint.methods:
                # Fix placeholders
                for arg in method.arguments:
                    arg.type = self._fix_placeholders(endpoint.package, arg.type)
                for ret in method.return_parameters:
                    ret.type = self._fix_placeholders(endpoint.package, ret.type)

                # Create params/return types
                name = "{}{}Params".format(endpoint.name, method.name)
                method.params_type = ModelDefinition(package=endpoint.package,
                                                     name=name,
                                                     members=[MemberDefinition(x) for x in method.arguments],
                                                     enums=[],
                                                     is_event=False, internal=True)

                name = "{}{}Response".format(endpoint.name, method.name)
                method.return_type = ModelDefinition(package=endpoint.package,
                                                     name=name,
                                                     members=[MemberDefinition(x) for x in method.return_parameters],
                                                     enums=[],
                                                     is_event=False, internal=True)

                self.register_type(method.params_type)
                self.register_type(method.return_type)

    def _fix_placeholders(self, package: PackageDefinition, type_def: TypeDefinition,
                          model_def: ModelDefinition = None) -> TypeDefinition:
        full_path = ()

        def find_type(t):
            for tt in self.internal_types:
                if tt.package.name == t.package.name and tt.id == t.id:
                    return tt
            self.register_type(t)
            return t

        if model_def is not None:
            full_path = model_def.get_full_path()

        if isinstance(type_def, PlaceholderTypeDefinition):
            t = self.find_nested(full_path, type_def.name.split("."), package=package)
            return t

        if isinstance(type_def, PlaceholderListTypeDefinition):
            item_type = self._fix_placeholders(package, type_def.item_type, model_def=model_def)
            t = ListTypeDefinition(package, item_type)
            return find_type(t)

        if isinstance(type_def, PlaceholderMapTypeDefinition):
            key_type = self._fix_placeholders(package, type_def.key_type, model_def=model_def)
            value_type = self._fix_placeholders(package, type_def.value_type, model_def=model_def)
            t = MapTypeDefinition(package, key_type, value_type)
            return find_type(t)

        return type_def
