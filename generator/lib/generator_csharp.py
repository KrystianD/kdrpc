from textwrap import indent

from .generator import *
from .parser_types import *
from .utils import *

script_dir = os.path.dirname(os.path.realpath(__file__))


class CSharpAPIGenerator:
    def __init__(self, generator: APIGenerator, package: PackageDefinition):
        self.generator = generator
        self.package = package
        # self.package.name_cs = package.capitalize()

    def _write_package_file(self, name: str, content: str):
        write_file(os.path.join(self.generator.ts_gen_dir, self.package.name, name), content)

    def generate(self):
        os.makedirs(os.path.join(self.generator.csharp_out_dir, self.package.name), exist_ok=True)
        os.makedirs(os.path.join(self.generator.csharp_user_dir, self.package.name), exist_ok=True)

        self._generate_user_models()
        self._generate_models()
        self._generate_converters()
        self._generate_descriptor()

        self._gen_file(self._generate_endpoints(), "Endpoints.cs", subnamespace="Endpoints")

    def _gen_file(self, content: str, name: str, subnamespace: str = None):
        tpl_classes = open(script_dir + "/../assets/tpl_classes.cs", "rt").read()
        tpl_classes += f"""
using KDRPC;
using static KDRPC.BaseConverters;
using Interfaces{self.package.name_cs};

namespace Interfaces{self.package.name_cs}{("." + subnamespace) if subnamespace is not None else ""} {{
{content}
}}
"""
        write_file(os.path.join(self.generator.csharp_out_dir, self.package.name, name), tpl_classes)

    def _generate_user_models(self):
        user_dir = self.generator.csharp_user_dir
        os.makedirs(user_dir, exist_ok=True)

        for model in self.generator.registry.get_package_models(self.package):
            if model.internal:
                continue

            path_parts = [user_dir, self.package.name]
            if model.namespace is not None:
                path_parts += model.namespace.get_full_path()

            user_model_dir = os.path.join(*path_parts)
            os.makedirs(user_model_dir, exist_ok=True)
            user_model_file = os.path.join(user_model_dir, model.name + ".cs")

            namespace_str = ".".join(model.namespace.get_full_path()) if model.namespace is not None else None

            py_class = f"""
namespace Interfaces{self.package.name_cs}{("." + namespace_str) if namespace_str is not None else ""} {{
    public class {model.name}: {model.type.to_csharp_base()} {{
    }}
}}
""".lstrip()

            if not os.path.exists(user_model_file):
                write_file(user_model_file, py_class)

    def _generate_single_model(self, model: ModelDefinition):
        model_enums = [indent(self._generate_enum(x), "    ") for x in model.enums]

        content = render("""
public class {{ model.base_class_name }}: API{{ base_class }} {
{%- for enum in model_enums %}
    {{ enum }}
{% endfor %}

    public class APIModelDescriptor : IAPIModelDescriptor {
{%- for member in model.members %}
        public readonly {{ member.type.get_csharp_field_type() }} {{ member.name_camelcase }} = new {{ member.type.get_csharp_field_type() }}("{{ member.name }}", "{{ member.name_camelcase }}", {{ member.id }}, "{{ model.base_class_name }}");
{%- else %}
{%- endfor %}

        public override List<IAPIField> GetFields() {
            return new List<IAPIField> { {% for member in model.members %}{{ member.name_camelcase }}, {% endfor %} };
        }
        
        public override string GetName() => "{{ model.name }}";
    }
    
    public static APIModelDescriptor DESCRIPTOR = new APIModelDescriptor();
    public override IAPIModelDescriptor GetDescriptor() { return DESCRIPTOR; }
{% for member in model.members %}
    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private {{ member.type.to_csharp() }} @{{ member.name }};
{%- endfor %}
{% for member in model.members %}
    public {{ member.type.to_csharp() }} {{ member.name_camelcase }} {
        get => @{{ member.name }};
        set { this.@{{ member.name }} = value; existingFields.Add(DESCRIPTOR.{{ member.name_camelcase }}); }
    }
{%- endfor %}
}
""", model=model, model_enums=model_enums, base_class="Event" if model.is_event else "Model")

        return content

    def _generate_enum(self, enum_def: EnumTypeDefinition):
        return render("""
public class {{ enum.name }}: APIEnum<{{ enum.name }}> {
    private static Dictionary<int, {{ enum.name }}> unknownEnums = new Dictionary<int, {{ enum.name }}>();
      
    private {{ enum.name }}(string name, int value, string display): base(name, value, display) { }
    internal static readonly {{ enum.name }} Internal = new {{ enum.name }}(null, -1, null);
    
{%- for value in enum.values %}
    public static readonly {{ enum.name }} {{ value.name }} = new {{ enum.name }}("{{ value.name }}", {{ value.id }}, "{{ value.display }}");
{%- endfor %}
    
    public static readonly Dictionary<int,{{ enum.name }}> IdToEnum = new Dictionary<int,{{ enum.name }}> {
{%- for value in enum.values %}
        { {{ value.id }}, {{ value.name }} },
{%- endfor %}
    };
    
    public static readonly Dictionary<string,{{ enum.name }}> NameToEnum = new Dictionary<string,{{ enum.name }}> {
{%- for value in enum.values %}
        { "{{ value.name }}", {{ value.name }} },
{%- endfor %}
    };
    
    public override {{ enum.name }} GetById(int id)
    {
        {{ enum.name }} en;
        if (!IdToEnum.TryGetValue(id, out en))
          if (!unknownEnums.TryGetValue(id, out en))
            unknownEnums.Add(id, en = new {{ enum.name }}($"Unknown{id}", id, $"<unknown:{id}>"));
        return en;
    }
}""", enum=enum_def)

    def _create_assignment_to_proto(self, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = "{{type.get_csharp_converter_to_proto() }}({{source_var}})"
        elif isinstance(type_def, EnumTypeDefinition):
            t = """
({{ type.get_csharp_proto_path() }})Enum_to_proto<{{ type.get_csharp_proto_path_inner() }}, {{ type.get_csharp_proto_path() }}>({{source_var}})"""
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = "{{type.get_csharp_converter_to_proto() }}({{source_var}})"
        else:
            raise ParseException("Invalid type")

        return render(t, source_var=source_var, type=type_def).lstrip()

    def _generate_creation_from_proto(self, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = "{{type.get_csharp_converter_from_proto() }}({{source_var}})"
        elif isinstance(type_def, EnumTypeDefinition):
            t = "Enum_from_proto({{source_var}}, {{type.to_csharp()}}.Internal)"
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = "{{type.get_csharp_converter_from_proto() }}({{source_var}})"
        else:
            raise ParseException("Invalid type")

        return render(t, source_var=source_var, type=type_def)

    def _generate_member_assignment_to_proto(self, member: MemberDefinition):
        source_var = "x.{name}".format(name=member.name_camelcase)
        t = self._create_assignment_to_proto(source_var=source_var,
                                             type_def=member.type)
        return f'if (x.HasField({member.model.type.to_csharp_base()}.DESCRIPTOR.{member.name_camelcase}))\n' \
               f'    obj.{member.name_camelcase} = {t};'

    def _create_member_assignment_from_proto(self, member: MemberDefinition):
        t = self._generate_creation_from_proto(source_var="x.{name}".format(name=member.name_camelcase),
                                               type_def=member.type)
        return f'if (x.{member.name_camelcase} != null)\n' \
               f'    obj.{member.name_camelcase} = {t};'

    def _create_custom_type_converter(self, custom_type: ModelDefinition):
        members_converters_to_proto = ""
        members_converters_from_proto = ""
        for member in custom_type.members:
            members_converters_to_proto += indent(self._generate_member_assignment_to_proto(member), "        ") + "\n"
            members_converters_from_proto += indent(self._create_member_assignment_from_proto(member), "    ") + "\n"
        return f"""

public static {custom_type.to_proto(True)} {custom_type.get_csharp_converter_to_proto(full_path=False)}({custom_type.to_csharp_base()} x) {{
    var obj = new {custom_type.to_proto(True)}();
    if (x == null)
        obj.IsNull = true;
    else if (!x.HasAnyField())
        obj.IsEmpty = true;
    else {{
{members_converters_to_proto}
    }}
    return obj;
}}

public static {custom_type.to_csharp_public()} {custom_type.get_csharp_converter_from_proto(full_path=False)}({custom_type.to_proto(True)} x) {{
    if (x.IsNull)
        return null;
    var obj = new {custom_type.to_csharp()}();
{members_converters_from_proto}
    return obj;
}}
"""

    def _generate_models(self):
        enums_content = ""
        for type_ in self.generator.registry.get_package_types(self.package):
            # if type_.package.name != self.package.name:
            #     continue

            if isinstance(type_, EnumTypeDefinition):
                if type_.parent_model is None:
                    if type_.namespace is not None:
                        enums_content += f"namespace {type_.namespace.name} {{\n{self._generate_enum(type_)}\n}}\n"
                    else:
                        enums_content += f"{self._generate_enum(type_)}\n"

        enums_content = f"""
using System.Collections.Generic;
using KDRPC;

namespace Interfaces{self.package.name_cs} {{
{enums_content}
}}
"""
        write_file(os.path.join(self.generator.csharp_out_dir, self.package.name, "Enums.cs"), enums_content)

        models_content = ""
        for model in self.generator.registry.get_package_models(self.package):
            # if model.package.name != self.package.name:
            #     continue

            models_content += self._generate_single_model(model)

        tpl_classes = open(script_dir + "/../assets/tpl_classes.cs", "rt").read()
        tpl_classes += f"""
using KDRPC;
using static KDRPC.BaseConverters;
using Interfaces{self.package.name_cs};

namespace Interfaces{self.package.name_cs}.BaseModels {{
{models_content}
}}
"""
        write_file(os.path.join(self.generator.csharp_out_dir, self.package.name, "Types.cs"), tpl_classes)

    def _generate_converters(self):
        convs = "internal static class InternalConverters {"
        for type in self.generator.registry.get_package_types(self.package):
            if isinstance(type, ModelDefinition):
                convs += self._create_custom_type_converter(type)

            if isinstance(type, EnumTypeDefinition):
                convs += ""

            if isinstance(type, ListTypeDefinition):
                convs += render(
                        """

public static {{ type.to_proto(True) }} {{type.get_csharp_converter_to_proto(full_path=False)}}({{ type.to_csharp() }} x) {
    var p = new {{ type.to_proto(True) }}();
    if (x == null)         p.IsNull = true;
    else if (x.Count == 0) p.IsEmpty = true;
    else                   p.Values.AddRange(x.Select(item => {{ proto_item_assignment_to_proto }}));
    return p;
}

public static {{ type.to_csharp() }} {{type.get_csharp_converter_from_proto(full_path=False)}}({{ type.to_proto(True) }} x) {
    if (x.IsNull)          return null;
    else if (x.IsEmpty)    return new {{ type.to_csharp() }}();
    else                   return x.Values.Select(item => {{proto_item_creation_from_proto}}).ToList();
}
""",
                        type=type,
                        proto_item_assignment_to_proto=self._create_assignment_to_proto("item", type.item_type),
                        proto_item_creation_from_proto=self._generate_creation_from_proto("item", type.item_type))

            if isinstance(type, MapTypeDefinition):
                convs += render(
                        """

public static {{ type.to_proto(True) }} {{type.get_csharp_converter_to_proto(full_path=False)}}({{ type.to_csharp() }} x) {
    var p = new {{ type.to_proto(True) }}();
    if (x == null)         p.IsNull = true;
    else if (x.Count == 0) p.IsEmpty = true;
    else 
        p.Values.AddRange(x.Select(item => new {{ type.to_proto(True) }}.Types.MapItem {
            Key = {{proto_key_assignment_to_proto}},
            Value = {{proto_value_assignment_to_proto}} }));
    
    return p;
}

public static {{ type.to_csharp() }} {{type.get_csharp_converter_from_proto(full_path=False)}}({{ type.to_proto(True) }} x) {
    if (x.IsNull)          return null;
    else if (x.IsEmpty)    return new {{ type.to_csharp() }}();
    else
        return x.Values.ToDictionary(
            item => {{proto_key_creation_from_proto}},
            item => {{proto_value_creation_from_proto}});
}
""",
                        type=type,
                        proto_key_assignment_to_proto=self._create_assignment_to_proto("item.Key", type.key_type),
                        proto_value_assignment_to_proto=self._create_assignment_to_proto("item.Value", type.value_type),
                        proto_key_creation_from_proto=self._generate_creation_from_proto("item.Key", type.key_type),
                        proto_value_creation_from_proto=self._generate_creation_from_proto("item.Value",
                                                                                           type.value_type))

        convs += "\n}"

        self._gen_file(convs, "InternalConverters.cs")

    def _generate_descriptor(self):
        models = [x for x in self.generator.registry.get_package_models(self.package)]
        events = [x for x in self.generator.registry.get_package_models(self.package) if x.is_event]
        convs = render("""
public class PackageDescriptor: BasePackageDescriptor {
    public static readonly PackageDescriptor Instance = new PackageDescriptor();
    public override BasePackageDescriptor GetInstance() => Instance;

    private readonly HashSet<System.Type> Events = new HashSet<System.Type>() {
{%- for model in models %}
        typeof({{ model.to_csharp() }}),
{%- endfor %}
    };
    
    private static readonly PBTypeRegistry pbTypeRegistry = PBTypeRegistry.FromMessages(proto_{{ package.name }}_pb2.Proto{{ package.name_camelcase }}Reflection.Descriptor.MessageTypes);
    private static readonly JsonParser pbJsonParser = new JsonParser(new JsonParser.Settings(recursionLimit: 100, typeRegistry: pbTypeRegistry));
    private static readonly JsonFormatter pbJsonSerializer = new JsonFormatter(new JsonFormatter.Settings(formatDefaultValues: false, typeRegistry: pbTypeRegistry));

    public override PBTypeRegistry GetProtobufTypeRegistry() => pbTypeRegistry;
    public override JsonParser GetProtobufJsonParser() => pbJsonParser;
    public override JsonFormatter GetProtobufJsonFormatter() => pbJsonSerializer;
    
    public override HashSet<System.Type> GetEvents() => Events;
    
    public override Google.Protobuf.IMessage EventToProto(APIEvent ev)
    {
      switch (ev) {
{%- for event in events %}
        case {{ event.to_csharp() }} e: return {{ event.get_csharp_converter_to_proto() }}(e);
{%- endfor %}
        default: throw new System.Exception("Invalid event type");
      }
    }
    
    public override Google.Protobuf.IMessage ModelToProto(APIModel ev)
    {
      switch (ev) {
{%- for model in models %}
        case {{ model.to_csharp() }} e: return {{ model.get_csharp_converter_to_proto() }}(e);
{%- endfor %}
        default: throw new System.Exception("Invalid model type");
      }
    }
    
    public override APIEvent EventFromProto(Google.Protobuf.IMessage proto, string eventName)
    {
      switch (eventName) {
    {%- for event in events %}
        case "{{ event.name }}": return {{ event.get_csharp_converter_from_proto() }}(({{ event.type.to_proto(full_path=True) }})proto);
    {%- endfor %}
        default: throw new System.Exception("Invalid event type");
      }
    }
    
    public override APIEvent EventFromMessage(proto_base_pb2.APIMessage msg)
    {
      switch (msg.Event.Name)
      {
    {%- for event in events %}
        case "{{ event.name }}": return {{ event.get_csharp_converter_from_proto() }}(msg.Event.Payload.Unpack<{{ event.type.to_proto(True) }}>());
    {%- endfor %}
        default: throw new System.Exception("Invalid event type");
      }
    }
    
    public override T ModelFromJson<T>(string json)
    {
      if (false) {}
{%- for model in models %}
      else if (typeof(T) == typeof({{ model.to_csharp() }})) {
        return (T)(object){{ model.get_csharp_converter_from_proto() }}({{ model.type.to_proto(full_path=True) }}.Parser.ParseJson(json));
      }
{%- endfor %}
      else {
        throw new System.Exception("Invalid model type");
      }
    }
}
""", package=self.package, events=events, models=models, types=self.generator.registry.get_package_models(self.package))

        self._gen_file(convs, "Descriptor.cs")

    def _generate_endpoints(self):
        t = render("""
public abstract class BaseAPIClient: InternalAPIClient {
{%- for ep in endpoints %}
    public readonly {{ ep.name }}ClientStub {{ ep.name }};
{%- endfor %}

    public BaseAPIClient() {
{%- for ep in endpoints %}
        {{ ep.name }} = new {{ ep.name }}ClientStub(this);
{%- endfor %}
    }
}
""", endpoints=self.generator.registry.get_package_endpoints(self.package))

        for endpoint in self.generator.registry.get_package_endpoints(self.package):
            data = {
                "endpoint_name": endpoint.name,
                "methods": endpoint.methods,
                "package": self.package,
            }
            t += render("""
{# Server call #}
public abstract class {{endpoint_name}}<T>: IAPIEndpoint where T: IAPIRequestContext {
    public string Name => "{{endpoint_name}}";
    public List<string> Methods => new List<string> { {% for method in methods %}"{{method.name}}", {% endfor %} };
    
    public BasePackageDescriptor PackageDescriptor => Interfaces{{ package.name_camelcase }}.PackageDescriptor.Instance; 
    
    public Dictionary<string,APIEndpointMethodMeta> MethodsMeta => new Dictionary<string,APIEndpointMethodMeta> {
{% for method in methods %}
            { "{{ method.name }}", new APIEndpointMethodMeta(
                typeof({{method.return_type.to_csharp_base()}}),
                typeof({{method.params_type.to_proto(full_path=True)}}),
                x => {{method.params_type.get_csharp_converter_from_proto()}}(({{method.params_type.to_proto(full_path=True)}})x),
                x => {{method.return_type.get_csharp_converter_to_proto()}}(({{method.return_type.to_csharp_base()}})x),
                {{ "true" if method.has_single_return() else "false" }},
                (self, ctx, _params) => (({{ endpoint_name }}<T>) self).{{ method.name }}((T) ctx, ({{ method.params_type.to_csharp_base() }}) _params)
            ) },
{%- endfor %}
    };
{% for method in methods %}
    public abstract {{ method.get_return_csharp() }} {{method.name}}(T ctx, {{method.params_type.to_csharp_base()}} _params);
{%- endfor %}
}
{# Client Stub #}
public class {{endpoint_name}}ClientStub: IEndpointClientStub {
    public IAPIClient Client { get; }
    public string Name => "{{endpoint_name}}";
    
    public {{endpoint_name}}ClientStub(IAPIClient client) { Client = client; }

{#- Client - End-user method calls #}
{% for method in methods %}
    public async {{ method.get_return_csharp() }} {{method.name}}(
            {%- for arg in method.arguments -%}
                {{arg.type.to_csharp()}} @{{arg.name}}, 
            {%- endfor -%}
            int timeout=-1, bool noExpire=false, bool noAck=false) {
        var _params = new {{method.params_type.to_csharp_base()}} {
{%- for arg in method.arguments %}
            {{arg.name_camelcase}} = @{{arg.name}},
{%- endfor %}
        };
        
        string msgId = Client.GenerateMessageId();
        var packedParams = Any.Pack({{method.params_type.get_csharp_converter_to_proto()}}(_params));
        
        var packedResp = await Client.Call(
                msgId,
                Name,
                "{{method.name}}",
                packedParams,
                _params,
                timeout: timeout,
                noExpire: noExpire,
                noAck: noAck);
                
        var protoResp = {{method.return_type.to_proto(full_path=True)}}.Parser.ParseFrom(packedResp.Value);
        var resp = {{method.return_type.get_csharp_converter_from_proto()}}(protoResp);
        Client.AfterRequest(msgId, Name, "{{method.name}}", packedParams, _params, protoResp, resp);
                
        {% if method.has_return() -%}
        return noAck ? null : {% if method.has_single_return() %}resp.DefaultReturn{% else %}resp{% endif %};
        {%- endif %}
    }
{% endfor -%}
}
""", **data)

        return t
