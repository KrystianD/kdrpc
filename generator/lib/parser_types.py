from typing import List, Optional, Union, Iterable, Any, Match
import enum
import re
from abc import abstractmethod

from .utils import underscore2camelcase, underscore2camelcase_lowerfirst


class Option:
    def __init__(self, name: str, value: Union[int, str]):
        self.name = name
        self.value = value


class OptionsCollection:
    def __init__(self, options: Iterable[Option] = None):
        self.options = list(options) if options is not None else []

    def get_option(self, name: str, default: Any = None):
        for opt in self.options:
            if opt.name == name:
                return opt.value
        return default


check_stack = set()


class TypeEnum(enum.Enum):
    Integer = "Integer"
    Integer64 = "Integer64"
    String = "String"
    Decimal = "Decimal"
    Float = "Float"
    Date = "Date"
    DateTime = "DateTime"
    Time = "Time"
    Duration = "Duration"
    Binary = "Binary"
    JSON = "JSON"
    BSON = "BSON"
    List = "List"
    Map = "Map"
    Enum = "Enum"
    Bool = "bool"
    Query = "Query"
    UUID = "UUID"

    @staticmethod
    def from_schema(type_name: str):
        return {
            "int": TypeEnum.Integer,
            "int64": TypeEnum.Integer64,
            "string": TypeEnum.String,
            "decimal": TypeEnum.Decimal,
            "float": TypeEnum.Float,
            "date": TypeEnum.Date,
            "datetime": TypeEnum.DateTime,
            "time": TypeEnum.Time,
            "json": TypeEnum.JSON,
            "bson": TypeEnum.BSON,
            "bool": TypeEnum.Bool,
            "query": TypeEnum.Query,
            "binary": TypeEnum.Binary,
            "uuid": TypeEnum.UUID,
        }.get(type_name.lower())


class PackageDefinition:
    def __init__(self, name: str, depends: List['PackageDefinition']):
        self.name = name
        self.name_cs = underscore2camelcase(name)
        self.name_camelcase = underscore2camelcase(name)
        self.depends = depends

    def __repr__(self):
        return f"[Package {self.name}]"


class TypeDefinition:
    def __init__(self, package: PackageDefinition):
        self.package = package

    @property
    @abstractmethod
    def id(self) -> str:
        pass

    @abstractmethod
    def to_proto(self, full_path=False) -> str:
        pass

    @abstractmethod
    def to_python(self) -> str:
        pass

    def to_python_base_type(self) -> str:
        return self.to_python()

    @abstractmethod
    def to_csharp(self) -> str:
        pass

    @abstractmethod
    def is_csharp_nullable(self) -> bool:
        pass

    @abstractmethod
    def to_typescript(self) -> str:
        pass

    @abstractmethod
    def to_typescript_public(self) -> str:
        return self.to_typescript()

    @abstractmethod
    def to_typescript_internal_fix(self, **kwargs) -> str:
        return self.to_typescript()

    def is_typescript_async(self):
        return False

    @property
    def python_type(self):
        return self.to_python()

    @property
    def python_base_type(self):
        return self.to_python_base_type()

    @property
    def get_full_path(self):
        pass

    def get_csharp_converter_to_proto(self, full_path=True):
        name = self.get_converter_to_proto_name()
        return "Interfaces{}.InternalConverters.{}".format(self.package.name_cs, name) if full_path else name

    def get_csharp_converter_from_proto(self, full_path=True):
        name = self.get_converter_from_proto_name()
        return "Interfaces{}.InternalConverters.{}".format(self.package.name_cs, name) if full_path else name

    def get_typescript_converter_to_proto(self, full_path=True):
        name = self.get_converter_to_proto_name()
        return "Converters_{}.{}".format(self.package.name, name) if full_path else name

    def get_typescript_converter_from_proto(self, full_path=True):
        name = self.get_converter_from_proto_name()
        return "Converters_{}.{}".format(self.package.name, name) if full_path else name

    def get_python_converter_to_proto(self, full_path=True):
        name = self.get_converter_to_proto_name()
        return name

    def get_python_converter_from_proto(self, full_path=True):
        name = self.get_converter_from_proto_name()
        return name

    def get_converter_to_proto_name(self):
        return "{}_to_proto".format(self.id)

    def get_converter_from_proto_name(self):
        return "{}_from_proto".format(self.id)

    def get_typescript_type_class(self):
        pass

    def __str__(self):
        return "[Type {}]".format(self.id)


class NamespaceDefinition:
    def __init__(self, package: PackageDefinition, name: str):
        self.package = package
        self.name = name
        self.namespace: NamespaceDefinition = None
        self.entries: List[Any] = []

    def get_full_path(self):
        if self.namespace is None:
            return [self.name]
        else:
            return self.namespace.get_full_path() + [self.name]

    def get_full_path_str(self):
        if self.namespace is None:
            return self.name
        else:
            return "{}__{}".format(self.namespace.get_full_path_str(), self.name)


class PlaceholderTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, name: str):
        super().__init__(package)
        self.name = name

    @property
    def id(self) -> str:
        return self.name

    def to_proto(self, full_path=False) -> str:
        raise Exception

    def to_python(self, escape=False) -> str:
        raise Exception

    def to_csharp(self) -> str:
        raise Exception

    def to_typescript(self) -> str:
        raise Exception

    def is_csharp_nullable(self) -> bool:
        raise Exception


class PlaceholderListTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, item_type: TypeDefinition):
        super().__init__(package)
        self.item_type = item_type

    @property
    def id(self) -> str:
        return self.name

    def to_proto(self, full_path=False) -> str:
        raise Exception

    def to_python(self, escape=False) -> str:
        raise Exception

    def to_csharp(self) -> str:
        raise Exception

    def to_typescript(self) -> str:
        raise Exception

    def is_csharp_nullable(self) -> bool:
        raise Exception


class PlaceholderMapTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, key_type: TypeDefinition, value_type: TypeDefinition):
        super().__init__(package)
        self.key_type = key_type
        self.value_type = value_type

    @property
    def id(self) -> str:
        return self.name

    def to_proto(self, full_path=False) -> str:
        raise Exception

    def to_python(self, escape=False) -> str:
        raise Exception

    def to_csharp(self) -> str:
        raise Exception

    def to_typescript(self) -> str:
        raise Exception

    def is_csharp_nullable(self) -> bool:
        raise Exception


class SimpleTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, type_def: TypeEnum):
        super().__init__(package)
        self.type = type_def

    @property
    def id(self) -> str:
        return self.to_proto()

    def to_proto(self, full_path=False) -> str:
        map_ = {
            TypeEnum.Integer: "Int32",
            TypeEnum.Integer64: "Int64",
            TypeEnum.String: "String",
            TypeEnum.Decimal: "Decimal",
            TypeEnum.Float: "Float",
            TypeEnum.Date: "Date",
            TypeEnum.DateTime: "DateTime",
            TypeEnum.Time: "Time",
            TypeEnum.JSON: "JSON",
            TypeEnum.BSON: "BSON",
            TypeEnum.Bool: "Boolean",
            TypeEnum.Query: "APIQuery",
            TypeEnum.Binary: "Binary",
            TypeEnum.UUID: "UUID",
        }
        if full_path:
            return f"proto_base_pb2.{map_[self.type]}"
        else:
            return map_[self.type]

    @property
    def field_type(self) -> str:
        map_ = {
            TypeEnum.Integer: "",
            TypeEnum.Integer64: "",
            TypeEnum.String: "",
            TypeEnum.Decimal: "",
            TypeEnum.Float: "",
            TypeEnum.Date: "Date",
            TypeEnum.DateTime: "DateTime",
            TypeEnum.Time: "",
            TypeEnum.JSON: "",
            TypeEnum.BSON: "",
            TypeEnum.Bool: "",
            TypeEnum.Query: "",
            TypeEnum.Binary: "",
            TypeEnum.UUID: "",
        }
        return map_[self.type]

    def get_python_field_type(self) -> str:
        map_ = {
            TypeEnum.Integer: "APIFieldInteger",
            TypeEnum.Integer64: "APIFieldInteger64",
            TypeEnum.String: "APIFieldString",
            TypeEnum.Decimal: "APIFieldDecimal",
            TypeEnum.Float: "APIFieldFloat",
            TypeEnum.Date: "APIFieldDate",
            TypeEnum.DateTime: "APIFieldDateTime",
            TypeEnum.Time: "APIFieldTime",
            TypeEnum.JSON: "APIFieldJSON",
            TypeEnum.BSON: "APIFieldBSON",
            TypeEnum.Bool: "APIFieldBool",
            TypeEnum.Query: "APIFieldQuery",
            TypeEnum.Binary: "APIFieldBinary",
            TypeEnum.UUID: "APIFieldUUID",
        }
        return map_[self.type]

    def get_csharp_field_type(self) -> str:
        return self.get_python_field_type()

    def to_python(self, escape=False) -> str:
        map_ = {
            TypeEnum.Integer: "int",
            TypeEnum.Integer64: "int",
            TypeEnum.String: "str",
            TypeEnum.Decimal: "Decimal",
            TypeEnum.Float: "float",
            TypeEnum.Date: "datetime.date",
            TypeEnum.DateTime: "datetime.datetime",
            TypeEnum.Time: "datetime.time",
            TypeEnum.JSON: "JSONType",
            TypeEnum.BSON: "JSONType",
            TypeEnum.Bool: "bool",
            TypeEnum.Query: "APIQuery",
            TypeEnum.Binary: "bytes",
            TypeEnum.UUID: "uuid.UUID",
        }
        return map_.get(self.type)

    def to_python_base_type(self) -> str:
        map_ = {
            TypeEnum.Integer: "int",
            TypeEnum.Integer64: "int",
            TypeEnum.String: "str",
            TypeEnum.Decimal: "Decimal",
            TypeEnum.Float: "float",
            TypeEnum.Date: "datetime.date",
            TypeEnum.DateTime: "datetime.datetime",
            TypeEnum.Time: "datetime.time",
            TypeEnum.JSON: "object",
            TypeEnum.BSON: "object",
            TypeEnum.Bool: "bool",
            TypeEnum.Query: "APIQuery",
            TypeEnum.Binary: "bytes",
            TypeEnum.UUID: "uuid.UUID",
        }
        return map_.get(self.type)

    def to_csharp(self) -> str:
        map_ = {
            TypeEnum.Integer: "int",
            TypeEnum.Integer64: "long",
            TypeEnum.String: "string",
            TypeEnum.Decimal: "decimal",
            TypeEnum.Float: "float",
            TypeEnum.Date: "Date",
            TypeEnum.DateTime: "DateTime",
            TypeEnum.Time: "TimeSpan",
            TypeEnum.JSON: "JToken",
            TypeEnum.BSON: "JToken",
            TypeEnum.Bool: "bool",
            TypeEnum.Query: "APIQuery",
            TypeEnum.Binary: "byte[]",
            TypeEnum.UUID: "Guid",
        }
        v = map_.get(self.type)
        if not self.is_csharp_nullable():
            v = "{}?".format(v)
        return v

    def is_csharp_nullable(self) -> bool:
        map_ = {
            TypeEnum.Integer: False,
            TypeEnum.Integer64: False,
            TypeEnum.String: True,
            TypeEnum.Decimal: False,
            TypeEnum.Float: False,
            TypeEnum.Date: False,
            TypeEnum.DateTime: False,
            TypeEnum.Time: False,
            TypeEnum.JSON: True,
            TypeEnum.BSON: True,
            TypeEnum.Bool: False,
            TypeEnum.Query: True,
            TypeEnum.Binary: True,
            TypeEnum.UUID: False,
        }
        return map_.get(self.type)

    def to_typescript(self) -> str:
        map_ = {
            TypeEnum.Integer: "number",
            TypeEnum.Integer64: "number",
            TypeEnum.String: "string",
            TypeEnum.Decimal: "Decimal",
            TypeEnum.Float: "number",
            TypeEnum.Date: "Moment",
            TypeEnum.DateTime: "Moment",
            TypeEnum.Time: "Moment",
            TypeEnum.JSON: "any",
            TypeEnum.BSON: "any",
            TypeEnum.Bool: "boolean",
            TypeEnum.Query: "APIQuery",
            TypeEnum.Binary: "Uint8Array",
            TypeEnum.UUID: "UUID",
        }
        v = map_.get(self.type)
        return v

    def is_typescript_async(self) -> bool:
        map_ = {
            TypeEnum.Integer: False,
            TypeEnum.Integer64: False,
            TypeEnum.String: False,
            TypeEnum.Decimal: False,
            TypeEnum.Float: False,
            TypeEnum.Date: False,
            TypeEnum.DateTime: False,
            TypeEnum.Time: False,
            TypeEnum.JSON: False,
            TypeEnum.BSON: False,
            TypeEnum.Bool: False,
            TypeEnum.Query: False,
            TypeEnum.Binary: False,
            TypeEnum.UUID: False,
        }
        return map_.get(self.type)

    def get_csharp_converter_to_proto(self, full_path=True):
        return self.get_converter_to_proto_name()

    def get_csharp_converter_from_proto(self, full_path=True):
        return self.get_converter_from_proto_name()

    def get_typescript_converter_to_proto(self, full_path=True):
        return self.get_converter_to_proto_name()

    def get_typescript_converter_from_proto(self, full_path=True):
        return self.get_converter_from_proto_name()

    def get_python_converter_to_proto(self, full_path=True):
        return self.get_converter_to_proto_name()

    def get_python_converter_from_proto(self, full_path=True):
        return self.get_converter_from_proto_name()

    def get_typescript_type_class(self):
        map_ = {
            TypeEnum.Integer: "APITypeInteger",
            TypeEnum.Integer64: "APITypeInteger64",
            TypeEnum.String: "APITypeString",
            TypeEnum.Decimal: "APITypeDecimal",
            TypeEnum.Float: "APITypeFloat",
            TypeEnum.Date: "APITypeDate",
            TypeEnum.DateTime: "APITypeDateTime",
            TypeEnum.Time: "APITypeTime",
            TypeEnum.JSON: "APITypeJSON",
            TypeEnum.BSON: "APITypeBSON",
            TypeEnum.Bool: "APITypeBool",
            TypeEnum.Query: "APITypeQuery",
            TypeEnum.Binary: "APITypeBinary",
            TypeEnum.UUID: "APITypeUUID",
        }
        return f"new {map_[self.type]}(\"{self.to_proto(full_path=True)}\")"


def proto_to_camelcase(x: str):
    def sub(m: Match):
        return m.group(0).title()

    x = re.sub("[A-Z]+", sub, x)

    return x


class EnumValueDefinition:
    def __init__(self, name: str, _id: int, options: OptionsCollection):
        self.name = name
        self.id = _id
        self.options = options

        self.display = self.options.get_option("display", self.name)
        self.name_proto_camelcase = proto_to_camelcase(self.name)

    @property
    def field_type(self) -> str:
        return "Enum"


class EnumTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, name, values: List[EnumValueDefinition]):
        super().__init__(package)
        self.name = name
        self.values = values
        self.parent_model = None  # type: Optional[ModelDefinition]
        self.namespace: NamespaceDefinition = None

    @property
    def id(self) -> str:
        return "__".join(self.get_full_path()) + "__" + self.name

    def to_proto(self, full_path=False) -> str:
        # v = "__".join(self.get_full_path()) + "__" + "{}Value".format(self.name)
        v = self.get_namespace_str("__") + "{}".format(self.name)

        return f"proto_{self.package.name}_pb2.{v}" if full_path else v

    def to_proto_inner(self, full_path=False) -> str:
        # v = "__".join(self.get_full_path()) + "__" + "{}".format(self.name)
        v = self.get_namespace_str("__") + "{}Inner".format(self.name)

        return v

    def get_python_field_type(self) -> str:
        return "APIFieldEnum"

    def get_csharp_field_type(self) -> str:
        return "APIFieldEnum<{}>".format(self.to_csharp())

    def to_python(self, escape=False) -> str:
        return ".".join(self.get_full_path())

    def to_python_base_type(self, escape=False) -> str:
        return "enum.Enum"

    def to_csharp(self) -> str:
        return self.to_csharp_public()

    def to_csharp_public(self) -> str:
        v = self.name
        if self.parent_model is not None:
            v = self.parent_model.to_csharp_public() + "." + v
        else:
            v = "Interfaces" + self.package.name_cs + "." + self.get_namespace_str(".") + v
        return v

    def is_csharp_nullable(self) -> bool:
        return True

    def to_typescript(self) -> str:
        v = "Enums." + self.get_namespace_str(".") + "{}".format(self.name)
        return v

    def to_typescript_internal_fix(self, full_path) -> str:
        return "Enums_{}.{}".format(self.package.name, self.id)

    def to_typescript_public(self) -> str:
        if self.parent_model is None:
            v = self.name
            v = "Interfaces{}.{}{}".format(self.package.name_camelcase, self.get_namespace_str("."), v)
            return v
        else:
            return self.to_typescript_internal_fix(full_path=True)

    def get_display(self, value_id: int):
        value = [x for x in self.values if x[1] == value_id][0]
        return value[2].get_option("display", value[0])

    def get_csharp_proto_path_inner(self):
        return f"proto_{self.package.name}_pb2.{{0}}.Types.{{0}}Inner".format(self.to_proto())

    def get_csharp_proto_path(self):
        return f"proto_{self.package.name}_pb2.{{0}}".format(self.to_proto())

    def get_namespace_parts(self):
        ns_parts = []
        if self.namespace is not None:
            ns_parts = self.namespace.get_full_path()
        if self.parent_model is not None:
            ns_parts = self.parent_model.get_full_path(include_package=False)
        return ns_parts

    def get_namespace_str(self, separator: str):
        ns = self.get_namespace_parts()
        if len(ns) == 0:
            return ""
        else:
            return "".join(x + separator for x in ns)

    def get_full_path(self):
        if self.namespace is not None:
            return (self.package.name, *self.namespace.get_full_path(), self.name)
        if self.parent_model is not None:
            return (*self.parent_model.get_full_path(), self.name)
        return (self.package.name, self.name)

    def get_converter_to_proto_name(self):
        return "{}__{}{}_to_proto".format(self.package.name, self.get_namespace_str("__"), self.name)

    def get_converter_from_proto_name(self):
        return "{}__{}{}_from_proto".format(self.package.name, self.get_namespace_str("__"), self.name)

    def get_typescript_type_class(self):
        return f"new APITypeEnum<{self.to_typescript_internal_fix(full_path=True)}>(\"{self.to_proto(full_path=True)}\")"


class ListTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, item_type: TypeDefinition):
        super().__init__(package)
        self.item_type = item_type

    @property
    def id(self) -> str:
        return "List__{}".format(self.item_type.id)

    def get_python_field_type(self) -> str:
        return "APIFieldList"

    def get_csharp_field_type(self) -> str:
        return "APIFieldList<{}>".format(self.to_csharp())

    def to_proto(self, full_path=False):
        v = "List_{}".format(self.item_type.id)
        return f"proto_{self.package.name}_pb2.{v}" if full_path else v

    def to_python(self):
        return "List[{}]".format(self.item_type.to_python())

    def to_python_base_type(self) -> str:
        return "list"

    def to_csharp(self) -> str:
        return "List<{}>".format(self.item_type.to_csharp())

    def is_csharp_nullable(self) -> bool:
        return True

    def to_typescript(self) -> str:
        return "kdlib.List<{}>".format(self.item_type.to_typescript())

    def to_typescript_public(self) -> str:
        return "kdlib.List<{}>".format(self.item_type.to_typescript_public())

    def is_typescript_async(self) -> bool:
        return self.item_type.is_typescript_async()

    def to_typescript_internal_fix(self, full_path) -> str:
        return "kdlib.List<{}>".format(self.item_type.to_typescript_internal_fix(full_path=full_path))

    def get_typescript_type_class(self):
        return f"new APITypeList(\"{self.to_proto(full_path=True)}\", {self.item_type.get_typescript_type_class()})"


class MapTypeDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, key_type: TypeDefinition, value_type: TypeDefinition):
        super().__init__(package)
        self.key_type = key_type
        self.value_type = value_type

    @property
    def id(self) -> str:
        return "Map__{}__{}".format(self.key_type.id, self.value_type.id)

    def get_python_field_type(self) -> str:
        return "APIFieldMap"

    def get_csharp_field_type(self) -> str:
        return "APIFieldMap<{}>".format(self.to_csharp())

    def to_proto(self, full_path=False):
        v = "Map_{}_{}".format(self.key_type.id, self.value_type.id)
        return f"proto_{self.package.name}_pb2.{v}" if full_path else v

    def to_python(self, escape=False):
        v = "Dict[{}, {}]".format(self.key_type.to_python(), self.value_type.to_python())
        return "'{}'".format(v) if escape else v

    def to_python_base_type(self, escape=False) -> str:
        return "dict"

    def to_csharp(self) -> str:
        return "Dictionary<{},{}>".format(self.key_type.to_csharp(), self.value_type.to_csharp())

    def is_csharp_nullable(self) -> bool:
        return True

    def to_typescript(self) -> str:
        return "Map<{},{}>".format(self.key_type.to_typescript(), self.value_type.to_typescript())

    def to_typescript_public(self) -> str:
        return "Map<{},{}>".format(self.key_type.to_typescript_public(), self.value_type.to_typescript_public())

    def is_typescript_async(self) -> bool:
        return self.key_type.is_typescript_async() or self.value_type.is_typescript_async()

    def to_typescript_internal_fix(self, full_path) -> str:
        return "Map<{},{}>".format(self.key_type.to_typescript_internal_fix(full_path=full_path),
                                   self.value_type.to_typescript_internal_fix(full_path=full_path))

    def get_typescript_type_class(self):
        return f"new APITypeMap(\"{self.to_proto(full_path=True)}\", {self.key_type.get_typescript_type_class()}, {self.value_type.get_typescript_type_class()})"


class VariableDefinition:
    def __init__(self, type_def: TypeDefinition, name: str, id_: int):
        self.type = type_def
        self.name = name
        self.id = id_
        self.name_camelcase = underscore2camelcase(name)
        self.name_camelcase_lowerfirst = underscore2camelcase_lowerfirst(name)


class MemberDefinition:
    def __init__(self, variable_definition: VariableDefinition):
        self.variable_definition = variable_definition
        self.model = None  # type: ModelDefinition

    @property
    def name(self) -> str:
        return self.variable_definition.name

    @property
    def name_camelcase(self) -> str:
        return self.variable_definition.name_camelcase

    @property
    def name_camelcase_lowerfirst(self) -> str:
        return self.variable_definition.name_camelcase_lowerfirst

    @property
    def type(self) -> TypeDefinition:
        return self.variable_definition.type

    @property
    def id(self) -> int:
        return self.variable_definition.id


class ModelDefinition(TypeDefinition):
    def __init__(self, package: PackageDefinition, name: str,
                 members: List[MemberDefinition],
                 enums: List[EnumTypeDefinition],
                 is_event: bool,
                 internal=False):
        super().__init__(package)
        self.name = name
        self.type: 'ModelDefinition' = self
        self.members = members
        self.is_event = is_event
        self.internal = internal
        self.enums = enums
        self.namespace: NamespaceDefinition = None

        for m in members:
            m.model = self

    # ModelDefinition
    def find_enum_by_name(self, name: str) -> EnumTypeDefinition:
        for e in self.enums:
            if e.name == name:
                return e

    @property
    def base_class_name(self):
        return self.type.to_base_class_name()

    def get_full_path(self, include_package=True):
        if self.namespace is None:
            v = (self.name,)
        else:
            v = (*self.namespace.get_full_path(), self.name)

        if include_package:
            v = (self.package.name, *v)
        return v

    # CustomTypeDefinition
    @property
    def id(self) -> str:
        return "__".join(self.get_full_path())

    def get_python_field_type(self) -> str:
        return "APIFieldModel"

    def get_csharp_field_type(self) -> str:
        return "APIFieldModel<{}>".format(self.to_csharp())

    def to_proto(self, full_path=False):
        # if self.internal:  # for Params and Response
        #     v = self.get_namespace_str("__") + "{}".format(self.name)
        # else:
        #     v = self.package.name + "__" + self.get_namespace_str("__") + "{}".format(self.name)
        v = self.get_namespace_str("__") + "{}".format(self.name)
        return f"proto_{self.package.name}_pb2.{v}" if full_path else v

    def to_python(self):
        return self.to_python_public()

    def to_python_public(self):
        v = self.name
        if self.internal:
            v = "types.{}{}".format(self.get_namespace_str("."), v)
        else:
            v = "{}.{}{}".format(self.package.name, self.get_namespace_str("."), v)
        return v

    def to_python_base(self):
        v =  self.get_namespace_str("__") + self.name
        return v

    def to_csharp(self) -> str:
        return self.to_csharp_public()

    def to_base_class_name(self) -> str:
        v = self.name
        if self.internal:
            v = self.get_namespace_str("__") + v
        else:
            v = self.get_namespace_str("__") + "Base" + v
        return v

    def to_csharp_base(self) -> str:
        v = self.name
        if self.internal:
            v = "BaseModels." + self.get_namespace_str("__") + v
        else:
            v = "BaseModels." + self.get_namespace_str("__") + "Base" + v
        return v

    def to_csharp_public(self) -> str:
        v = self.name
        if self.internal:
            v = "BaseModels." + self.get_namespace_str(".") + v
        else:
            v = "Interfaces" + self.package.name_cs + "." + self.get_namespace_str(".") + v
        return v

    def is_csharp_nullable(self) -> bool:
        return True

    def to_typescript(self) -> str:
        return self.to_typescript_public()

    def to_typescript_base(self) -> str:
        v = self.name
        if self.internal:
            v = "BaseModels." + self.get_namespace_str("__") + v
        else:
            v = "BaseModels." + self.get_namespace_str("__") + "Base" + v
        return v

    def to_typescript_public(self) -> str:
        v = self.name
        if self.internal:
            v = "BaseModels.{}{}".format(self.get_namespace_str("."), v)
        else:
            v = "Interfaces{}.{}{}".format(self.package.name_camelcase, self.get_namespace_str("."), v)
        return v

    def is_typescript_async(self) -> bool:
        for m in self.members:
            if m.type in check_stack:
                continue
            check_stack.add(m.type)
            if m.type.is_typescript_async():
                check_stack.remove(m.type)
                return True
        return False

    def get_namespace_parts(self):
        if self.namespace is None:
            return []
        else:
            return self.namespace.get_full_path()

    def get_namespace_str(self, separator: str):
        ns = self.get_namespace_parts()
        if ns is None:
            return ""
        else:
            return "".join(x + separator for x in ns)

    def get_converter_to_proto_name(self):
        return "{}__{}{}_to_proto".format(self.package.name, self.get_namespace_str("__"), self.name)

    def get_converter_from_proto_name(self):
        return "{}__{}{}_from_proto".format(self.package.name, self.get_namespace_str("__"), self.name)

    def get_typescript_type_class(self):
        return f"new APITypeModel<{self.to_typescript_public()}>(\"{self.to_proto(full_path=True)}\")"


class MethodDefinition:
    def __init__(self, name: str, arguments: List[VariableDefinition], return_parameters: List[VariableDefinition]):
        self.name = name
        self.arguments = arguments
        self.return_parameters = return_parameters
        self.endpoint: EndpointDefinition = None
        self.params_type: ModelDefinition = None
        self.return_type: ModelDefinition = None

    def has_single_return(self):
        return len(self.return_parameters) == 1 and self.return_parameters[0].name == "default_return"

    def has_return(self):
        return len(self.return_parameters) > 0

    def get_return_csharp(self):
        t = ""
        if self.has_single_return():
            t = self.return_parameters[0].type.to_csharp()
        elif self.has_return():
            t = "BaseModels.{}{}Response".format(self.endpoint.name, self.name)

        if len(t) > 0:
            return "Task<{}>".format(t)
        else:
            return "Task"

    def get_return_typescript(self):
        t = ""
        if self.has_single_return():
            t = self.return_parameters[0].type.to_typescript_internal_fix(full_path=True)
        elif self.has_return():
            t = "BaseModels.{}{}Response".format(self.endpoint.name, self.name)

        if len(t) > 0:
            return "{}".format(t)
        else:
            return "any"

    def get_return_typescript_rx(self):
        return "Observable<{}>".format(self.get_return_typescript())

    def get_return_typescript_async(self):
        return "Promise<{}>".format(self.get_return_typescript())


class EndpointDefinition:
    def __init__(self, package: PackageDefinition, name, methods: Iterable[MethodDefinition]):
        self.package = package
        self.name = name
        self.methods = list(methods)

        for m in self.methods:
            m.endpoint = self
