import os

from textwrap import indent

from .generator import APIGenerator, ParseException
from .parser_types import *
from .utils import *

script_dir = os.path.dirname(os.path.realpath(__file__))


class TSAPIGenerator:
    def __init__(self, generator: APIGenerator, package: PackageDefinition):
        self.generator = generator
        self.package = package
        # self.package_cs = package.capitalize()

    def _write_package_file(self, name: str, content: str):
        write_file(os.path.join(self.generator.ts_gen_dir, self.package.name, name), content)

    def generate(self):
        os.makedirs(os.path.join(self.generator.ts_gen_dir, self.package.name), exist_ok=True)
        os.makedirs(os.path.join(self.generator.ts_user_dir, self.package.name), exist_ok=True)

        self._generate_user_models()
        self._generate_models()
        self._generate_converters()
        self._generate_endpoints()
        self._generate_descriptor()

    #     def _gen_file(self, content: str, name: str):
    #         tpl_classes = render("""
    # import Decimal from 'decimal.js';
    #
    # import * as kdlib from 'kdlib';
    # import { List } from 'kdlib';
    # import { APIEnum, APIField, APIModel, IAPIModelDescriptor } from 'kdrpc';
    #
    # import * as {{this.package.name}}_user_ts from '../../user_ts/{{this.package.name}}';
    #
    # {% for package in this.package.depends %}
    # import * as {{package.name}}_user_ts from '../../user_ts/{{package.name}}';
    # {% endfor %}
    #
    # {{content}}
    # """, this=self, content=content).lstrip()
    #
    #         self._write_package_file(name, tpl_classes)

    def _generate_user_models(self):
        user_dir = self.generator.ts_user_dir
        os.makedirs(user_dir, exist_ok=True)

        user_models = [x for x in self.generator.registry.get_package_models(self.package) if not x.internal]

        def process_ns(ns: NamespaceDefinition, level=0):
            index_content = f"/* tslint:disable */\nimport {{ Enums_{self.package.name} }} from '{'../' * level}../../gen_ts/{self.package.name}/enums';\n"

            if ns is None:
                entries = self.generator.registry.get_package_root_entries(self.package)
                path_items = []
            else:
                entries = ns.entries
                path_items = ns.get_full_path()

            for entry in entries:

                if isinstance(entry, NamespaceDefinition):
                    index_content += f"import * as {entry.name} from './{entry.name}';\n"
                    index_content += f"export {{ {entry.name} }};\n"
                    process_ns(entry, level=level + 1)
                if isinstance(entry, EnumTypeDefinition):
                    if entry.parent_model is None:
                        index_content += f"class {entry.name} extends {entry.to_typescript_internal_fix(full_path=True)} {{}}\n"
                        index_content += f"export {{ {entry.name} }};\n"
                if isinstance(entry, ModelDefinition):
                    if entry.internal:
                        continue
                    index_content += f"export * from './{entry.name}';\n"

            p = os.path.join(user_dir, self.package.name, *path_items, "index.ts")
            os.makedirs(os.path.dirname(p), exist_ok=True)
            write_file(p, index_content)

        process_ns(None)

        index_content = "/* tslint:disable */\n"
        if len(user_models) == 0:
            index_content += "export module Dummy{}\n"
        for model in user_models:
            path_parts = [user_dir, self.package.name]
            relative_up = ""
            relative_path = ""
            if model.namespace is not None:
                path_parts += model.namespace.get_full_path()
                relative_up += "".join("../" for _ in model.namespace.get_full_path())
                relative_path += "".join(x + "/" for x in model.namespace.get_full_path())

            user_model_dir = os.path.join(*path_parts)
            user_model_file = os.path.join(user_model_dir, model.name + ".ts")

            ts_class = f"""
import {{ BaseModels }} from '{relative_up}../../gen_ts/{self.package.name}/types';

// import {{ Interfaces{self.package.name_camelcase} }} from '{relative_up}../..';

export class {model.name} extends {model.type.to_typescript_base()} {{
}}
""".lstrip()

            if not os.path.exists(user_model_file):
                write_file(user_model_file, ts_class)

        # for package in self.package

    def _generate_single_model(self, model: ModelDefinition):
        content = render("""


export class {{ model.base_class_name }} extends API{{ base_class }} {

{% for enum in model.enums %}
    public static {{ enum.name }} = class extends {{ enum.to_typescript_internal_fix(full_path=False) }} {};
{% endfor %}

    static ModelDescriptor = class implements IAPIModelDescriptor {
{%- for member in model.members %}
        public {{ member.name_camelcase }} = new APIField("{{ member.name }}", "{{ member.name_camelcase }}", {{ member.id }}, "{{ model.base_class_name }}", {{ member.type.get_typescript_type_class() }});
{%- endfor %}

        public GetFields(): APIField[] {
            return [ {% for member in model.members %}this.{{ member.name_camelcase }}, {% endfor %} ];
        }
        
        public GetName() { return "{{ model.name }}"; }
        
        public GetTypeName() { return "{{ model.type.to_proto(full_path=True) }}"; }
    };
    
    public static DESCRIPTOR = new {{ model.base_class_name }}.ModelDescriptor();
    public GetDescriptor() { return {{ model.base_class_name }}.DESCRIPTOR; }
    public Clone(): {{ model.type.to_typescript_internal_fix(full_path=False) }} { return this.CloneInternal() as {{ model.type.to_typescript_internal_fix(full_path=False) }}; }

{% for member in model.members %}
    private {{ member.name }}: {{ member.type.to_typescript_public() }};
{%- endfor %}

{% for member in model.members %}
    public get {{ member.name_camelcase }}(): {{ member.type.to_typescript_public() }} { return this.{{ member.name }}; }
    public set {{ member.name_camelcase }}(value: {{ member.type.to_typescript_public() }}) {
        this.{{ member.name }} = value;
        this.existingFields.set({{ model.base_class_name }}.DESCRIPTOR.{{ member.name_camelcase }}, true);
    }
{%- endfor %}

    public static Create(data: {
{%- for member in model.members %}
        {{ member.name_camelcase }}?: {{ member.type.to_typescript_public() }},
{%- endfor %}
    }): {{ model.type.to_typescript_internal_fix(full_path=False) }} {
        const obj = new this();
{%- for member in model.members %}
        if (data.{{ member.name_camelcase }} !== undefined)
            obj.{{ member.name_camelcase }} = data.{{ member.name_camelcase }}; 
{%- endfor %}
        return obj as {{ model.type.to_typescript_internal_fix(full_path=False) }};
    }
}
""", model=model, _generate_enum=self._generate_enum, indent=indent, base_class="Event" if model.is_event else "Model")

        return content

    def _generate_enum(self, enum_def: EnumTypeDefinition):
        return render("""
        
export class {{ enum.id }} extends APIEnum {
  public constructor(name: string, value: number, display: string, protoValue: object) {
    super(name, value, display, protoValue);
  }

{%- for value in enum.values %}
{%- if value.id != 0 %}
  public static {{ value.name }} = new {{ enum.id }}("{{ value.name }}", {{ value.id }}, "{{ value.display }}", null);
{%- endif %}
{%- endfor %}
  
  private static members: {{ enum.id }}[] = [
{%- for value in enum.values %}
    {%- if value.id != 0 %}
    {{ enum.id }}.{{ value.name }},
    {%- endif %}
{%- endfor %}
  ];
  
  private static idToEnumMap = new Map<number,{{ enum.id }}>([
{%- for value in enum.values %}
    {%- if value.id != 0 %}
    [ {{ value.id }}, {{ enum.id }}.{{ value.name }} ],
    {%- endif %}
{%- endfor %}
  ]);
    
  private static map = {{ enum.id }}.members.map(x => <any>{ value: x, label: x.display });
  private static mapWithNull = [<any>{ value: null, label: "not set" }].concat({{ enum.id }}.map);

  public static GetMembers(): {{ enum.id }}[] { return {{ enum.id }}.members; }
  public static GetById(id: number): {{ enum.id }} { return {{ enum.id }}.idToEnumMap.get(id); }

  public static getValueLabelMap(includeNull = false): any[] {
    return includeNull ? {{ enum.id }}.mapWithNull : {{ enum.id }}.map;
  }
  
  public static GetValueLabelMap(includeNull = false) { return {{ enum.id }}.getValueLabelMap(includeNull); }
}
""", enum=enum_def).lstrip()

    def _create_assignment_to_proto(self, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = 'BaseConverters.{{type.to_proto()}}_to_proto({{source_var}})'
        elif isinstance(type_def, EnumTypeDefinition):
            t = """BaseConverters.Enum_to_proto({{source_var}}, proto_root.lookupType("{{type.to_proto(full_path=True)}}"))"""
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = f"{type_def.get_typescript_converter_to_proto()}({source_var})"
        else:
            raise ParseException("Invalid type")

        if type_def.is_typescript_async():
            t = "await " + t

        return render(t, source_var=source_var, type=type_def, package=self.package.name).lstrip()

    def _generate_creation_from_proto(self, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = "BaseConverters.{{type.to_proto()}}_from_proto({{source_var}})"
        elif isinstance(type_def, EnumTypeDefinition):
            t = "BaseConverters.Enum_from_proto({{source_var}}, {{type.to_typescript_internal_fix(full_path=True)}})"
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = f"{type_def.get_typescript_converter_from_proto()}({source_var})"
        else:
            raise ParseException("Invalid type")

        return render(t, source_var=source_var, type=type_def)

    def _generate_member_assignment_to_proto(self, member: MemberDefinition):
        source_var = "x.{name}".format(name=member.name_camelcase)
        t = self._create_assignment_to_proto(source_var=source_var,
                                             type_def=member.type)
        return f'if (x.HasField({member.model.type.to_typescript()}.DESCRIPTOR.{member.name_camelcase}))\n' \
               f'    obj["{member.name_camelcase_lowerfirst}"] = {t};'

    def _create_member_assignment_from_proto(self, member: MemberDefinition):
        t = self._generate_creation_from_proto(source_var="x.{name}".format(name=member.name_camelcase_lowerfirst),
                                               type_def=member.type)
        return f'if (x.{member.name_camelcase_lowerfirst} !== null && x.{member.name_camelcase_lowerfirst} !== undefined)\n' \
               f'    obj.{member.name_camelcase} = {t};'

    def _create_custom_type_converter(self, custom_type: ModelDefinition):
        members_converters_to_proto = ""
        members_converters_from_proto = ""

        async_str = ""
        if custom_type.is_typescript_async():
            async_str = "async "

        for member in custom_type.members:
            members_converters_to_proto += indent(self._generate_member_assignment_to_proto(member), "        ") + "\n"
            members_converters_from_proto += indent(self._create_member_assignment_from_proto(member), "    ") + "\n"
        return f"""

public static {async_str}{custom_type.get_typescript_converter_to_proto(full_path=False)}(x: {custom_type.to_typescript()}) {{
    const ProtoType = proto_root.lookupType("{custom_type.to_proto(True)}");
    
    const obj: PlainObject = {{}};
    if (x === null || x === undefined)
        obj["isNull"] = true;
    else if (!x.HasAnyField())
        obj["isEmpty"] = true;
    else {{
{members_converters_to_proto}
    }}
    return ProtoType.create(obj);
}}

public static {custom_type.get_typescript_converter_from_proto(full_path=False)}(x: any): {custom_type.to_typescript()} {{
    if (x.isNull)
        return null;
    const obj = new {custom_type.to_typescript()}();
{members_converters_from_proto}
    return obj;
}}
"""

    def _generate_models(self):
        enums_content = ""
        for type_ in self.generator.registry.get_package_types(self.package):
            if isinstance(type_, EnumTypeDefinition):
                # if type_.parent_model is None:
                # if type_.namespace is not None:
                #     enums_content += f"export namespace {type_.namespace.name} {{\n{self._generate_enum(type_)}\n}}\n"
                # else:
                enums_content += f"{self._generate_enum(type_)}\n"

        # Types
        file_content = f"""
/* tslint:disable */
import Decimal from 'decimal.js';

import * as kdlib from 'kdlib';
import {{ List }} from 'kdlib';
import {{ Moment }} from 'moment';
import {{ APIEnum, APIField, APIModel, IAPIModelDescriptor }} from 'kdrpc';

// import * as user_ts from '../../user_ts/{self.package.name}';

export namespace Enums_{self.package.name} {{ 
{enums_content}
}}

""".lstrip()

        self._write_package_file("enums.ts", file_content)

        models_content = ""
        for model in self.generator.registry.get_package_models(self.package):
            models_content += self._generate_single_model(model)

        # Types
        file_content = render("""
/* tslint:disable */
import Decimal from 'decimal.js';

import * as kdlib from 'kdlib';
import { List } from 'kdlib';
import { Moment } from 'moment';
import { 
    APIEnum, APIField, APIModel, APIEvent, IAPIModelDescriptor, UUID,

    APITypeInteger, APITypeInteger64, APITypeString, APITypeDecimal, APITypeFloat, APITypeDate, APITypeDateTime,
    APITypeTime, APITypeJSON, APITypeBSON, APITypeBool, APITypeQuery, APITypeBinary, APITypeEnum,
    APITypeModel, APITypeList, APITypeMap, APITypeUUID,
} from 'kdrpc';

{% for package in [this.package] + this.package.depends %}
import * as Interfaces{{package.name_camelcase}} from '../../user_ts/{{package.name}}';
import { Enums_{{package.name}} } from '../../gen_ts/{{package.name}}/enums';
import { PackageDescriptor as PackageDescriptor_{{package.name}} } from '../../gen_ts/{{package.name}}/descriptor';
{% endfor %}

export namespace BaseModels {
{{models_content}}
}

""", this=self, models_content=models_content).lstrip()

        self._write_package_file("types.ts", file_content)

    def _generate_converters(self):
        file_content = render("""
/* tslint:disable */
import * as protobuf from 'protobufjs';

import * as kdlib from 'kdlib';
import { BaseConverters, UUID } from 'kdrpc';

import proto_pb2_json from '../proto';

import * as types from './types';

import { BaseModels } from './types';

const proto_root = protobuf.Root.fromJSON(proto_pb2_json);

import { Enums_{{this.package.name}} } from './enums';
import * as Interfaces{{this.package.name_camelcase}} from '../../user_ts/{{this.package.name}}';

{% for package in this.package.depends %}
import * as Interfaces{{package.name_camelcase}} from '../../user_ts/{{package.name}}';
import { Converters_{{package.name}} } from '../../gen_ts/{{package.name}}/converters';
import { Enums_{{package.name}} } from '../../gen_ts/{{package.name}}/enums';
{% endfor %}

type PlainObject = { [name: string]: any };

export class Converters_{{this.package.name}} {
""", this=self)

        for _type in self.generator.registry.get_package_types(self.package):
            if isinstance(_type, ModelDefinition):
                file_content += self._create_custom_type_converter(_type)

            if isinstance(_type, ListTypeDefinition):
                async_str = ""
                if _type.is_typescript_async():
                    async_str = "async "

                file_content += f"""

public static {async_str}{_type.get_typescript_converter_to_proto(full_path=False)}(x: {_type.to_typescript_public()}) {{
    const ProtoType = proto_root.lookupType("{_type.to_proto(True)}");
    
    const obj: PlainObject = {{}};
    if (x === null || x === undefined) obj["isNull"] = true;
    else if (x.length === 0)           obj["isEmpty"] = true;
    else {{
        obj["values"] = [];
        for (let i = 0, len = x.length; i < len; i++)
            obj["values"].push({self._create_assignment_to_proto("x[i]", _type.item_type)});
    }}
    return ProtoType.create(obj);
}}

public static {_type.get_typescript_converter_from_proto(full_path=False)}(x: any): {_type.to_typescript_public()} {{
    if (x.isNull)       return null;
    else if (x.isEmpty) return new {_type.to_typescript_internal_fix(full_path=True)}();
    // else                return x.values.map(item => {self._generate_creation_from_proto("item", _type.item_type)});
    else {{
        const obj = new {_type.to_typescript_internal_fix(full_path=True)}(x.values.length);
        x.values.forEach((item: any, i: any) => {{
            obj[i] = {self._generate_creation_from_proto("item", _type.item_type)};
        }});
        return obj;
    }}
}}
"""
            if isinstance(_type, MapTypeDefinition):
                async_str = ""
                if _type.is_typescript_async():
                    async_str = "async "

                file_content += f"""

public static {async_str}{_type.get_typescript_converter_to_proto(full_path=False)}(x: {_type.to_typescript_public()}) {{
    const ProtoType = proto_root.lookupType("{_type.to_proto(True)}");
    
    const obj: PlainObject = {{}};
    if (x === null || x === undefined) obj["isNull"] = true;
    else if (x.size === 0)             obj["isEmpty"] = true;
    else {{
        obj["values"] = [];
        x.forEach({async_str}(value: any, key: any) => {{
          obj["values"].push({{ "key": {self._create_assignment_to_proto("key", _type.key_type)},
                               "value": {self._create_assignment_to_proto("value", _type.value_type)} }});
        }});
    }}
    return ProtoType.create(obj);
}}

public static {_type.get_typescript_converter_from_proto(full_path=False)}(x: any): {_type.to_typescript_public()} {{
    if (x.isNull)       return null;
    else if (x.isEmpty) return new {_type.to_typescript_internal_fix(full_path=True)}();
    else {{
        const obj = new {_type.to_typescript_internal_fix(full_path=True)}();
        x.values.forEach((item: any) => {{
            let k = {self._generate_creation_from_proto("item.key", _type.key_type)};
            let v = {self._generate_creation_from_proto("item.value", _type.value_type)};
            obj.set(k, v);
        }});
        return obj;
    }}
}}
"""

        # convs += self._generate_events()

        file_content += "\n}\n"

        write_file(os.path.join(self.generator.ts_gen_dir, self.package.name, "converters.ts"), file_content.lstrip())

    def _generate_endpoints(self):
        file_content = render("""
/* tslint:disable */
import * as protobuf from 'protobufjs';
import { Observable } from 'rxjs';
import * as kdlib from 'kdlib';

import { List } from 'kdlib';
import { Moment } from 'moment';

import {
    UUID,
    IEndpointClientStub, IAPIClient, IAPIRequestContext, IAPIEndpoint,
    InternalAPIClient, APIEndpointMethodMeta, BaseConverters
} from 'kdrpc';

import proto_pb2_json from '../proto';
let proto_root = protobuf.Root.fromJSON(proto_pb2_json);

import { Converters_{{ package.name }} } from './converters';
import * as types from './types';
import { PackageDescriptor } from './descriptor';

import { Enums_{{this.package.name}} } from './enums';
import * as Interfaces{{package.name_camelcase}} from '../../user_ts/{{this.package.name}}';

{% for package in this.package.depends %}
import * as Interfaces{{package.name_camelcase}} from '../../user_ts/{{package.name}}';
import { Enums_{{package.name}} } from '../../gen_ts/{{package.name}}/enums';
{% endfor %}

import {
    BaseModels
} from './types';

export namespace Endpoints {

const AnyType = proto_root.lookupType("google.protobuf.Any");

export abstract class BaseAPIClient extends InternalAPIClient {
{%- for ep in endpoints %}
    public {{ ep.name }}: {{ ep.name }}ClientStub;
{%- endfor %}

    protected GetPackageDescriptor() { return PackageDescriptor.GetInstance(); } 

    constructor() {
        super();
{%- for ep in endpoints %}
        this.{{ ep.name }} = new {{ ep.name }}ClientStub(this);
{%- endfor %}
    }
}
            
{% for endpoint in endpoints -%}
{#- Server call -#}
export abstract class {{endpoint.name}}<T extends IAPIRequestContext> implements IAPIEndpoint {
    public Name = "{{endpoint.name}}";
    
    public MethodsMeta = new Map<string,APIEndpointMethodMeta> ([
{% for method in endpoint.methods %}
            [ "{{ method.name }}", new APIEndpointMethodMeta(
                () => new BaseModels.{{endpoint.name}}{{method.name}}Response(),
                proto_root.lookupType("{{method.params_type.to_proto(full_path=True)}}"),
                (x: any) => {{method.params_type.get_typescript_converter_from_proto()}}(x),
                async (x: any) => {{ create_assignment_to_proto("x",method.return_type) }},
                {{ "true" if method.has_single_return() else "false" }}
            ) ],
{%- endfor %}
    ]);

{% for method in endpoint.methods %}
    public abstract {{method.name}}(ctx: T, params: BaseModels.{{endpoint.name}}{{method.name}}Params): {{ method.get_return_typescript_rx() }} | {{ method.get_return_typescript_async() }};
{%- endfor %}
}

    {% for method in endpoint.methods %}
export class {{endpoint.name}}{{method.name}}Builder {
  private params = new BaseModels.{{endpoint.name}}{{method.name}}Params();

  constructor(private endpoint: IEndpointClientStub) {
  }

        {% for arg in method.arguments %}
  public Set{{ arg.name_camelcase }}({{ arg.name }}: {{ arg.type.to_typescript_public() }}): {{endpoint.name}}{{method.name}}Builder {
    this.params.{{ arg.name_camelcase }} = {{ arg.name }};
    return this;
  }
        {% endfor %}

  public Build(timeout = -1, noExpire = false, noAck = false): {{ method.get_return_typescript_rx() }} {
    let msgId = this.endpoint.Client.GenerateMessageId();
    return Observable.of(null)
{%- if method.params_type.is_typescript_async() %}
      .flatMap(() => Observable.fromPromise((async () => {{ create_assignment_to_proto("this.params",method.params_type) }})()))
{%- else %}
      .map(() => {{ create_assignment_to_proto("this.params",method.params_type) }})
{%- endif %}
      .flatMap(paramsProto => {
        let paramsUrl = `type.googleapis.com/{{method.params_type.to_proto(full_path=True)}}`;
        let packedParams = AnyType.create({ "type_url": paramsUrl, "value": paramsProto.$type.encode(paramsProto).finish() });
        this.endpoint.Client.BeforeRequest(msgId, "{{endpoint.name}}", "{{method.name}}", paramsProto, this.params);
        return this.endpoint.Client.Call(msgId, "{{endpoint.name}}", "{{method.name}}", packedParams, timeout, noExpire, noAck);
      })
      .map(packedResponse => {
        let ResponseType = proto_root.lookupType("{{method.return_type.to_proto(full_path=True)}}");
        let responseProto = ResponseType.decode(packedResponse["value"]);
        let response = {{ method.return_type.get_typescript_converter_from_proto() }}(responseProto);
        this.endpoint.Client.AfterRequest(msgId, "{{endpoint.name}}", "{{method.name}}", responseProto, response);
        return response;
      })
{%- if method.has_single_return() %}
      .map(response => response.DefaultReturn)
{%- endif -%}
    ;
  }
}
    {% endfor %}
    
export class {{endpoint.name}}ClientStub implements IEndpointClientStub {
    public Client: IAPIClient;
    public Name = "{{endpoint.name}}";
    
    public constructor(client: IAPIClient) { this.Client = client; }

    static BuilderClass = class {
        constructor(private stub: IEndpointClientStub) { }
{% for method in endpoint.methods %}
        public get {{method.name}}(): {{endpoint.name}}{{method.name}}Builder { return new {{endpoint.name}}{{method.name}}Builder(this.stub); }
{%- endfor %}
    };
    
    public Builders = new {{endpoint.name}}ClientStub.BuilderClass(this);
    {% for method in endpoint.methods %}
    public {{method.name}}(
            {%- for arg in method.arguments -%}
                {{arg.name}}: {{arg.type.to_typescript_public()}}, 
            {%- endfor -%}
            timeout=-1, noExpire=false, noAck=false): {{ method.get_return_typescript_rx() }} {
        return this.Builders.{{method.name}}
{%- for arg in method.arguments %}
            .Set{{arg.name_camelcase}}({{arg.name}})
{%- endfor %}
            .Build(timeout, noExpire, noAck);
    }
{% endfor -%}
}

{% endfor -%}

}
""",
                              this=self,
                              endpoints=self.generator.registry.get_package_endpoints(self.package),
                              package=self.package,
                              create_assignment_to_proto=self._create_assignment_to_proto)

        write_file(os.path.join(self.generator.ts_gen_dir, self.package.name, "endpoints.ts"), file_content.lstrip())

    def _generate_descriptor(self):
        events = [x for x in self.generator.registry.get_package_models(self.package) if x.is_event]
        models = [x for x in self.generator.registry.get_package_models(self.package) if not x.is_event]
        convs = render("""
/* tslint:disable */
import * as protobuf from 'protobufjs';

import proto_pb2_json from '../proto';
let proto_root = protobuf.Root.fromJSON(proto_pb2_json);

import { BaseModels } from './types';
import * as Interfaces{{package.name_camelcase}} from '../../user_ts/{{package.name}}';
import { Converters_{{package.name}} } from '../../gen_ts/{{package.name}}/converters';
import { BasePackageDescriptor, APIEvent, APIModel } from 'kdrpc';

export class PackageDescriptor extends BasePackageDescriptor {
    public static Instance: PackageDescriptor = null;
    public static GetInstance(): PackageDescriptor { return PackageDescriptor.Instance ? PackageDescriptor.Instance : (PackageDescriptor.Instance = new PackageDescriptor()); }

    public EventFromMessage(msg: any): APIEvent
    {
      const eventProtoType = proto_root.lookupType(msg.event.payload.type_url.split("/")[1]);
      switch (msg.event.name)
      {
    {%- for event in events %}
        case "{{ event.name }}": return {{ event.get_typescript_converter_from_proto() }}(eventProtoType.decode(msg.event.payload.value));
    {%- endfor %}
        default: throw new Error("Invalid event type");
      }
    }
    
    public ModelToBytes(model: APIModel): Uint8Array
    {
        let obj = null;
        if (0) {}
    {%- for model in models %}
        else if (model instanceof {{ model.to_typescript() }})
            obj = {{ model.get_typescript_converter_to_proto() }}(model);
    {%- endfor %}
        else
            throw new Error("Invalid model type");
        
        return obj.$type.encode(obj).finish();
    }
    
    public ModelFromBytes(data: Uint8Array, modelTypeName: string): APIModel
    {
        const ProtoType = proto_root.lookupType(modelTypeName);
        
        if (0) {}
    {%- for model in models %}
        else if (modelTypeName === "{{ model.type.to_proto(full_path=True) }}")
            return {{ model.get_typescript_converter_from_proto() }}(ProtoType.decode(data));
    {%- endfor %}
        else
            throw new Error("Invalid model type");
    }

}
""".lstrip(), package=self.package, events=events, models=models, types=self.generator.registry.get_package_models(self.package))

        self._write_package_file("descriptor.ts", convs)
