from jinja2 import Template

template_cache = {}


def write_file(path: str, content: str):
    with open(path, "wt") as f:
        f.write(content)


def append_file(path: str, content: str):
    with open(path, "at") as f:
        f.write(content)


def read_file(path: str):
    with open(path, "rt") as f:
        return f.read()


def render(template, **kwargs):
    if template in template_cache:
        tpl = template_cache[template]
    else:
        tpl = Template(template)
        template_cache[template] = tpl
    return tpl.render(**kwargs)


def underscore2camelcase(x):
    return x.replace("_", " ").title().replace(" ", "")


def underscore2camelcase_lowerfirst(x):
    v = underscore2camelcase(x)
    return v[0].lower() + v[1:]
