from textwrap import indent
import os

from .generator import APIGenerator, ParseException
from .parser_types import *
from .utils import *

script_dir = os.path.dirname(os.path.realpath(__file__))


class PythonAPIGenerator:
    def __init__(self, generator: APIGenerator, package: PackageDefinition):
        self.generator = generator
        self.package = package

    def generate(self):
        os.makedirs(os.path.join(self.generator.python_gen_dir, self.package.name), exist_ok=True)
        os.makedirs(os.path.join(self.generator.python_user_dir, self.package.name), exist_ok=True)

        self._generate_models()

        tpl_converters = open(script_dir + "/../assets/tpl_converters.py", "rt").read()

        tpl_converters += """
from .. import proto_{package}_pb2
""".format(package=self.package.name)

        tpl_converters += self.generate_converters()
        tpl_converters += self.generate_events()

        write_file(os.path.join(self.generator.python_gen_dir, self.package.name, "converters.py"), tpl_converters)

        py_file = self.generate_endpoints()
        write_file(os.path.join(self.generator.python_gen_dir, self.package.name, "endpoints.py"), py_file)

        self._generate_user_models()

    def _generate_models(self):
        cnt = """
import enum

__all__ = []
""".lstrip()

        for type in self.generator.registry.get_package_enums(self.package):
            if type.parent_model is None:
                cnt += self._generate_py_enum(type) + "\n\n"
                cnt += "__all__ += [\"" + type.name + "\"]"

        write_file(os.path.join(self.generator.python_gen_dir, self.package.name, "enums.py"), cnt)

        tpl_classes = open(script_dir + "/../assets/tpl_classes.py", "rt").read()

        tpl_classes += render("""
__all__ = []

if TYPE_CHECKING:
    from ...user_py import {{this.package.name}}
    
    {%- for package in this.package.depends %}
    from ...user_py import {{package.name}}
    {% endfor %}
""", this=self)

        for model in self.generator.registry.get_package_models(self.package):
            class_name = model.base_class_name

            model_enums = [indent(self._generate_py_enum(x), "    ") for x in model.enums]

            py_class = render("""
class {{ class_name }}({{ base_class }}):
{%- for enum in model_enums %}
    {{ enum }}
{% endfor %}
    
    class DESCRIPTOR:
{%- for member in model.members %}
    {%- if member.type.to_python_base_type() == "enum.Enum" %}
        {{ member.name }} = APIFieldEnum("{{ member.name }}", {{ member.id }}, "{{ class_name }}", "{{ member.type.to_proto(full_path=True) }}", lambda: {{ member.type.to_python() }})
    {%- else %}
        {{ member.name }} = {{ member.type.get_python_field_type() }}("{{ member.name }}", {{ member.id }}, "{{ class_name }}", "{{ member.type.to_proto(full_path=True) }}")
    {%- endif -%}
{%- else %}
        pass
{%- endfor %}

        @staticmethod
        def get_fields():
            return [{% for member in model.members %}{{ class_name }}.DESCRIPTOR.{{ member.name }}, {% endfor %}]
            
        @staticmethod
        def get_field_names():
            return [{% for member in model.members %}"{{ member.name }}", {% endfor %}]

{% if not model.internal %}
    @staticmethod
    def create( 
{%- for member in model.members %}
                 {{ member.name }}: '{{ member.type.to_python() }}' = None,
{%- endfor -%}
) -> '{{ model.to_python_public() }}':
        obj = {{ model.to_python_public() }}()
{%- for member in model.members %}
        if {{ member.name }} is not None: obj.{{ member.name }} = {{ member.name }}
{%- endfor %}
        return obj
{% endif %}

    def __init__(self) -> None:
        super().__init__()
{%- for member in model.members %}
        self._{{ member.name }}: '{{ member.type.to_python() }}' = None
{%- endfor -%}

{% for member in model.members %}

    @property
    def {{ member.name }}(self) -> '{{ member.type.to_python() }}':
        return self._{{ member.name }}
    
    @{{ member.name }}.setter
    def {{ member.name }}(self, v: '{{ member.type.to_python() }}') -> None:
        self._{{ member.name }} = v
        self._existing_fields.add({{ class_name }}.DESCRIPTOR.{{ member.name }})
{%- endfor %}

    def __eq__(self, other):
        if other is None or not isinstance(other, self.__class__): return False
{%- for member in model.members %}
        if self._{{ member.name }} != other._{{ member.name }}: return False
{%- endfor %}
        return True

    def __ne__(self, other):
        if other is None or not isinstance(other, self.__class__): return True
{%- for member in model.members %}
        if self._{{ member.name }} != other._{{ member.name }}: return True
{%- endfor %}
        return False
        
{% if model.internal %}
{% endif %}
__all__ += ["{{ class_name }}"]
""", class_name=class_name, model=model, model_enums=model_enums, base_class="Event" if model.is_event else "Model")

            tpl_classes += py_class

            tpl_classes += "\n\n"

        tpl_classes += "\n"

        tpl_classes += render("""
from ...user_py import {{this.package.name}}

{%- for package in this.package.depends %}
from ...user_py import {{package.name}}
{% endfor %}
""", this=self)

        write_file(os.path.join(self.generator.python_gen_dir, self.package.name, "types.py"), tpl_classes)

    def _generate_user_models(self):
        user_dir = self.generator.python_user_dir
        os.makedirs(user_dir, exist_ok=True)

        user_models = [x for x in self.generator.registry.get_package_models(self.package) if not x.internal]

        def process_ns(ns: NamespaceDefinition, level=0):
            index_content = ""
            # index_content = f"import {{ Enums }} from '{'../' * level}../../gen_ts/{self.package.name}/enums';\n"

            if ns is None:
                entries = self.generator.registry.get_package_root_entries(self.package)
                path_items = []
            else:
                entries = self.generator.registry.get_namespace_entries(ns)
                path_items = ns.get_full_path()

            for entry in entries:
                if isinstance(entry, NamespaceDefinition):
                    index_content += f"from . import {entry.name}\n"
                    process_ns(entry, level=level + 1)
                if isinstance(entry, EnumTypeDefinition):
                    if entry.parent_model is None:
                        index_content += f"from ..{'.' * level}.gen_py.{self.package.name}.enums import {entry.name}\n"
                if isinstance(entry, ModelDefinition):
                    if entry.internal:
                        continue
                    index_content += f"from .{entry.name} import {entry.name}\n"

            p = os.path.join(user_dir, self.package.name, *path_items, "__init__.py")
            os.makedirs(os.path.dirname(p), exist_ok=True)
            write_file(p, index_content)

        process_ns(None)

        for model in user_models:
            path_parts = [user_dir, self.package.name]
            relative_up = ""
            # relative_path = ""
            if model.namespace is not None:
                path_parts += model.namespace.get_full_path()
                relative_up += "".join("." for _ in model.namespace.get_full_path())
                # relative_path += "".join(x + "/" for x in model.namespace.get_full_path())

            user_model_dir = os.path.join(*path_parts)
            user_model_file = os.path.join(user_model_dir, model.name + ".py")
            os.makedirs(user_model_dir, exist_ok=True)

            # write_file(os.path.join(user_model_dir, "__init__.py"), "")

            py_class = f"""
from {relative_up}...gen_py.{self.package.name} import types

class {model.name}(types.{model.base_class_name}):
    def __init__(self) -> None:
        super().__init__()
        """.lstrip()

            if not os.path.exists(user_model_file):
                write_file(user_model_file, py_class)

    def _generate_py_enum(self, enum_def: EnumTypeDefinition):
        return render("""
class {{ enum.name }}(enum.Enum):
{%- for value in enum.values %}
    {{ value.name }} = {{ value.id }}
{%- endfor %}
    
    @property
    def display(self):
        DISPLAY_MAP = {
{%- for value in enum.values %}
            {{ value.id }}: "{{ value.display }}",
{%- endfor %}
        }
        return DISPLAY_MAP[self.value]
        
    def __repr__(self):
        return "{{ enum.name }}.{}".format(self.name)
""", enum=enum_def)

    def _generate_py_imports(self):
        s = ""
        for model in self.generator.models:
            if model.internal:
                continue
            name = ".".join(model.get_full_path())
            print(name)
            s += "from ...user_py.{package}.{0} import {0}\n".format(name, package=self.package.name)
        return s

    def _generate_assignment_to_proto(self, target_var: str, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = "{{type.get_python_converter_to_proto()}}({{target_var}}, {{source_var}})"
        elif isinstance(type_def, EnumTypeDefinition):
            t = 'Enum_to_proto({{target_var}}, {{source_var}})'
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = "{{type.get_python_converter_to_proto()}}({{target_var}}, {{source_var}})"
        else:
            raise ParseException("Invalid type")

        return render(t, target_var=target_var, source_var=source_var, type=type_def)

    def _generate_creation_from_proto(self, source_var: str, type_def: TypeDefinition):
        if isinstance(type_def, SimpleTypeDefinition):
            t = "{{type.get_python_converter_from_proto()}}({{source_var}})"
        elif isinstance(type_def, EnumTypeDefinition):
            t = "Enum_from_proto({{source_var}}, {{type.to_python()}})"
        elif isinstance(type_def, ModelDefinition) or \
                isinstance(type_def, ListTypeDefinition) or \
                isinstance(type_def, MapTypeDefinition) or \
                isinstance(type_def, EnumTypeDefinition):
            t = "{{type.get_python_converter_from_proto()}}({{source_var}})"
        else:
            raise ParseException("Invalid type")

        return render(t, source_var=source_var, type=type_def)

    def _create_member_assignment_to_proto(self, name: str, base_type_def: ModelDefinition, type_def: TypeDefinition):
        source_var = "x.{name}".format(name=name)
        t = self._generate_assignment_to_proto(target_var="obj.{name}".format(name=name),
                                               source_var=source_var,
                                               type_def=type_def)
        return 'if {type}.DESCRIPTOR.{name} in x._existing_fields:\n' \
               '    {t}'.format(
                name=name,
                type=base_type_def.to_python(),
                source_var=source_var, t=t)

    def _create_member_assignment_from_proto(self, name: str, base_type_def: ModelDefinition, type_def: TypeDefinition):
        t = self._generate_creation_from_proto(source_var="x.{name}".format(name=name),
                                               type_def=type_def)
        return 'if x.HasField("{name}"):\n' \
               '    obj.{name} = {t}'.format(name=name, t=t)

    def _create_custom_type_converter(self, model: ModelDefinition):
        convs = ""

        members_converters = ""
        for member in model.members:
            members_converters += \
                indent(self._create_member_assignment_to_proto(member.name,
                                                               model,
                                                               member.type), "        ") + "\n"
        if len(model.members) == 0:
            members_converters = "        pass"
        convs += f"""

def {model.get_python_converter_to_proto(full_path=False)}(obj: {model.to_proto(full_path=True)}, x: '{model.to_python()}'):
    if x is None:
        obj.is_null = True
    elif len(x._existing_fields) == 0:
        obj.is_empty = True
    else:
{members_converters}

__all__ += ["{model.get_python_converter_to_proto(full_path=False)}"]
"""

        members_converters = ""
        for member in model.members:
            members_converters += \
                indent(self._create_member_assignment_from_proto(member.name,
                                                                 model,
                                                                 member.type), "    ") + "\n"

        convs += f"""
def {model.get_python_converter_from_proto(full_path=False)}(x: {model.to_proto(full_path=True)}) \
        -> Optional['{model.to_python()}']:
    if x.is_null:
        return None

    obj = {model.to_python()}()
{members_converters}
    return obj

__all__ += ["{model.get_python_converter_from_proto(full_path=False)}"]
"""

        return convs

    def generate_converters(self):
        convs = render("""
from ...user_py import {{this.package.name}}
{%- for package in this.package.depends %}
from ...gen_py.{{package.name}}.converters import *
from ...user_py import {{package.name}}
{% endfor %}

__all__ = []
""", this=self)
        for _type in self.generator.registry.get_package_types(self.package):
            if isinstance(_type, ModelDefinition):
                convs += self._create_custom_type_converter(_type)

            if isinstance(_type, EnumTypeDefinition):
                convs += ""

            if isinstance(_type, ListTypeDefinition):
                convs += f"""

def {_type.get_python_converter_to_proto(full_path=False)}(p: {_type.to_proto(full_path=True)}, x: '{_type.to_python()}'):
    if x is None:
        p.is_null = True
    elif len(x) == 0:
        p.is_empty = True
    else:
        for item in x:
            p_item = p.values.add()
            {self._generate_assignment_to_proto("p_item", "item", _type.item_type)}


def {_type.get_python_converter_from_proto(full_path=False)}(x: {_type.to_proto(full_path=True)}) \
        -> Optional['{_type.to_python()}']:
    if x.is_null:
        return None
    if x.is_empty:
        return []
        
    obj = [{self._generate_creation_from_proto("x", _type.item_type)} for x in x.values]
    return obj

__all__ += ["{_type.get_python_converter_to_proto(full_path=False)}", "{_type.get_python_converter_from_proto(full_path=False)}"]
"""

            if isinstance(_type, MapTypeDefinition):
                convs += f"""

def {_type.get_python_converter_to_proto(full_path=False)}(p: {_type.to_proto(full_path=True)}, x: '{_type.to_python()}'):
    if x is None:
        p.is_null = True
    elif len(x) == 0:
        p.is_empty = True
    else:
        for key, value in x.items():
            item = p.values.add()
            {_type.key_type.get_python_converter_to_proto()}(item.key, key)
            {_type.value_type.get_python_converter_to_proto()}(item.value, value)


def {_type.get_python_converter_from_proto(full_path=False)}(x: {_type.to_proto(full_path=True)}) \
        -> Optional['{_type.to_python()}']:
    if x.is_null:
        return None
    if x.is_empty:
        return {{}}
        
    obj = {{ {_type.key_type.get_python_converter_from_proto()}(x.key):
             {_type.value_type.get_python_converter_from_proto()}(x.value) for x in x.values }}
    return obj

__all__ += ["{_type.get_python_converter_to_proto(full_path=False)}", "{_type.get_python_converter_from_proto(full_path=False)}"]
"""

        convs += render("""
def model_to_proto(model: Model):
    if False:
        p = None
{%- for model in models %}
    elif isinstance(model, {{ model.to_python() }}):
        p = {{ model.to_proto(full_path=True) }}()
        {{ model.get_python_converter_to_proto() }}(p, model)
{%- endfor %}
    else:
        raise ValueError("invalid model")
    return p
    
def model_from_proto(p, cls):
    if False:
        p = None
{%- for model in models %}
    elif cls == {{ model.to_python() }}:
        return {{ model.get_python_converter_from_proto() }}(p)
{%- endfor %}
    else:
        raise ValueError("invalid model")
    return p
    
# def deserialize_model(msg_bytes: Union[bytes,str]):
#     msg = proto_base_pb2.APIMessage()
#     if isinstance(msg_bytes, bytes):
#         msg.ParseFromString(msg_bytes)
#     elif isinstance(msg_bytes, str):
#         Parse(msg_bytes, msg)
#     assert msg.HasField("event")
# 
#     obj: Event = None
#     
#     if False:
#         pass
# {%- for event in events %}
#     elif msg.event.name == "{{ event.name }}":
#         p = proto_{{package}}_pb2.{{ event.name }}()
#         msg.event.payload.Unpack(p)
#         obj = {{ event.name }}_from_proto(p)
# {%- endfor %}
#     else:
#         raise ValueError("invalid event")
#         
#     return obj
    
""",
                        models=self.generator.registry.get_package_models(self.package),
                        package=self.package.name)

        return convs

    def generate_endpoints(self):
        t = render("""
from .. import proto_{{this.package.name}}_pb2

import datetime, enum, uuid
from decimal import Decimal
from abc import abstractmethod
from typing import Optional, Union, Dict, Any, List

from .types import *
from .converters import *
from kdrpc import proto, APIQuery, IAPIRequestContext

from ...user_py import {{this.package.name}}
{%- for package in this.package.depends %}
from ...user_py import {{package.name}}
{% endfor %}

JSONValue = Union[str, int, float, bool, None, Dict[str, Any], List[Any]]
JSONType = Union[Dict[str, JSONValue], List[JSONValue]]

class BaseAPIClient(proto.IBaseAPIClient):
    def __init__(self) -> None:
        super().__init__()
{%- for ep in endpoints %}
        self.{{ ep.name }} = {{ ep.name }}ClientStub(self)
{%- endfor %}

    @abstractmethod
    async def send_request(self, request: bytes, endpoint_name: str, timeout: int, no_expire: bool, no_ack: bool) -> bytes:
        pass
        
__all__ = ["BaseAPIClient"]
""", endpoints=self.generator.registry.get_package_endpoints(self.package), this=self)

        for endpoint in self.generator.registry.get_package_endpoints(self.package):
            data = {
                "endpoint_name": endpoint.name,
                "methods": endpoint.methods,
                "package": self.package.name,
            }
            t += render("""
{% macro get_return_type(endpoint_name, method) %}
    {%- if method.has_single_return() -%}
     -> Optional[{{ method.return_parameters[0].type.to_python() }}]
    {%- elif method.has_return() -%}
     -> Optional[{{endpoint_name}}{{method.name}}Response]
    {%- endif -%}
{%- endmacro -%}
{# Server call #}
class {{endpoint_name}}(proto.APIEndpoint):
    MethodsMeta = {
{% for method in methods %}
            "{{ method.name }}": proto.APIEndpointMethodMeta(
                {{endpoint_name}}{{method.name}}Response,
                {{ method.return_type.to_proto(full_path=True) }},
                {{ method.params_type.to_proto(full_path=True) }},
                {{ method.params_type.get_python_converter_from_proto() }},
                {{ method.return_type.get_python_converter_to_proto() }},
                {{ "True" if method.has_single_return() else "False" }}
            ),
{%- endfor %}
    }
    
    def get_methods_meta(self):
        return {{endpoint_name}}.MethodsMeta
    
    def __init__(self) -> None:
        super().__init__("{{endpoint_name}}", [{% for method in methods %}"{{method.name}}", {% endfor %}])
{% for method in methods %}
    @abstractmethod
    async def {{method.name}}(self, ctx: IAPIRequestContext, params: {{endpoint_name}}{{method.name}}Params){{ get_return_type(endpoint_name, method) }}:
        pass
{% endfor %}

class {{endpoint_name}}ClientStub(proto.BaseEndpointClientStub):
    def __init__(self, client: BaseAPIClient) -> None:
        super().__init__("{{endpoint_name}}", client)

{#- Client - End-user method calls #}
{% for method in methods %}
    async def {{method.name}}(self{% for arg in method.arguments %}, {{arg.name}}: {{arg.type.python_type}}{% endfor %}, *, timeout: int=None, no_expire: bool=False, no_ack: bool=False){{ get_return_type(endpoint_name, method) }}:
        __parms = {{endpoint_name}}{{method.name}}Params()
{%- for arg in method.arguments %}
        assert {{arg.name}} is None or isinstance({{arg.name}}, {{arg.type.python_base_type}})
        __parms.{{arg.name}} = {{arg.name}}
{%- endfor %}

        params_proto = {{ method.params_type.to_proto(full_path=True) }}()
        {{ method.params_type.get_python_converter_to_proto() }}(params_proto, __parms)
        
        packed_resp = await self.client.call(
                self.endpoint_name,
                "{{method.name}}",
                params_proto,
                timeout=timeout,
                no_expire=no_expire,
                no_ack=no_ack)
                
        if no_ack:
            return None
        else:
            proto_resp = {{ method.return_type.to_proto(full_path=True) }}()
            packed_resp.Unpack(proto_resp)
            resp = {{ method.return_type.get_python_converter_from_proto() }}(proto_resp)
            {% if method.has_single_return() -%}
            resp = resp.default_return
            {% endif -%}
            return resp
            
{% endfor %}
__all__ += ["{{endpoint_name}}", "{{endpoint_name}}ClientStub"]
""", **data)

        return t

    def generate_events(self):
        events = [x for x in self.generator.models if x.is_event]

        t = render("""
from google.protobuf.json_format import MessageToJson, Parse

def serialize_event(event: Event, to_json=False):
    msg = proto_base_pb2.APIMessage()
    msg.event.name = type(event).__name__
    
    if False:
        p = None
{%- for event in events %}
    elif isinstance(event, {{ event.to_python_public() }}):
        p = {{ event.to_proto(full_path=True) }}()
        {{ event.get_python_converter_to_proto() }}(p, event)
{%- endfor %}
    else:
        raise ValueError("invalid event")
    
    msg.event.payload.Pack(p)
        
    if to_json:
        return MessageToJson(msg)
    else:
        return msg.SerializeToString()
    
def deserialize_event(msg_bytes: Union[bytes,str]):
    msg = proto_base_pb2.APIMessage()
    if isinstance(msg_bytes, bytes):
        msg.ParseFromString(msg_bytes)
    elif isinstance(msg_bytes, str):
        Parse(msg_bytes, msg)
    assert msg.HasField("event")

    obj: Event = None
    
    if False:
        pass
{%- for event in events %}
    elif msg.event.name == "{{ event.name }}":
        p = {{ event.to_proto(full_path=True) }}()
        msg.event.payload.Unpack(p)
        obj = {{ event.get_python_converter_from_proto() }}(p)
{%- endfor %}
    else:
        raise ValueError("invalid event")
        
    return obj
    
""", events=events, package=self.package.name)

        return t
