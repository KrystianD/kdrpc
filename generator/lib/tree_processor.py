from typing import Dict
import os
from lark import InlineTransformer, Tree

from .parser_types import *
from .types import *

script_dir = os.path.dirname(os.path.realpath(__file__))

# noinspection PyMethodMayBeStatic
class TreeProcessor(InlineTransformer):
    number = float
    int_number = int
    identifier = str
    option_value_str = lambda _, x: x[1:-1]
    option_value_int = int

    def __init__(self, package: PackageDefinition):
        super().__init__()
        self.package = package
        self.endpoints = []  # type: List[EndpointDefinition]
        self.types = []  # type: List[TypeDefinition]
        self.namespaces = []  # type: List[NamespaceDefinition]

        self.type_id_to_type = {}  # type: Dict[str,TypeDefinition]

    def namespace(self, namespace_name, *namespace_entries):
        ns = NamespaceDefinition(self.package, namespace_name)
        ns.entries = namespace_entries
        self.namespaces.append(ns)
        for inner in namespace_entries:
            inner.namespace = ns
        return ns

    def namespace_entry(self, e):
        return e

    def process_tree(self, tree):
        self.transform(tree)

    def _create_model(self, identifier, members, is_event):
        enum_members = [x for x in members if isinstance(x, EnumTypeDefinition)]
        var_members = [x for x in members if isinstance(x, MemberDefinition)]

        model = ModelDefinition(self.package, identifier, var_members, enum_members, is_event=is_event)
        for enum_ in enum_members:
            enum_.parent_model = model
        return model

    def model(self, identifier: str, *members):
        m = self._create_model(identifier, members, False)
        self.types.append(m)
        return m

    def event(self, identifier: str, *members):
        e = self._create_model(identifier, members, True)
        self.types.append(e)
        return e

    def endpoint(self, identifier: str, *methods: MethodDefinition):
        s = EndpointDefinition(self.package, identifier, methods)
        self.endpoints.append(s)

    def global_enum(self, enum_def: EnumTypeDefinition):
        return enum_def

    # Enum
    def enum(self, identifier: str, *enum_members):
        if not identifier.endswith("Enum"):
            raise ValueError("Enum name /{}/ doesn't end with 'Enum'".format(identifier))
        enum_def = EnumTypeDefinition(self.package, identifier, list(enum_members))
        self.types.append(enum_def)
        return enum_def

    def enum_value(self, identifier: str, num, options=None):
        return EnumValueDefinition(identifier, num, options or OptionsCollection())

    # Endpoint method
    def method(self, name: str, arguments: Tree, return_parameters=None):
        if return_parameters is None:
            return_parameters = []

        arguments: List[VariableDefinition] = arguments.children

        for i, m in enumerate(arguments):
            m.id = i + 1
        for i, m in enumerate(return_parameters):
            m.id = i + 1

        return MethodDefinition(name, arguments, return_parameters)

    def method_return_object(self, return_parameters: Tree):
        return return_parameters.children

    def method_return_single(self, type_str):
        return [VariableDefinition(type_str, "default_return", 1)]

    # Member
    def var_member(self, variable_definition, num):
        variable_definition.id = num
        return MemberDefinition(variable_definition)

    def enum_member(self, enum_):
        return enum_

    # Variable
    def variable_definition(self, type_str, name):
        return VariableDefinition(type_str, name, -1)

    # Type
    def simple_type(self, type_name):
        type_ = TypeEnum.from_schema(type_name)
        if type_ is None:
            return PlaceholderTypeDefinition(self.package, type_name)
        else:
            return SimpleTypeDefinition(self.package, type_)

    def list_type(self, item_type):
        if isinstance(item_type, str):
            return PlaceholderListTypeDefinition(self.package, self.simple_type(item_type))
        else:
            return PlaceholderListTypeDefinition(self.package, item_type)

    def map_type(self, key_type, value_type):
        if isinstance(key_type, str):
            key_type = self.simple_type(key_type)

        if isinstance(value_type, str):
            value_type = self.simple_type(value_type)

        return PlaceholderMapTypeDefinition(self.package, key_type, value_type)

    # Options
    def options(self, *options: Option):
        return OptionsCollection(options)

    def option(self, identifier: str, option_value: Union[str, int]):
        return Option(identifier, option_value)

    # Helpers
    def _create_type(self, type_def: TypeDefinition):
        new_id = type_def.id

        if new_id in self.type_id_to_type:
            return self.type_id_to_type[new_id]
        else:
            self.types.append(type_def)
            self.type_id_to_type[new_id] = type_def
            return type_def
