import glob
import subprocess
import os
import shutil

from .api_registry import APIRegistry
from .parser_types import *
from .types import *
from .utils import write_file, render

script_dir = os.path.dirname(os.path.realpath(__file__))


# noinspection PyMethodMayBeStatic
class ParseException(Exception):
    pass


class APIGenerator:
    def __init__(self, registry: APIRegistry):
        self.schema_files: List[str] = []

        self.ts_out_dir: str = None
        self.proto_out_dir: str = None
        self.csharp_out_dir: str = None
        self.ts_gen_dir: str = None
        self.csharp_user_dir: str = None
        self.ts_user_dir: str = None

        self.python_out_dir: str = None
        self.python_gen_dir: str = None
        self.python_user_dir: str = None

        self.registry = registry

    @property
    def enums(self):
        return self.registry.enums

    @property
    def types(self):
        return self.registry.types

    @property
    def models(self):
        return self.registry.models

    @property
    def namespaces(self):
        return self.registry.namespaces

    @property
    def root_entries(self) -> List[TypeDefinition]:
        return list(x for x in self.models if x.namespace is None) + \
               list(x for x in self.enums if x.namespace is None) + \
               list(x for x in self.namespaces if x.namespace is None)

    def generate(self, packages_names: List[str]):
        packages = set(self.registry.find_package(name) for name in packages_names)
        packages_to_check = list(packages)

        while len(packages_to_check) > 0:
            pkg = packages_to_check.pop()
            packages |= set(pkg.depends)
            packages_to_check += list(pkg.depends)

        packages = list(sorted(packages, key=lambda x: x.name))

        base_proto_original_path = os.path.join(script_dir, "../../proto_base.proto")
        base_proto_path = os.path.join(self.proto_out_dir, "proto_base.proto")
        shutil.copy(base_proto_original_path, base_proto_path)

        source_proto_files = [base_proto_path]

        def generate_imports(pkg: PackageDefinition):
            return "\n".join("import public \"proto_{}.proto\";".format(x.name) for x in pkg.depends)

        for package in packages:
            proto = f"""
syntax = "proto3";
option csharp_namespace = "proto_{package.name}_pb2";

import "google/protobuf/any.proto";

import public "proto_base.proto";
{generate_imports(package)}

package proto_{package.name}_pb2;

{self._generate_proto_messages(package)}
"""

            proto_path = os.path.join(self.proto_out_dir, "proto_{}.proto".format(package.name))
            write_file(proto_path, proto)

            source_proto_files.append(proto_path)

        args = ["protoc", "--proto_path", self.proto_out_dir]

        if self.python_gen_dir is not None:
            args += ["--python_out", self.python_gen_dir]
        if self.csharp_out_dir is not None:
            args += ["--csharp_out", self.csharp_out_dir]

        if self.python_gen_dir is not None or self.csharp_out_dir is not None:
            subprocess.check_call(args + source_proto_files)

        if self.csharp_out_dir is not None:
            a = os.path.join(self.csharp_out_dir, "ProtoBase.cs")
            os.unlink(a)

        if self.ts_out_dir is not None:
            js_proto = subprocess.check_output([
                "pbjs",
                "-t", "json",
                *source_proto_files
            ]).decode("ascii")

            os.makedirs(os.path.join(self.ts_gen_dir), exist_ok=True)
            ts_proto_path = os.path.join(self.ts_gen_dir, "proto.ts")
            proto = f"""
let proto_pb2_json = {js_proto};
export default proto_pb2_json;
"""
            write_file(ts_proto_path, proto)

        from .generator_ts import TSAPIGenerator
        from .generator_csharp import CSharpAPIGenerator
        from .generator_py import PythonAPIGenerator

        if self.python_out_dir is not None:
            for package in packages:
                PythonAPIGenerator(self, package=package).generate()

                lib_path = os.path.join(self.python_out_dir, f"interfaces_{package.name}.py")
                write_file(lib_path, f"""
# noinspection PyUnresolvedReferences
from .gen_py.{package.name}.types import *
# noinspection PyUnresolvedReferences
# from .gen_py.{package.name}.enums import *
# noinspection PyUnresolvedReferences
from .gen_py.{package.name}.endpoints import *
# noinspection PyUnresolvedReferences
from .gen_py.{package.name}.converters import deserialize_event, serialize_event, model_to_proto, model_from_proto

# noinspection PyUnresolvedReferences
from .user_py.{package.name} import *
""".lstrip())

        if self.csharp_out_dir is not None:
            for package in packages:
                CSharpAPIGenerator(self, package=package).generate()
        if self.ts_out_dir is not None:
            index_content = ""
            for package in packages:
                TSAPIGenerator(self, package=package).generate()

                lib_path = os.path.join(self.ts_out_dir, f"interfaces_{package.name}.ts")
                write_file(lib_path, f"""
export * from './gen_ts/{package.name}/endpoints';
export {{ PackageDescriptor }} from './gen_ts/{package.name}/descriptor';
export {{ BaseModels }} from './gen_ts/{package.name}/types';
export {{ Enums_{package.name} as Enums }} from './gen_ts/{package.name}/enums';
export * from './user_ts/{package.name}/index';
""".lstrip())
                index_content += f"""
import * as Interfaces{package.name_camelcase} from "./interfaces_{package.name}";
export {{ Interfaces{package.name_camelcase} }};

""".lstrip()

            index_path = os.path.join(self.ts_out_dir, f"index.ts")
            write_file(index_path, index_content.rstrip() + "\n")

    def _create_proto_enum(self, enum_def: EnumTypeDefinition):
        return render("""
message {{ enum.to_proto() }} {
    enum {{ enum.to_proto_inner() }} {
    {%- for value in enum.values %}
        {{ value.name }} = {{ value.id }};
    {%- endfor %}
    }
    bool is_null = 1;
    {{ enum.to_proto_inner() }} value = 2;
}""", enum=enum_def)

    def _generate_proto_messages(self, package: PackageDefinition):
        proto = ""

        for enum_def in self.registry.enums:
            if enum_def.package != package:
                continue

            proto += self._create_proto_enum(enum_def)

        for model in self.registry.models:
            if model.package != package:
                continue

            proto += render("""
message {{ model.type.to_proto() }} {
    bool is_null = 100;
    bool is_empty = 101;
{%- for member in model.members %}
    {{ member.type.to_proto(True) }} {{ member.name }} = {{ member.id }};
{%- endfor %}
}""", model=model)

        for type_def in self.registry.internal_types:
            if type_def.package != package:
                continue

            if isinstance(type_def, ListTypeDefinition):
                name = type_def.to_proto()
                proto += """
message {} {{
    bool is_null = 1;
    bool is_empty = 2;
    repeated {} values = 3;
}}""".format(name, type_def.item_type.to_proto(full_path=True))

            if isinstance(type_def, MapTypeDefinition):
                name = type_def.to_proto()
                proto += """
message {name} {{
    message MapItem {{
        {key_name} key = 1;
        {value_name} value = 2;
    }}
    bool is_null = 1;
    bool is_empty = 2;
    repeated MapItem values = 3;
}}""".format(name=name,
             key_name=type_def.key_type.to_proto(full_path=True),
             value_name=type_def.value_type.to_proto(full_path=True))

        return proto

    def add_schema_file(self, path: str):
        self.schema_files += path

    def add_schema_dir(self, path: str, namespace: str = None):
        self.schema_files += glob.glob(os.path.join(path, "**/*.kproto"), recursive=True)

    def set_ts_out_dir(self, path: str):
        self.ts_out_dir = path
        self.ts_gen_dir = os.path.join(path, "gen_ts")
        self.ts_user_dir = os.path.join(path, "user_ts")
        os.makedirs(self.ts_gen_dir, exist_ok=True)
        os.makedirs(self.ts_user_dir, exist_ok=True)

        # idx_path = os.path.join(self.ts_out_dir, "index.ts")
        # write_file(idx_path, "")

    def set_proto_out_dir(self, path: str):
        self.proto_out_dir = path

    def set_python_out_dir(self, path: str):
        self.python_out_dir = path
        self.python_gen_dir = os.path.join(path, "gen_py")
        self.python_user_dir = os.path.join(path, "user_py")
        os.makedirs(self.python_gen_dir, exist_ok=True)
        os.makedirs(self.python_user_dir, exist_ok=True)

    def set_csharp_out_dir(self, path: str):
        self.csharp_out_dir = os.path.join(path, "gen_cs")
        self.csharp_user_dir = os.path.join(path, "user_cs")
        os.makedirs(self.csharp_out_dir, exist_ok=True)
        os.makedirs(self.csharp_user_dir, exist_ok=True)

    def set_python_user_dir(self, path: str):
        self.python_user_dir = path

    def set_csharp_user_dir(self, path: str):
        self.csharp_user_dir = path


if __name__ == "__main__":
    gen = APIGenerator()
    kproto_path = os.path.join(script_dir, "../schema")
    gen.generate_from_dir(kproto_path)
