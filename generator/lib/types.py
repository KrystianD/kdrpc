from typing import List

from abc import abstractmethod


class APIType:
    def __init__(self):
        pass

    @property
    @abstractmethod
    def id(self) -> str:
        pass

    @abstractmethod
    def to_proto(self, full_path=False) -> str:
        pass

    @abstractmethod
    def to_python(self, escape=False) -> str:
        pass

    @property
    def python_type(self):
        return self.to_python()

    def __str__(self):
        return "[Type {}]".format(self.id)


class APIModelMember:
    def __init__(self, name: str, _type: APIType, _id: int):
        self.name = name
        self.type = _type
        self.id = _id


class APIModel:
    def __init__(self, name: str):
        self.name = name
        self.members: List[APIModelMember] = None
        self.is_event = None
        self.internal = None
        self.enums = None
