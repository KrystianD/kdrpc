# Generated
from typing import List, Dict, Optional, Any, Union, Type
from decimal import Decimal
import datetime
import json
import uuid

import enum
import msgpack

from kdrpc import APIQueryFilter, APIQueryFieldsList, Event, Model

JSONValue = Union[str, int, float, bool, None, Dict[str, Any], List[Any]]
JSONType = Union[Dict[str, JSONValue], List[JSONValue]]

from .. import proto_base_pb2
# from .types import *

from . import types


# To proto
def String_to_proto(p: proto_base_pb2.String, x: str):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def Int32_to_proto(p: proto_base_pb2.Int32, x: int):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def Int64_to_proto(p: proto_base_pb2.Int64, x: int):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def Decimal_to_proto(p: proto_base_pb2.Decimal, x: Decimal):
    if x is None:
        p.is_null = True
    else:
        p.value = str(x)


def Float_to_proto(p: proto_base_pb2.Float, x: float):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def Boolean_to_proto(p: proto_base_pb2.Boolean, x: bool):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def Date_to_proto(p: proto_base_pb2.Date, x: datetime.date):
    if x is None:
        p.is_null = True
    else:
        p.year = x.year
        p.month = x.month
        p.day = x.day


def JSON_to_proto(p: proto_base_pb2.JSON, x: JSONType):
    if x is None:
        p.is_null = True
    else:
        p.value = json.dumps(x)


def BSON_to_proto(p: proto_base_pb2.JSON, x: JSONType):
    if x is None:
        p.is_null = True
    else:
        p.value = msgpack.packb(x)


def Binary_to_proto(p: proto_base_pb2.Binary, x: bytes):
    if x is None:
        p.is_null = True
    else:
        p.value = x


def UUID_to_proto(p: proto_base_pb2.UUID, x: uuid.UUID):
    if x is None:
        p.is_null = True
    else:
        p.value = x.bytes


def DateTime_to_proto(p: proto_base_pb2.DateTime, x: datetime.datetime):
    if x is None:
        p.is_null = True
    else:
        Date_to_proto(p.date, x.date())
        Time_to_proto(p.time, x.time())


def Time_to_proto(p: proto_base_pb2.Time, x: datetime.time):
    if x is None:
        p.is_null = True
    else:
        p.hour = x.hour
        p.minute = x.minute
        p.second = x.second
        p.microsecond = x.microsecond
        p.timezone = 0


def Enum_to_proto(p: Any, x: enum.Enum):
    if x is None:
        p.is_null = True
    else:
        p.value = x.value


def APIQuery_to_proto(p: proto_base_pb2.APIQuery, x: 'APIQuery'):
    if x.filter is not None:
        p.filter = json.dumps(x.filter.value)
    if x.fields_list is not None:
        p.fields_list = json.dumps(x.fields_list.value)


# From proto
def String_from_proto(x: proto_base_pb2.String) -> Optional[str]:
    if x.is_null: return None
    return x.value


def Int32_from_proto(x: proto_base_pb2.Int32) -> Optional[int]:
    if x.is_null: return None
    return x.value


def Int64_from_proto(x: proto_base_pb2.Int64) -> Optional[int]:
    if x.is_null: return None
    return x.value


def Decimal_from_proto(x: proto_base_pb2.Decimal) -> Optional[Decimal]:
    if x.is_null: return None
    return Decimal(x.value)


def Float_from_proto(x: proto_base_pb2.Float) -> Optional[float]:
    if x.is_null: return None
    return x.value


def Boolean_from_proto(x: proto_base_pb2.Boolean) -> Optional[bool]:
    if x.is_null: return None
    return x.value


def Binary_from_proto(x: proto_base_pb2.Binary) -> Optional[bytes]:
    if x.is_null: return None
    return x.value


def Date_from_proto(x: proto_base_pb2.Date) -> Optional[datetime.date]:
    if x.is_null: return None
    return datetime.date(x.year, x.month, x.day)


def DateTime_from_proto(x: proto_base_pb2.DateTime) -> Optional[datetime.datetime]:
    if x.is_null: return None
    return datetime.datetime.combine(Date_from_proto(x.date), Time_from_proto(x.time))


def Time_from_proto(x: proto_base_pb2.Time) -> Optional[datetime.time]:
    if x.is_null: return None
    return datetime.time(x.hour, x.minute, x.second, x.microsecond)


def JSON_from_proto(x: proto_base_pb2.String) -> Optional[JSONType]:
    if x.is_null: return None
    return json.loads(x.value)


def BSON_from_proto(x: proto_base_pb2.String) -> Optional[JSONType]:
    if x.is_null: return None
    return msgpack.unpackb(x.value, encoding='utf-8')


def UUID_from_proto(x: proto_base_pb2.UUID) -> Optional[uuid.UUID]:
    if x.is_null: return None
    return uuid.UUID(bytes=x.value)


def Enum_from_proto(x: Any, cls: Type[enum.Enum]) -> Optional[enum.Enum]:
    if x.is_null: return None
    return cls(x.value)


def APIQuery_from_proto(p: proto_base_pb2.APIQuery):
    from kdrpc import APIQuery
    x = APIQuery()
    if p.filter != "":
        x.filter = APIQueryFilter(json.loads(p.filter))
    if p.fields_list != "":
        x.fields_list = APIQueryFieldsList(json.loads(p.fields_list))
    return x
