// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable RedundantNameQualifier
// ReSharper disable AccessToStaticMemberViaDerivedType
// ReSharper disable RedundantVerbatimPrefix
// ReSharper disable ArrangeThisQualifier
#pragma warning disable 169

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;

using JToken = Newtonsoft.Json.Linq.JToken;
using Date = System.DateTime;
using DateTime = System.DateTime;
using Decimal = System.Decimal;
using TimeSpan = System.TimeSpan;

using PBTypeRegistry = Google.Protobuf.Reflection.TypeRegistry;
