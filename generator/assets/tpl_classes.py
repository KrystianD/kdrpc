# Generated
from typing import List, Dict, Optional, Any, Union, Set, TYPE_CHECKING
from decimal import Decimal
import datetime, json
import enum

from kdrpc import APIQueryFilter, APIQuery, Event, Model
from kdrpc import APIField, APIFieldEnum, APIFieldList, APIFieldMap, APIFieldModel, \
    APIFieldInteger, APIFieldInteger64, APIFieldString, APIFieldDecimal, APIFieldFloat, APIFieldDate, \
    APIFieldDateTime, APIFieldTime, APIFieldJSON, APIFieldBSON, APIFieldBool, \
    APIFieldQuery, APIFieldBinary, APIFieldUUID


JSONValue = Union[str, int, float, bool, None, Dict[str, Any], List[Any]]
JSONType = Union[Dict[str, JSONValue], List[JSONValue]]

