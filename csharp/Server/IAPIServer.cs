using System;
using System.Threading.Tasks;
using Google.Protobuf;

namespace KDRPC
{
  public class APIRequestProcessingContext<T> where T : IAPIRequestContext
  {
    public T ctx;
    public string msgId;
    public string endpointName;
    public string methodName;

    public IMessage paramsProto;
    public APIModel @params;

    public IMessage responseProto;
    public APIModel response;
    public byte[] responseBytes;

    public string error;
    public Exception exception;
  }

  public class APIRequestProcessorObserver<T> where T : IAPIRequestContext
  {
    public delegate void BeforeRequestHandlerType(T ctx, string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params);

    public delegate void AfterRequestHandlerType(T ctx, string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, IMessage responseProto, APIModel response);

    public delegate void AfterFailedRequestHandlerType(T ctx, string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, string error, Exception exception);
    public delegate void UnableToHandleRequestType(T ctx, string msgId, string endpointName, string methodName, string error, Exception exception);

    public BeforeRequestHandlerType BeforeRequest;
    public AfterRequestHandlerType AfterRequest;
    public AfterFailedRequestHandlerType AfterFailedRequest;
    public UnableToHandleRequestType UnableToHandleRequest;
  }


  public interface IAPIServer<T> where T : IAPIRequestContext
  {
    void RegisterEndpoint(IAPIEndpoint endpoint);

    Task<byte[]> ProcessMessage(byte[] message, T ctx = default, APIRequestProcessorObserver<T> observer = null);

    PackagesDescriptorsCollection GetPackagesDescriptorsCollection();

    void BeforeRequest(T ctx, string msgId, string endpointName, string methodName, APIEndpointMethodMeta meta, IMessage paramsProto, APIModel @params);

    void AfterRequest(T ctx, string msgId, string endpointName, string methodName, APIEndpointMethodMeta meta, IMessage paramsProto, APIModel @params, IMessage responseProto, APIModel response);

    void AfterFailedRequest(T ctx, string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, string error, Exception exception);
  }
}