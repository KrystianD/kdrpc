using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using proto_base_pb2;

namespace KDRPC
{
  public class BaseAPIServer<T> : IAPIServer<T> where T : IAPIRequestContext
  {
    private readonly List<IAPIEndpoint> _endpoints = new List<IAPIEndpoint>();

    private readonly Dictionary<Tuple<string, string>, IAPIEndpoint> _endpointMethodLookup =
        new Dictionary<Tuple<string, string>, IAPIEndpoint>();

    private readonly PackagesDescriptorsCollection DescriptorsCollection = new PackagesDescriptorsCollection();

    private APIMessage CreateResponse(MessageStatus status,
                                      string messageId,
                                      string errorMessage = "",
                                      Any response = null)
    {
      return new APIMessage {
          Response = new APIResponse {
              Status = (uint)status,
              ErrorMessage = errorMessage,
              Id = messageId,
              Response = response,
          }
      };
    }

    public void RegisterEndpoint(IAPIEndpoint endpoint)
    {
      _endpoints.Add(endpoint);
      DescriptorsCollection.AddDescriptor(endpoint.PackageDescriptor);

      foreach (string method in endpoint.Methods)
        _endpointMethodLookup.Add(new Tuple<string, string>(endpoint.Name, method), endpoint);
    }

    public async Task<byte[]> ProcessMessage(byte[] message, T ctx = default, APIRequestProcessorObserver<T> observer = null)
    {
      APIMessage msg = APIMessage.Parser.ParseFrom(message);
      APIMessage apiResponse = await ProcessMessageInternal(msg, ctx, observer);
      return msg.Request.NoAck ? null : apiResponse.ToByteArray();
    }

    public PackagesDescriptorsCollection GetPackagesDescriptorsCollection() => DescriptorsCollection;

    private async Task<APIMessage> ProcessMessageInternal(APIMessage msg, T ctx, APIRequestProcessorObserver<T> observer = null)
    {
      string endpointName = msg.Request.Endpoint;
      string methodName = msg.Request.Method;

      var key = new Tuple<string, string>(endpointName, methodName);

      IAPIEndpoint endpoint;
      if (!_endpointMethodLookup.TryGetValue(key, out endpoint)) {
        observer?.UnableToHandleRequest(ctx, msg.Request.Id, endpointName, methodName, "method-not-found", null);
        return CreateResponse(MessageStatus.MethodNotFound, msg.Request.Id, "Method not found");
      }

      APIEndpointMethodMeta meta = endpoint.MethodsMeta[methodName];

      IMessage paramsProto = meta.CreateParamsProto();
      paramsProto.MergeFrom(msg.Request.Params.Value);

      APIModel _params = meta.ParamsFromProto(paramsProto);

      // string paramsStr = Utils.FormatValue(_params, new FormatOptions() { Multiline = false, SkipModelName = true, TruncateBlobs = true });
      BeforeRequest(ctx, msg.Request.Id, endpointName, methodName, meta, paramsProto, _params);
      observer?.BeforeRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params);

      object respObj;

      try {
        var task = meta.Method(endpoint, ctx, _params);
        await task.ConfigureAwait(true);
        if (msg.Request.NoAck) return null;

        var resultProperty = task.GetType().GetProperty("Result");
        respObj = resultProperty?.GetValue(task);

        if (respObj != null && respObj.GetType().Name == "VoidTaskResult")
          respObj = null;
      }
      catch (TargetInvocationException e) {
        if (e.InnerException is NotImplementedException) {
          AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "not-implemented", null);
          observer?.AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "not-implemented", null);
          return CreateResponse(MessageStatus.MethodNotImplemented, msg.Request.Id);
        }
        else {
          AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "general-exception", e);
          observer?.AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "general-exception", e);
          return CreateResponse(MessageStatus.GeneralError, msg.Request.Id, "General exception occurred");
        }
      }
      catch (UserErrorException e) {
        AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "user-exception", e);
        observer?.AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "user-exception", e);
        return CreateResponse(MessageStatus.UserError, msg.Request.Id, e.Message);
      }
      catch (NotImplementedException e) {
        AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "not-implemented", null);
        observer?.AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "not-implemented", null);
        return CreateResponse(MessageStatus.MethodNotImplemented, msg.Request.Id);
      }
      catch (Exception e) {
        AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "general-exception", e);
        observer?.AfterFailedRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, "general-exception", e);
        return CreateResponse(MessageStatus.GeneralError, msg.Request.Id, "General exception occurred");
      }

      APIModel response;
      string responseStr;
      if (meta.DefaultReturn) {
        response = meta.CreateResponse();
        meta.ResponseType.GetProperty("DefaultReturn").SetValue(response, respObj);
        responseStr = Utils.FormatValue(respObj, new FormatOptions() { Multiline = false, SkipModelName = false, TruncateBlobs = true });
      }
      else {
        response = (APIModel)respObj;
        responseStr = Utils.FormatValue(respObj, new FormatOptions() { Multiline = false, SkipModelName = true, TruncateBlobs = true });
      }

      var respProto = meta.ResponseToProto(response);

      AfterRequest(ctx, msg.Request.Id, endpointName, methodName, meta, paramsProto, _params, respProto, response);
      observer?.AfterRequest(ctx, msg.Request.Id, endpointName, methodName, paramsProto, _params, respProto, response);

      return CreateResponse(MessageStatus.Success, msg.Request.Id, "", Any.Pack(respProto));
    }

    public virtual void BeforeRequest(T ctx, string msgId, string endpointName, string methodName, APIEndpointMethodMeta meta, IMessage paramsProto, APIModel @params) { }

    public virtual void AfterRequest(T ctx, string msgId, string endpointName, string methodName, APIEndpointMethodMeta meta, IMessage paramsProto, APIModel @params, IMessage responseProto, APIModel response) { }

    public virtual void AfterFailedRequest(T ctx, string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, string error, Exception exception) { }
  }
}