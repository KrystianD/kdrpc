using System;

namespace KDRPC
{
  public class APIEnumValue : IEquatable<APIEnumValue>
  {
    public readonly string Name, Display;
    public readonly int Value;

    public APIEnumValue(string name, int value, string display)
    {
      Name = name;
      Value = value;
      Display = display;
    }

    public bool Equals(APIEnumValue other)
    {
      return Value == other.Value;
    }

    public override bool Equals(object obj)
    {
      switch (obj) {
        case null:
          return false;
        case APIEnumValue e:
          return Value == e.Value;
        default:
          return false;
      }
    }

    public override int GetHashCode()
    {
      return Value.GetHashCode();
    }

    public static bool operator ==(APIEnumValue obj1, APIEnumValue obj2)
    {
      if (ReferenceEquals(obj1, obj2))
        return true;

      if (ReferenceEquals(obj1, null))
        return false;
      if (ReferenceEquals(obj2, null))
        return false;

      return obj1.Equals(obj2);
    }

    public static bool operator !=(APIEnumValue obj1, APIEnumValue obj2)
    {
      return !(obj1 == obj2);
    }
  }
}