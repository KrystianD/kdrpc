using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using KDLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KDRPC
{
  public class FormatOptions
  {
    public bool Multiline = false;
    public bool SkipModelName = false;
    public bool TruncateBlobs = false;

    public HashSet<string> SanitizeFields = new HashSet<string>();

    public FormatOptions Clone()
    {
      return new FormatOptions() {
          Multiline = Multiline,
          SkipModelName = SkipModelName,
          TruncateBlobs = TruncateBlobs,
          SanitizeFields = SanitizeFields,
      };
    }
  }

  public abstract class APIModel
  {
    protected HashSet<IAPIField> existingFields = new HashSet<IAPIField>();

    public APIModel() { }

    public abstract IAPIModelDescriptor GetDescriptor();

    public bool HasField(IAPIField field) => existingFields.Contains(field);
    public bool HasAnyField() => existingFields.Count > 0;

    public object GetField(IAPIField field)
    {
      return GetType().GetProperty(field.NameCS).GetValue(this);
    }

    public void Print() => Console.WriteLine(Format());

    public string Format(bool multiline)
    {
      var options = new FormatOptions();
      options.Multiline = multiline;
      return FormatInner(options, 0);
    }

    public string Format(FormatOptions options = null)
    {
      if (options == null) options = new FormatOptions();
      return FormatInner(options, 0);
    }

    public string FormatInline(FormatOptions options = null)
    {
      if (options == null) {
        options = new FormatOptions();
        options.Multiline = false;
      }

      return FormatInner(options, 0);
    }

    public string FormatInner(FormatOptions options, int indent)
    {
      FormatOptions innerOptions = options.Clone();
      innerOptions.SkipModelName = false;

      string sep = options.Multiline ? ",\n" : ", ";
      string indentStr = options.Multiline ? new string(' ', indent) : "";

      string fieldsStr = GetDescriptor()
                         .GetFields()
                         .Where(HasField)
                         .Select(field =>
                         {
                           var fieldValue = GetField(field);
                           bool toSanitize = innerOptions.SanitizeFields.Contains(field.Name.ToLower());
                           return string.Concat(
                               indentStr,
                               field.NameCS,
                               "=",
                               toSanitize ? "***" : Utils.FormatValue(fieldValue, innerOptions, indent));
                         })
                         .JoinString(sep);

      if (options.SkipModelName) {
        return fieldsStr;
      }
      else {
        return string.Format(options.Multiline ? "{0}:\n{1}" : "{0}({1})", GetDescriptor().GetName(), fieldsStr);
      }
    }
  }
}