using Newtonsoft.Json.Linq;

namespace KDRPC
{
  public class APIQueryFilter
  {
    public JArray Value;

    public APIQueryFilter(JArray value = null)
    {
      Value = value;
    }

    public static implicit operator APIQuery(APIQueryFilter f)
    {
      return new APIQuery { Filter = f };
    }

    public static APIQueryFilter operator &(APIQueryFilter c1, APIQueryFilter c2)
    {
      return new APIQueryFilter(new JArray("$and", c1.Value, c2.Value));
    }

    public static APIQueryFilter operator |(APIQueryFilter c1, APIQueryFilter c2)
    {
      return new APIQueryFilter(new JArray("$or", c1.Value, c2.Value));
    }

    public static APIQueryFilter operator ~(APIQueryFilter c)
    {
      return new APIQueryFilter(new JArray("$not", c.Value));
    }
  }
}