namespace KDRPC
{
  public interface IAPIField
  {
    string Name { get; }
    string NameCS { get; }
    string BaseModel { get; }
    int Id { get; }
    string FieldQueryPath { get; }
  }
}