namespace KDRPC
{
  public abstract class IAPIRequestContext
  {
    public ITransportContext TransportContext;

    public virtual string GetSessionName() => "";
  }
}