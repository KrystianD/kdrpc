namespace KDRPC
{
  public abstract class APIEnum<T> : APIEnumValue
  {
    public APIEnum(string name, int value, string display) : base(name, value, display) { }

    public abstract T GetById(int id);
  }
}