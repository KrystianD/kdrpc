namespace KDRPC
{
  public class APIQuery
  {
    public APIQueryFilter Filter = new APIQueryFilter();
    public APIQueryFieldsList FieldsList = new APIQueryFieldsList();

    public void AddField(IAPIField field)
    {
      FieldsList.Value.Add(field.FieldQueryPath);
    }
  }
}