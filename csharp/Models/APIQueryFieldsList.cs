using Newtonsoft.Json.Linq;

namespace KDRPC
{
  public class APIQueryFieldsList
  {
    public JArray Value = new JArray();

    public APIQueryFieldsList(JArray value = null)
    {
      if (value != null) Value = value;
    }
  }
}