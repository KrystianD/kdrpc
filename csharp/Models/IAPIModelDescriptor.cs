using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace KDRPC
{
  public abstract class IAPIModelDescriptor
  {
    public abstract string GetName();

    public abstract List<IAPIField> GetFields();

    public IEnumerable<string> GetFieldNames() => GetFields().Select(x => x.Name);
  }
}