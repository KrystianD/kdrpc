using System;
using System.Transactions;
using Newtonsoft.Json.Linq;
using Date = System.DateTime;
using DateTime = System.DateTime;
using Decimal = System.Decimal;
using TimeSpan = System.TimeSpan;

namespace KDRPC
{
  public class APIField<T> : IAPIField
  {
    public string Name { get; }
    public string NameCS { get; }
    public string BaseModel { get; }
    public int Id { get; }
    public string FieldQueryPath { get; }

    private static object Convert(T x)
    {
      switch (x) {
        case DateTime dt: return ((DateTimeOffset) dt).ToUnixTimeMilliseconds();
        case APIEnum<T> e: return e.Value;
        default: return x;
      }
    }

    public APIField(string name, string nameCS, int id, string baseModel)
    {
      Name = name;
      NameCS = nameCS;
      Id = id;
      BaseModel = baseModel;
      FieldQueryPath = string.Format("{0}.{1}.{2}", BaseModel, Name, Id);
    }

    public static APIQueryFilter operator ==(APIField<T> field, T x)
    {
      return new APIQueryFilter(new JArray("$eq", field.FieldQueryPath, Convert(x)));
    }

    public static APIQueryFilter operator !=(APIField<T> field, T x)
    {
      return new APIQueryFilter(new JArray("$ne", field.FieldQueryPath, Convert(x)));
    }

    public static APIQueryFilter operator >(APIField<T> field, T x)
    {
      return new APIQueryFilter(new JArray("$gt", field.FieldQueryPath, Convert(x)));
    }

    public static APIQueryFilter operator <(APIField<T> field, T x)
    {
      return new APIQueryFilter(new JArray("$lt", field.FieldQueryPath, Convert(x)));
    }

    protected bool Equals(APIField<T> other)
    {
      return FieldQueryPath == other.FieldQueryPath;
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != this.GetType()) return false;
      return Equals((APIField<T>) obj);
    }

    public override int GetHashCode()
    {
      return FieldQueryPath.GetHashCode();
    }
  }

  public class APIFieldEnum<T> : APIField<T>
  {
    public APIFieldEnum(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldList<T> : APIField<T>
  {
    public APIFieldList(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldMap<T> : APIField<T>
  {
    public APIFieldMap(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldModel<T> : APIField<T>
  {
    public APIFieldModel(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldInteger : APIField<int?>
  {
    public APIFieldInteger(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldInteger64 : APIField<int?>
  {
    public APIFieldInteger64(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldString : APIField<string>
  {
    public APIFieldString(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldDecimal : APIField<decimal>
  {
    public APIFieldDecimal(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldFloat : APIField<float>
  {
    public APIFieldFloat(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldDate : APIField<Date>
  {
    public APIFieldDate(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldDateTime : APIField<DateTime>
  {
    public APIFieldDateTime(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldTime : APIField<TimeSpan>
  {
    public APIFieldTime(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldJSON : APIField<JToken>
  {
    public APIFieldJSON(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldBSON : APIField<JToken>
  {
    public APIFieldBSON(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldBool : APIField<bool>
  {
    public APIFieldBool(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldQuery : APIField<APIQuery>
  {
    public APIFieldQuery(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldBinary : APIField<byte[]>
  {
    public APIFieldBinary(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }

  public class APIFieldUUID : APIField<byte[]>
  {
    public APIFieldUUID(string name, string nameCS, int id, string baseModel) : base(name, nameCS, id, baseModel) { }
  }
}