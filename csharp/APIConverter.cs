﻿using System;

namespace KDRPC
{
  public class APIConverterContext { }

  public class APIConverter { }

  public class BaseAPIConverter { }

  public class APIConverterCSharp : BaseAPIConverter { }

  public class APIConverterEnum<T, E> : BaseAPIConverter where T : APIEnumValue where E : struct, IConvertible
  {
    public Type apiEnum;
    public Type csEnum;


    public void ConvertForward(T participantTypeEnum) { }
  }

  public class APIConverterEnum : BaseAPIConverter
  {
    public Type apiEnum;
    public Type csEnum;

    

    // public object ConvertForward(object obj)
    // {
    //   if (obj.GetType() != apiEnum)
    //     throw new Exception("wrong type");
    //   
    //   
    // }
  }
}