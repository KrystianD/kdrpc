﻿using System.Net;

namespace KDRPC
{
  public class HTTPTransportContext : ITransportContext
  {
    public string ClientId { get; set; }
    
    public HttpListenerContext HttpContext { get; internal set; }

    public IPEndPoint RemoteEndPoint => HttpContext.Request.RemoteEndPoint;
    public IPAddress RealIPAddress;
  }
}