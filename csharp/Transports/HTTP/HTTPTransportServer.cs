﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using KDRPC;
using NLog;

namespace KDRPC
{
  public class HTTPTransportServer<TContext>
      where TContext : IAPIRequestContext, new()
  {
    private readonly Func<HTTPTransportClientHandler<TContext>> _creator;

    public ILogger Logger { get; private set; } = LogManager.CreateNullLogger();
    public IAPIServer<TContext> APIServer { get; }

    private SynchronizationContext _synchronizationContext;

    public HTTPTransportServer(IAPIServer<TContext> apiServer, Func<HTTPTransportClientHandler<TContext>> creator)
    {
      _creator = creator;
      APIServer = apiServer;
    }

    public void SetLogger(ILogger logger)
    {
      Logger = logger.Factory.GetLogger("kdrpc.http");
    }

    public void SetSynchronizationContext(SynchronizationContext ctx)
    {
      _synchronizationContext = ctx;
    }

    public async void Run(int port)
    {
      var listener = new HttpListener();
      Logger.Info($"starting KDRPC HTTP server on port {port}");
      listener.Prefixes.Add($"http://+:{port}/");

      listener.Start();

      while (true) {
        try {
          var httpContext = await listener.GetContextAsync();

          var client = _creator();
          HTTPTransportContext transportContext = new HTTPTransportContext();
          transportContext.HttpContext = httpContext;
          transportContext.RealIPAddress = Utils.GetClientIp(httpContext);

          TContext ctx = new TContext
          {
              TransportContext = transportContext
          };

          client.Initialize(this, ctx);
          client.Handle(httpContext, ctx);
        }
        catch (Exception e) {
          Console.WriteLine(e);
        }
      }
    }

    internal void RunTask(Func<Task> task)
    {
      if (_synchronizationContext == null) {
        task();
      }
      else {
        _synchronizationContext.Post(async _ =>
        {
          await task();
        }, null);
      }
    }
  }
}