﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Fluent;

namespace KDRPC
{
  public class HTTPTransportClientHandler<TContext>
      where TContext : IAPIRequestContext, new()
  {
    public HTTPTransportServer<TContext> TransportServer { get; set; }
    public ILogger Logger { get; set; }

    public TContext Context { get; private set; }
    public HTTPTransportContext TransportContext => (HTTPTransportContext) Context.TransportContext;

    private Dictionary<string, Stopwatch> timers = new Dictionary<string, Stopwatch>();
    private APIRequestProcessorObserver<TContext> _observer = new APIRequestProcessorObserver<TContext>();

    public void Initialize(HTTPTransportServer<TContext> transportServer, TContext ctx)
    {
      TransportServer = transportServer;
      Logger = transportServer.Logger.Factory.GetLogger("kdrpc.http.client");
      Context = ctx;

      _observer.BeforeRequest = (context, msgId, endpointName, methodName, proto, @params) =>
      {
        MappedDiagnosticsLogicalContext.Set("kdrpc_request_id", msgId);
        timers[msgId] = Stopwatch.StartNew();

        Logger?.Trace()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Recevied HTTP KDRPC request to {endpointName}/{methodName}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Write();
      };
      _observer.AfterRequest = (context, msgId, endpointName, methodName, proto, @params, responseProto, response) =>
      {
        long time = timers[msgId].ElapsedMilliseconds;
        timers.Remove(msgId);
        Logger?.Trace()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Sending HTTP KDRPC response from {endpointName}/{methodName} ({time}ms)")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Property("response", @response)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };
      _observer.AfterFailedRequest = (context, msgId, endpointName, methodName, proto, @params, error, exception) =>
      {
        long time = timers[msgId].ElapsedMilliseconds;
        timers.Remove(msgId);
        Logger?.Warn()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Error during handling HTTP KDRPC request to {endpointName}/{methodName} ({time}ms): {error}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Property("error", @error)
              .Exception(exception)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };
      _observer.UnableToHandleRequest = (context, msgId, endpointName, methodName, error, exception) =>
      {
        Logger?.Warn()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Unable to handle HTTP KDRPC request to {endpointName}/{methodName}: {error}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("error", @error)
              .Exception(exception)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };

      OnOpen();
    }

    private void OnOpen()
    {
      string shortId = Utils.GenerateRandomString(4);
      TransportContext.ClientId = $"{TransportContext.RealIPAddress}:{shortId}";
    }

    public void Handle(HttpListenerContext httpContext, TContext ctx)
    {
      TransportServer.RunTask(async () =>
      {
        using (MappedDiagnosticsLogicalContext.SetScoped("kdrpc_client_id", TransportContext.ClientId)) {
          try {
            httpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (httpContext.Request.HttpMethod == "GET" || httpContext.Request.HttpMethod == "HEAD") {
              byte[] resp1 = Encoding.ASCII.GetBytes("OK");
              await httpContext.Response.OutputStream.WriteAsync(resp1, 0, resp1.Length);
              httpContext.Response.OutputStream.Close();
              return;
            }

            await BeforeRequest(ctx);

            byte[] data = new byte[httpContext.Request.ContentLength64];
            await httpContext.Request.InputStream.ReadAsync(data, 0, data.Length);

            byte[] resp = await TransportServer.APIServer.ProcessMessage(data, ctx, _observer);

            httpContext.Response.StatusCode = 200;
            httpContext.Response.SendChunked = true;

            await httpContext.Response.OutputStream.WriteAsync(resp, 0, resp.Length);
            httpContext.Response.OutputStream.Close();

            // Console.WriteLine("processing request done");
          }
          catch (HttpListenerException e) {
            if ((uint) e.HResult == 0x80004005) {
              Logger.Info("Client closed connection (broken pipe)");
            }
            else {
              Logger.Error(e, "ClientHandler error");
              httpContext.Response.StatusCode = 500;
            }
          }
          catch (Exception e) {
            Logger.Error(e, "ClientHandler error");
            httpContext.Response.StatusCode = 500;
          }
        }
      });
    }

    protected virtual async Task BeforeRequest(TContext ctx) { }
  }
}
