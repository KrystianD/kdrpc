﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using MoreLinq;
using Nito.AsyncEx;
using NLog;
using WebSocketSharp.Server;

namespace KDRPC
{
  public abstract class ISharedContext<TContext>
  {
    // public virtual Task Initialize(TContext ctx)
    // {
    //   return Task.CompletedTask;
    // }

    public virtual void Deinitialize()
    {
    }
  }

  public class WebSocketTransportServer<TContext>
      where TContext : IAPIRequestContext, new()
  {
    private class ClientDescriptor
    {
      public WebSocketTransportClientHandler<TContext> Handler;

      public readonly HashSet<string> SubscribedChannels = new HashSet<string>();
      public readonly HashSet<SharedContextKey> UsedSharedContextsKeys = new HashSet<SharedContextKey>();
    }

    public struct SharedContextKey
    {
      public Type Type;
      public string Key;

      public bool Equals(SharedContextKey other)
      {
        return Type == other.Type && string.Equals(Key, other.Key);
      }

      public override bool Equals(object obj)
      {
        if (ReferenceEquals(null, obj)) return false;
        return obj is SharedContextKey key && Equals(key);
      }

      public override int GetHashCode()
      {
        unchecked { return ((Type != null ? Type.GetHashCode() : 0) * 397) ^ (Key != null ? Key.GetHashCode() : 0); }
      }
    }

    private class SharedContextDescriptor
    {
      public SharedContextKey Key;
      public int RefCount = 0;
      public ISharedContext<TContext> Instance;
    }

    private readonly Func<WebSocketTransportClientHandler<TContext>> _creator;

    public ILogger Logger { get; private set; } = LogManager.CreateNullLogger();
    public IAPIServer<TContext> APIServer { get; }

    private readonly AsyncLock _sharedContextCreationLock = new AsyncLock();
    private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

    private readonly List<ClientDescriptor> _clients = new List<ClientDescriptor>();

    private readonly Dictionary<WebSocketTransportClientHandler<TContext>, ClientDescriptor>
        _handlerToClient = new Dictionary<WebSocketTransportClientHandler<TContext>, ClientDescriptor>();

    private readonly Dictionary<string, HashSet<ClientDescriptor>>
        _channelSubscriptionClients = new Dictionary<string, HashSet<ClientDescriptor>>();

    private readonly Dictionary<SharedContextKey, SharedContextDescriptor>
        _sharedContexts = new Dictionary<SharedContextKey, SharedContextDescriptor>();

    public WebSocketTransportServer(IAPIServer<TContext> apiServer, Func<WebSocketTransportClientHandler<TContext>> creator)
    {
      _creator = creator;
      APIServer = apiServer;
    }

    public void SetLogger(ILogger logger)
    {
      Logger = logger.Factory.GetLogger("kdrpc.ws");
    }

    public void SetSynchronizationContext(SynchronizationContext ctx)
    {
      _synchronizationContext = ctx;
    }

    internal void RegisterClient(WebSocketTransportClientHandler<TContext> client)
    {
      var d = new ClientDescriptor() { Handler = client };
      _clients.Add(d);
      _handlerToClient[client] = d;
    }

    internal void UnregisterClient(WebSocketTransportClientHandler<TContext> client)
    {
      var descriptor = _handlerToClient[client];

      _handlerToClient.Remove(client);
      _clients.Remove(descriptor);

      foreach (var channel in descriptor.SubscribedChannels)
        _channelSubscriptionClients[channel].Remove(descriptor);

      foreach (var key in descriptor.UsedSharedContextsKeys) {
        var desc = _sharedContexts[key];
        desc.RefCount--;
        if (desc.RefCount == 0) {
          desc.Instance.Deinitialize();
          _sharedContexts.Remove(key);
        }
      }
    }

    public async Task<T> GetSharedContext<T>(string key,
                                             WebSocketTransportClientHandler<TContext> client,
                                             Func<T, Task> initializer) where T : ISharedContext<TContext>, new()
    {
      var keyType = new SharedContextKey() { Key = key, Type = typeof(T) };

      var descriptor = _handlerToClient[client];

      using (await _sharedContextCreationLock.LockAsync()) {
        if (!_clients.Contains(descriptor))
          throw new ClientNotConnectedException("client disconnected while waiting for lock");

        if (_sharedContexts.TryGetValue(keyType, out var sharedContextDescriptor)) {
          sharedContextDescriptor.RefCount++;
          descriptor.UsedSharedContextsKeys.Add(keyType);
          return (T) sharedContextDescriptor.Instance;
        }
        else {
          T ctx = new T();

          await initializer(ctx);

          if (!_clients.Contains(descriptor)) {
            ctx.Deinitialize();
            throw new ClientNotConnectedException("client disconnected while creating context");
          }

          var desc = new SharedContextDescriptor() {
              Instance = ctx,
              Key = keyType,
              RefCount = 1,
          };

          _sharedContexts[keyType] = desc;
          descriptor.UsedSharedContextsKeys.Add(keyType);
          return ctx;
        }
      }
    }


    internal void SubscribeClientToChannel(string channel, WebSocketTransportClientHandler<TContext> client)
    {
      if (!_handlerToClient.ContainsKey(client))
        throw new ClientNotConnectedException("client is not connected anymore");

      var descriptor = _handlerToClient[client];

      if (!_clients.Contains(descriptor))
        throw new ClientNotConnectedException("client is not connected anymore");

      descriptor.SubscribedChannels.Add(channel);

      if (!_channelSubscriptionClients.TryGetValue(channel, out var channelClients)) {
        channelClients = new HashSet<ClientDescriptor>();
        _channelSubscriptionClients[channel] = channelClients;
      }

      channelClients.Add(descriptor);
    }

    public void PublishInternalEventToChannel(string channel, object @event, WebSocketTransportClientHandler<TContext> skipClient = null)
    {
      if (_channelSubscriptionClients.TryGetValue(channel, out var channelClients))
        channelClients.Where(x => x.Handler != skipClient)
                      .ForEach(client =>
                      {
                        RunTask(async () => await client.Handler.OnInternalEvent(@event));
                      });
    }

    public void PublishInternalEventToAll(object @event)
    {
      _clients.ForEach(client =>
      {
        RunTask(async () => await client.Handler.OnInternalEvent(@event));
      });
    }

    public void Run(int port)
    {
      var wssv = new HttpServer(IPAddress.Any, port);

      wssv.ReuseAddress = true;
      wssv.AddWebSocketService("/", () =>
      {
        var client = _creator();

        WebSocketTransportContext transportContext = new WebSocketTransportContext();

        TContext ctx = new TContext {
            TransportContext = transportContext
        };

        client.Initialize(this, ctx);
        return client;
      });
      wssv.Start();
    }

    internal void RunTask(Func<Task> task)
    {
      if (_synchronizationContext == null) {
        task();
      }
      else {
        _synchronizationContext.Post(async _ =>
        {
          await task();
        }, null);
      }
    }
  }

  public class ClientNotConnectedException : Exception
  {
    public ClientNotConnectedException(string msg) : base(msg)
    {
    }
  }
}