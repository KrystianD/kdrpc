﻿using System.Net;
using WebSocketSharp.Net.WebSockets;

namespace KDRPC
{
  public class WebSocketTransportContext : ITransportContext
  {
    public string ClientId { get; set; }

    public WebSocketContext WebSocketContext { get; internal set; }

    public IPEndPoint RemoteEndPoint => WebSocketContext.ServerEndPoint;

    public IPAddress RealIPAddress;

    // public string ClientId => RemoteEndPoint.ToString();
  }
}