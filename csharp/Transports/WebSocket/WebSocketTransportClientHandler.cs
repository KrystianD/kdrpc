﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Fluent;

namespace KDRPC
{
  public abstract class WebSocketTransportClientHandler<TContext> : WebSocketSharp.Server.WebSocketBehavior
      where TContext : IAPIRequestContext, new()
  {
    private enum HandshakeStatus
    {
      InProgress,
      Success,
      Error,
    }

    public WebSocketTransportServer<TContext> TransportServer { get; set; }
    public ILogger Logger { get; set; }

    private string _sessionKey;
    private HandshakeStatus _handshakeStatus = HandshakeStatus.InProgress;

    public TContext Context { get; private set; }
    public WebSocketTransportContext TransportContext => (WebSocketTransportContext) Context.TransportContext;

    private Dictionary<string, Stopwatch> timers = new Dictionary<string, Stopwatch>();
    private APIRequestProcessorObserver<TContext> _observer = new APIRequestProcessorObserver<TContext>();

    public void Initialize(WebSocketTransportServer<TContext> transportServer, TContext ctx)
    {
      TransportServer = transportServer;
      Logger = transportServer.Logger.Factory.GetLogger("kdrpc.ws.client");
      Context = ctx;

      _observer.BeforeRequest = (context, msgId, endpointName, methodName, proto, @params) =>
      {
        MappedDiagnosticsLogicalContext.Set("kdrpc_request_id", msgId);
        timers[msgId] = Stopwatch.StartNew();

        Logger?.Trace()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Received WebSocket KDRPC request to {endpointName}/{methodName}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Write();
      };
      _observer.AfterRequest = (context, msgId, endpointName, methodName, proto, @params, responseProto, response) =>
      {
        long time = timers[msgId].ElapsedMilliseconds;
        timers.Remove(msgId);
        Logger?.Trace()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Sending WebSocket KDRPC response from {endpointName}/{methodName} ({time}ms)")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Property("response", @response)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };
      _observer.AfterFailedRequest = (context, msgId, endpointName, methodName, proto, @params, error, exception) =>
      {
        long time = timers[msgId].ElapsedMilliseconds;
        timers.Remove(msgId);
        Logger?.Warn()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Error during handling WebSocket KDRPC request to {endpointName}/{methodName} ({time}ms): {error}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("request", @params)
              .Property("error", @error)
              .Exception(exception)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };
      _observer.UnableToHandleRequest = (context, msgId, endpointName, methodName, error, exception) =>
      {
        Logger?.Warn()
              .Message($"[{TransportContext.RealIPAddress}] [#{msgId}] Unable to handle WebSocket KDRPC request to {endpointName}/{methodName}: {error}")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("msg_id", msgId)
              .Property("endpoint_name", endpointName)
              .Property("method_name", methodName)
              .Property("error", @error)
              .Exception(exception)
              .Write();

        MappedDiagnosticsLogicalContext.Remove("kdrpc_request_id");
      };
    }

    protected override void OnOpen()
    {
      TransportServer.RunTask(async () =>
      {
        TransportServer.RegisterClient(this);

        TransportContext.WebSocketContext = base.Context;
        TransportContext.RealIPAddress = Utils.GetClientIp(base.Context);

        string shortId = Utils.GenerateRandomString(4);
        TransportContext.ClientId = $"{TransportContext.RealIPAddress}:{shortId}";

        Logger.Trace()
              .Message($"[{TransportContext.RealIPAddress}] New WebSocket client connected")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Write();
      });
    }

    protected override void OnClose(WebSocketSharp.CloseEventArgs e)
    {
      TransportServer.RunTask(async () =>
      {
        TransportServer.UnregisterClient(this);
        OnClose();

        Logger.Trace()
              .Message($"[{TransportContext.RealIPAddress}] WebSocket client disconnected (Code={e.Code}, Reason={e.Reason})")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Property("ws_code", e.Code)
              .Property("ws_reason", e.Reason)
              .Write();
      });
    }

    protected virtual void OnClose()
    {
    }

    protected override void OnError(WebSocketSharp.ErrorEventArgs e)
    {
      TransportServer.RunTask(async () =>
      {
        Logger.Error()
              .Message($"[{TransportContext.RealIPAddress}] WebSocket client failed ({e.Message})")
              .Property("remote_ip", TransportContext.RealIPAddress)
              .Property("kdrpc_client_id", TransportContext.ClientId)
              .Exception(e.Exception)
              .Write();
      });
    }

    protected override void OnMessage(WebSocketSharp.MessageEventArgs e)
    {
      TransportServer.RunTask(async () =>
      {
        using (MappedDiagnosticsLogicalContext.SetScoped("kdrpc_client_id", TransportContext.ClientId)) {
          using (MappedDiagnosticsLogicalContext.SetScoped("kdrpc_session_key", _sessionKey)) {
            using (MappedDiagnosticsLogicalContext.SetScoped("remote_ip", TransportContext.RealIPAddress)) {
              using (SetupLogData()) {
                try {
                  if (e.IsText) {
                    var data = JObject.Parse(e.Data);
                    JObject resp = await ProcessCommand(data);
                    if (resp != null)
                      Send(resp.ToString());

                    if (resp?["type"].Value<string>() != "pong")
                      Logger.Trace().Message($"[{TransportContext.RealIPAddress}] Sending WebSocket command response")
                            .Property("command", data)
                            .Property("response", resp)
                            .Write();
                  }
                  else if (e.IsBinary) {
                    byte[] resp = await ProcessAPIMessage(e.RawData);
                    if (resp != null)
                      Send(resp);
                  }
                }
                catch (Exception e2) {
                  Logger.Error(e2);
                  base.Context.WebSocket.Close(WebSocketSharp.CloseStatusCode.InvalidData);
                }
              }
            }
          }
        }
      });
    }

    private async Task<JObject> ProcessCommand(JObject data)
    {
      string type = data.Value<string>("type");

      if (type == "ping") {
        return new JObject { { "type", "pong" } };
      }

      Logger.Trace().Message($"[{TransportContext.RealIPAddress}] New WebSocket command received ({type})")
            .Property("command", data)
            .Write();

      if (type == "handshake") {
        try {
          _sessionKey = data["session"].Value<string>();
          Logger.Trace().Message($"[{TransportContext.RealIPAddress}] Performing handshake (#{_sessionKey})...")
                .Property("kdrpc_session_key", _sessionKey)
                .Property("command", data)
                .Write();

          JToken respData = await PerformHandshake(Context, _sessionKey, data["data"]);

          Logger.Trace().Message($"[{TransportContext.RealIPAddress}] Handshake response (#{_sessionKey}) ({Utils.LimitText(respData?.ToString(Formatting.None), 100)})")
                .Property("kdrpc_session_key", _sessionKey)
                .Property("command", data)
                .Property("response", respData)
                .Write();

          _handshakeStatus = HandshakeStatus.Success;

          return new JObject {
              { "type", "handshake_response" },
              { "result", "ok" },
              { "data", respData },
          };
        }
        catch (Exception e) {
          _handshakeStatus = HandshakeStatus.Error;

          Logger.Error().Message($"[{TransportContext.RealIPAddress}] WebSocket handshake error")
                .Property("command", data)
                .Exception(e)
                .Write();

          return new JObject {
              { "type", "handshake_response" },
              { "result", "error" },
          };
        }
      }

      return null;
    }

    private async Task<byte[]> ProcessAPIMessage(byte[] data)
    {
      if (_handshakeStatus != HandshakeStatus.Success) {
        base.Context.WebSocket.Close(1000, "invalid handshake");
        return null;
      }

      await OnBeforeRequest(Context);

      return await TransportServer.APIServer.ProcessMessage(data, Context, _observer);
    }

    public void CloseConnection(int code)
    {
      base.Context.WebSocket.Close((ushort) code, "custom");
    }

    protected abstract Task<JToken> PerformHandshake(TContext ctx, string session, JToken handshakeData);

    protected virtual Task OnBeforeRequest(TContext context) => Task.CompletedTask;

    public virtual async Task OnInternalEvent(object @event)
    {
    }

    private class EmptyDisposable : IDisposable
    {
      public void Dispose()
      {
      }

      public static EmptyDisposable Empty = new EmptyDisposable();
    }

    protected virtual IDisposable SetupLogData()
    {
      return EmptyDisposable.Empty;
    }

    protected void SubscribeChannel(string channel)
    {
      TransportServer.SubscribeClientToChannel(channel, this);
    }

    public void PublishEvent(APIEvent @event)
    {
      var col = TransportServer.APIServer.GetPackagesDescriptorsCollection();
      var msg = col.EventToMessage(@event);

      Logger?.Trace()
            .Message($"[{TransportContext.RealIPAddress}] Publishing KDRPC WebSocket event")
            .Property("remote_ip", TransportContext.RealIPAddress)
            .Property("client_id", TransportContext.ClientId)
            .Property("event", @event)
            .Write();

      base.Context.WebSocket.Send(msg.ToByteArray());
    }
  }
}