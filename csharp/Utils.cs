﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebSocketSharp.Net.WebSockets;

namespace KDRPC
{
  public static class Utils
  {
    public static HashSet<IPAddress> TrustedProxies = new HashSet<IPAddress>() {
        IPAddress.Loopback,
    };

    public static string CreateGuid()
    {
      var guid = Guid.Empty;
      while (guid == Guid.Empty)
        guid = Guid.NewGuid();
      return guid.ToString();
    }

    public static string GenerateShortUUID()
    {
      return ShortGuid.NewShortGuid().Value;
    }

    public static string GenerateRandomString(
        int length,
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
    {
      var stringChars = new char[length];
      var random = new Random();

      for (var i = 0; i < stringChars.Length; i++)
        stringChars[i] = chars[random.Next(chars.Length)];

      return new string(stringChars);
    }

    public static string LimitText(string text, int maxLength)
    {
      if (text == null)
        return "(null)";
      if (text.Length > maxLength - 3)
        text = text.Substring(0, maxLength - 3) + "...";
      return text;
    }

    public static MethodInfo FindMethod(Type type, string name)
    {
      return type.GetMethods().FirstOrDefault(method => method.Name == name);
    }

    public static async Task<T> WaitFutureTimeout<T>(Task<T> task, int timeout)
    {
      using (var timeoutCancellationTokenSource = new CancellationTokenSource()) {
        var completed = await Task.WhenAny(task, Task.Delay(timeout, timeoutCancellationTokenSource.Token));
        if (completed == task) {
          timeoutCancellationTokenSource.Cancel();
          return await task;
        }
        else {
          throw new TimeoutException();
        }
      }
    }

    public static string FormatValue(object v, FormatOptions options, int indent = 0)
    {
      string str;
      switch (v) {
        case APIModel m:
          var cnt = m.FormatInner(options, indent + 2);
          // if (options.Multiline)
          //   cnt = "\n" + cnt;
          return cnt;
        case APIEnumValue e:
          return e.Name;
        case string e:
          if (options.TruncateBlobs && e.Length > 10 * 1024)
            return $"<string:{e.Length}>";
          else
            return $"\"{e}\"";
        case JToken e:
          if (options.TruncateBlobs)
            str = $"<json:{e.ToString(Formatting.None).Length}>";
          else
            str = e.ToString(options.Multiline ? Formatting.Indented : Formatting.None);
          return str;
        case byte[] e:
          // if (options.TruncateBlobs)
          str = $"<binary:{e.Length}>";
          // else
          // str = e.ToString();
          return str;
        // case IEnumerable<object> e:
        // 	return string.Join(", ", e.Select(FormatValue));
        case IDictionary e:
          return string.Format("{{{0}}}",
                               String.Join(", ", e.Keys.Cast<object>().Select(k => $"{FormatValue(k, options, indent)}:{FormatValue(e[k], options, indent)}")));
        case IEnumerable e:
          return string.Format("[{0}]", string.Join(",", e.Cast<object>().Select(x => FormatValue(x, options, indent))));
        case null:
          return "null";
        default:
          return v.ToString();
      }
    }

    public static IPAddress GetClientIp(HttpListenerContext httpContext, HashSet<IPAddress> trustedProxies = null)
    {
      return GetClientIp(httpContext.Request.RemoteEndPoint?.Address,
                         httpContext.Request.Headers["X-Forwarded-For"]?.Split(","),
                         httpContext.Request.Headers["X-Real-IP"],
                         trustedProxies);
    }

    public static IPAddress GetClientIp(WebSocketContext httpContext, HashSet<IPAddress> trustedProxies = null)
    {
      try {
        return GetClientIp(httpContext.UserEndPoint?.Address,
                           httpContext.Headers["X-Forwarded-For"]?.Split(","),
                           httpContext.Headers["X-Real-IP"],
                           trustedProxies);
      }
      catch (Exception e) {
        return null;
      }
    }

    private static IPAddress GetClientIp(IPAddress clientIp, IReadOnlyList<string> xForwardedFor, string realIp, HashSet<IPAddress> trustedProxies = null)
    {
      if (trustedProxies == null)
        trustedProxies = TrustedProxies;

      if (trustedProxies.Contains(clientIp)) {
        if (xForwardedFor != null)
          return IPAddress.Parse(xForwardedFor[0]);
        else if (realIp != null)
          return IPAddress.Parse(realIp);
      }

      return clientIp;
    }
  }
}