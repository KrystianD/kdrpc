using System;
using System.IO;
using Google.Protobuf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Msgpack;

namespace KDRPC
{
  public static class BaseConverters
  {
    public static T Enum_to_proto<V, T>(APIEnumValue x) where T : new()
    {
      dynamic p = new T();
      if (x == null)
        p.IsNull = true;
      else
        p.Value = (V) Enum.ToObject(typeof(V), x.Value);
      return (T) p;
    }

    public static proto_base_pb2.String String_to_proto(string x)
    {
      var p = new proto_base_pb2.String();
      if (x == null)
        p.IsNull = true;
      else
        p.Value = x;
      return p;
    }

    public static proto_base_pb2.Int32 Int32_to_proto(int? x)
    {
      var p = new proto_base_pb2.Int32();
      if (!x.HasValue)
        p.IsNull = true;
      else
        p.Value = x.Value;
      return p;
    }

    public static proto_base_pb2.Int64 Int64_to_proto(long? x)
    {
      var p = new proto_base_pb2.Int64();
      if (!x.HasValue)
        p.IsNull = true;
      else
        p.Value = x.Value;
      return p;
    }

    public static proto_base_pb2.Decimal Decimal_to_proto(decimal? x)
    {
      var p = new proto_base_pb2.Decimal();
      if (!x.HasValue)
        p.IsNull = true;
      else
        p.Value = x.Value.ToString();
      return p;
    }

    public static proto_base_pb2.Float Float_to_proto(float? x)
    {
      var p = new proto_base_pb2.Float();
      if (!x.HasValue)
        p.IsNull = true;
      else
        p.Value = x.Value;
      return p;
    }

    public static proto_base_pb2.Boolean Boolean_to_proto(bool? x)
    {
      var p = new proto_base_pb2.Boolean();
      if (!x.HasValue)
        p.IsNull = true;
      else
        p.Value = x.Value;
      return p;
    }

    public static proto_base_pb2.Date Date_to_proto(DateTime? x)
    {
      var p = new proto_base_pb2.Date();
      if (!x.HasValue)
        p.IsNull = true;
      else {
        p.Year = x.Value.Year;
        p.Month = x.Value.Month;
        p.Day = x.Value.Day;
      }

      return p;
    }

    public static proto_base_pb2.DateTime DateTime_to_proto(DateTime? x)
    {
      var p = new proto_base_pb2.DateTime();
      if (!x.HasValue)
        p.IsNull = true;
      else {
        p.Date = Date_to_proto(x.Value);
        p.Time = Time_to_proto(x.Value.TimeOfDay);
      }

      return p;
    }

    public static proto_base_pb2.Time Time_to_proto(TimeSpan? x)
    {
      var p = new proto_base_pb2.Time();
      if (!x.HasValue)
        p.IsNull = true;
      else {
        p.Hour = x.Value.Hours;
        p.Minute = x.Value.Minutes;
        p.Second = x.Value.Seconds;
        p.Microsecond = x.Value.Milliseconds * 1000;
        p.Timezone = 0;
      }

      return p;
    }


    public static proto_base_pb2.JSON JSON_to_proto(JToken x)
    {
      var p = new proto_base_pb2.JSON();
      if (x == null)
        p.IsNull = true;
      else
        p.Value = x.ToString(Formatting.None);
      return p;
    }

    public static proto_base_pb2.BSON BSON_to_proto(JToken x)
    {
      var p = new proto_base_pb2.BSON();
      if (x == null) {
        p.IsNull = true;
      }
      else {
        var memoryStream = new MemoryStream();
        var serializer = new JsonSerializer();
        
        var writer = new MessagePackWriter(memoryStream);
        writer.WriteDateTimeAsString = true;
        serializer.Serialize(writer, x);
        memoryStream.Seek(0, SeekOrigin.Begin);
        p.Value = ByteString.FromStream(memoryStream);
      }

      return p;
    }

    public static proto_base_pb2.Binary Binary_to_proto(byte[] x)
    {
      var p = new proto_base_pb2.Binary();
      if (x == null)
        p.IsNull = true;
      else
        p.Value = ByteString.CopyFrom(x);
      return p;
    }

    public static proto_base_pb2.APIQuery APIQuery_to_proto(APIQuery x)
    {
      var p = new proto_base_pb2.APIQuery();
      if (x.Filter != null) p.Filter = x.Filter.Value.ToString(Formatting.None);
      if (x.FieldsList != null) p.FieldsList = x.FieldsList.Value.ToString(Formatting.None);
      return p;
    }

    public static proto_base_pb2.UUID UUID_to_proto(Guid? x)
    {
      var p = new proto_base_pb2.UUID();
      if (!x.HasValue) {
        p.IsNull = true;
      }
      else {
        var data = x.Value.ToByteArray();
        p.Value = ByteString.CopyFrom(
            data[3], data[2], data[1], data[0],
            data[5], data[4],
            data[7], data[6],
            data[8], data[9],
            data[10], data[11], data[12], data[13], data[14], data[15]);
      }

      return p;
    }

    // From proto
    public static T Enum_from_proto<T>(dynamic p, APIEnum<T> cls) where T : class
    {
      return p.IsNull ? null : cls.GetById((int) p.Value);
    }


    public static string String_from_proto(proto_base_pb2.String x)
    {
      return x.IsNull ? null : x.Value;
    }

    public static int? Int32_from_proto(proto_base_pb2.Int32 x)
    {
      return x.IsNull ? (int?) null : x.Value;
    }

    public static long? Int64_from_proto(proto_base_pb2.Int64 x)
    {
      return x.IsNull ? (long?) null : x.Value;
    }

    public static decimal? Decimal_from_proto(proto_base_pb2.Decimal x)
    {
      return x.IsNull ? (decimal?) null : decimal.Parse(x.Value);
    }

    public static float? Float_from_proto(proto_base_pb2.Float x)
    {
      return x.IsNull ? (float?) null : x.Value;
    }

    public static bool? Boolean_from_proto(proto_base_pb2.Boolean x)
    {
      return x.IsNull ? (bool?) null : x.Value;
    }

    public static DateTime? Date_from_proto(proto_base_pb2.Date x)
    {
      return x.IsNull ? (DateTime?) null : new DateTime(x.Year, x.Month, x.Day);
    }

    public static DateTime? DateTime_from_proto(proto_base_pb2.DateTime x)
    {
      if (x.IsNull) return null;
      return new DateTime(x.Date.Year, x.Date.Month, x.Date.Day, x.Time.Hour, x.Time.Minute, x.Time.Second,
                          x.Time.Microsecond / 1000, DateTimeKind.Utc);
    }

    public static TimeSpan? Time_from_proto(proto_base_pb2.Time x)
    {
      if (x.IsNull) return null;
      return new TimeSpan(0, x.Hour, x.Minute, x.Second, x.Microsecond / 1000);
    }


    public static JToken JSON_from_proto(proto_base_pb2.JSON x)
    {
      return x.IsNull ? null : JsonConvert.DeserializeObject<JToken>(x.Value);
    }

    public static JToken BSON_from_proto(proto_base_pb2.BSON x)
    {
      if (x.IsNull) return null;

      var serializer = new JsonSerializer();
      var reader = new MessagePackReader(new MemoryStream(x.Value.ToByteArray()));

      return serializer.Deserialize<JToken>(reader);
    }

    public static byte[] Binary_from_proto(proto_base_pb2.Binary x)
    {
      return x.IsNull ? null : x.Value.ToByteArray();
    }

    public static Guid? UUID_from_proto(proto_base_pb2.UUID x)
    {
      if (x.IsNull) return null;

      var data = x.Value.ToByteArray();
      return new Guid(new[] {
          data[3], data[2], data[1], data[0],
          data[5], data[4],
          data[7], data[6],
          data[8], data[9],
          data[10], data[11], data[12], data[13], data[14], data[15]
      });
    }

    public static APIQuery APIQuery_from_proto(proto_base_pb2.APIQuery x)
    {
      var p = new APIQuery();
      if (x.Filter != "") p.Filter = new APIQueryFilter((JArray) JsonConvert.DeserializeObject(x.Filter));
      if (x.FieldsList != "") p.FieldsList = new APIQueryFieldsList((JArray) JsonConvert.DeserializeObject(x.Filter));
      return p;
    }
  }
}