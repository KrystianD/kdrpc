﻿using System;

namespace KDRPC
{
  public class MethodNotFoundException : Exception
  {
    public MethodNotFoundException() { }

    public MethodNotFoundException(string message)
        : base(message) { }

    public MethodNotFoundException(string message, Exception inner)
        : base(message, inner) { }
  }

  public class MethodNotImplementedException : Exception
  {
    public MethodNotImplementedException() { }

    public MethodNotImplementedException(string message)
        : base(message) { }

    public MethodNotImplementedException(string message, Exception inner)
        : base(message, inner) { }
  }

  public class GeneralErorrException : Exception
  {
    public GeneralErorrException() { }

    public GeneralErorrException(string message)
        : base(message) { }

    public GeneralErorrException(string message, Exception inner)
        : base(message, inner) { }
  }

  public class UserErrorException : Exception
  {
    public UserErrorException() { }

    public UserErrorException(string message)
        : base(message) { }

    public UserErrorException(string message, Exception inner)
        : base(message, inner) { }
  }
}