﻿using System;
using System.Collections.Generic;
using Google.Protobuf;
using Google.Protobuf.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KDRPC
{
  public class ProtobufMessageWrapper { }

  public abstract class BasePackageDescriptor
  {
    public abstract BasePackageDescriptor GetInstance();
    public abstract HashSet<Type> GetEvents();
    public abstract Google.Protobuf.IMessage EventToProto(APIEvent ev);
    public abstract Google.Protobuf.IMessage ModelToProto(APIModel ev);
    public abstract APIEvent EventFromProto(Google.Protobuf.IMessage proto, string eventName);
    public abstract APIEvent EventFromMessage(proto_base_pb2.APIMessage msg);
    public abstract T ModelFromJson<T>(string json) where T : APIModel;

    // Message
    public string ModelToJson(APIModel model, bool indent = false)
    {
      var json = GetProtobufJsonFormatter().Format(ModelToProto(model));
      if (indent)
        json = JToken.Parse(json).ToString(Formatting.Indented);
      return json;
    }

    // Event
    public bool CanConvertEvent<T>(T obj)
    {
      return GetEvents().Contains(obj.GetType());
    }

    public APIEvent EventFromMessageBytes(byte[] data)
    {
      var msg = proto_base_pb2.APIMessage.Parser.ParseFrom(data);
      return EventFromMessage(msg);
    }

    public APIEvent EventFromJsonMessage(string json)
    {
      var protoMessage = GetProtobufJsonParser().Parse<proto_base_pb2.APIMessage>(json);
      return EventFromMessage(protoMessage);
    }

    public string EventToJsonMessage(APIEvent @event, bool indent = false)
    {
      var json = GetProtobufJsonFormatter().Format(EventToMessage(@event));
      if (indent)
        json = JToken.Parse(json).ToString(Formatting.Indented);
      return json;
    }

    public proto_base_pb2.APIMessage EventToMessage(APIEvent ev)
    {
      var msg = new proto_base_pb2.APIMessage() {
          Event = new proto_base_pb2.APIEvent() {
              Name = ev.GetType().Name
          }
      };
      Google.Protobuf.IMessage d = EventToProto(ev);
      msg.Event.Payload = Google.Protobuf.WellKnownTypes.Any.Pack(d);
      return msg;
    }

    public byte[] EventToMessageBytes(APIEvent ev)
    {
      return EventToMessage(ev).ToByteArray();
    }

    public abstract TypeRegistry GetProtobufTypeRegistry();
    public abstract Google.Protobuf.JsonParser GetProtobufJsonParser();
    public abstract Google.Protobuf.JsonFormatter GetProtobufJsonFormatter();
  }
}