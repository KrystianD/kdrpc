using System.Collections.Generic;

namespace KDRPC
{
  public interface IAPIEndpoint
  {
    string Name { get; }
    List<string> Methods { get; }
    Dictionary<string, APIEndpointMethodMeta> MethodsMeta { get; }

    BasePackageDescriptor PackageDescriptor { get; }
  }
}