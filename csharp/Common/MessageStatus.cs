namespace KDRPC
{
  public enum MessageStatus : int
  {
    Success = 0,
    MethodNotFound = 1,
    MethodNotImplemented = 2,
    GeneralError = 3,
    UserError = 4,
  }
}