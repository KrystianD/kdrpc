using System;
using System.Threading.Tasks;
using Google.Protobuf;

namespace KDRPC
{
  public class APIEndpointMethodMeta
  {
    public readonly Type ResponseType;
    public readonly Type ParamsTypeProto;
    public readonly Func<IMessage, APIModel> ParamsFromProto;
    public readonly Func<APIModel, IMessage> ResponseToProto;
    public readonly Func<object, object, object, Task> Method;

    public readonly bool DefaultReturn;

    public APIEndpointMethodMeta(
        Type responseType,
        Type paramsTypeProto,
        Func<IMessage, APIModel> paramsFromProto,
        Func<APIModel, IMessage> responseToProto,
        bool defaultReturn,
        Func<object, object, object, Task> method)
    {
      ResponseType = responseType;
      ParamsTypeProto = paramsTypeProto;
      ParamsFromProto = paramsFromProto;
      ResponseToProto = responseToProto;
      DefaultReturn = defaultReturn;
      Method = method;
    }

    public APIModel CreateResponse() => (APIModel) Activator.CreateInstance(ResponseType);
    public IMessage CreateParamsProto() => (IMessage) Activator.CreateInstance(ParamsTypeProto);
  }
}