﻿using System;
using System.Collections.Generic;
using Google.Protobuf.Reflection;

namespace KDRPC
{
  public class PackagesDescriptorsCollection
  {
    internal List<BasePackageDescriptor> _descriptors = new List<BasePackageDescriptor>();

    public void AddDescriptor(BasePackageDescriptor descriptor)
    {
      _descriptors.Add(descriptor);
    }

    public proto_base_pb2.APIMessage EventToMessage(APIEvent ev)
    {
      foreach (var descriptor in _descriptors)
        if (descriptor.CanConvertEvent(ev))
          return descriptor.EventToMessage(ev);
      throw new Exception("no converter available");
    }
  }
}