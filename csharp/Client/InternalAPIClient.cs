using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using proto_base_pb2;

namespace KDRPC
{
  public abstract class InternalAPIClient : IAPIClient
  {
    private ConcurrentDictionary<string, TaskCompletionSource<APIMessage>> listeners =
        new ConcurrentDictionary<string, TaskCompletionSource<APIMessage>>();

    public async Task<Any> Call(string msgId, string endpointName, string methodName,
                                Any packedParams, APIModel @params,
                                int timeout,
                                bool noExpire,
                                bool noAck)
    {
      if (timeout == -1)
        timeout = 60000;

      var apiRequestProto = new APIMessage
      {
          Request = new APIRequest
          {
              Id = msgId,
              Endpoint = endpointName,
              Method = methodName,
              NoAck = noAck,
              Params = packedParams
          }
      };

      BeforeRequest(msgId, endpointName, methodName, packedParams, @params);

      APIMessage apiResponseMsgProto =
          await SendRequestInternal(apiRequestProto, msgId, endpointName, methodName, timeout, noExpire, noAck)
              .ConfigureAwait(false);

      if (noAck) return null;

      switch ((MessageStatus) apiResponseMsgProto.Response.Status) {
        case MessageStatus.Success:
          return apiResponseMsgProto.Response.Response;
        case MessageStatus.MethodNotFound:
          AfterFailedRequest(msgId, endpointName, methodName, packedParams, @params, "method-not-found");
          throw new MethodNotFoundException(apiResponseMsgProto.Response.ErrorMessage);
        case MessageStatus.MethodNotImplemented:
          AfterFailedRequest(msgId, endpointName, methodName, packedParams, @params, "method-not-implemented");
          throw new MethodNotImplementedException(apiResponseMsgProto.Response.ErrorMessage);
        case MessageStatus.GeneralError:
          AfterFailedRequest(msgId, endpointName, methodName, packedParams, @params, "general-exception");
          throw new GeneralErorrException(apiResponseMsgProto.Response.ErrorMessage);
        case MessageStatus.UserError:
          AfterFailedRequest(msgId, endpointName, methodName, packedParams, @params, "user-exception");
          throw new UserErrorException(apiResponseMsgProto.Response.ErrorMessage);
        default: throw new ArgumentOutOfRangeException();
      }
    }

    private async Task<APIMessage> SendRequestInternal(APIMessage apiRequestProto,
                                                       string msgId, string endpointName, string methodName,
                                                       int timeout, bool noExpire, bool noAck)
    {
      byte[] apiRequestBytes = apiRequestProto.ToByteArray();

      if (noAck) {
        await SendRequest(apiRequestBytes, msgId, endpointName, methodName, timeout, noExpire, noAck: true);
        return null;
      }
      else {
        var ts = new TaskCompletionSource<APIMessage>();
        listeners[msgId] = ts;

        await SendRequest(apiRequestBytes, msgId, endpointName, methodName, timeout, noExpire, noAck: false);

        try {
          return await Utils.WaitFutureTimeout(ts.Task, timeout);
        }
        finally {
          //          Console.WriteLine("SendRequest2 THREAD #{0}", Thread.CurrentThread.ManagedThreadId);
          listeners.TryRemove(msgId, out _);
        }
      }
    }

    public abstract string GenerateMessageId();

    protected abstract Task SendRequest(byte[] request, string msgId, string endpointName, string methodName,
                                        int timeout, bool noExpire, bool noAck);


    public virtual void BeforeRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params) { }

    public virtual void AfterRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, IMessage responseProto, APIModel response) { }

    public virtual void AfterFailedRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, string error) { }

    public void ProcessMessage(byte[] apiResponseBytes)
    {
      var apiResponseProto = APIMessage.Parser.ParseFrom(apiResponseBytes);

      var msgId = apiResponseProto.Response.Id;

      if (listeners.TryGetValue(msgId, out var listener))
        listener.SetResult(apiResponseProto);
    }
  }
}