namespace KDRPC
{
  public interface IEndpointClientStub
  {
    IAPIClient Client { get; }
    string Name { get; }
  }
}