using System;
using System.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;

namespace KDRPC
{
  public interface IAPIClient
  {
    Task<Any> Call(string msgId, string endpointName, string methodName,
                   Any packedParams, APIModel @params,
                   int timeout, bool noExpire, bool noAck);

    string GenerateMessageId();

    void BeforeRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params);

    void AfterRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, IMessage responseProto, APIModel response);

    void AfterFailedRequest(string msgId, string endpointName, string methodName, IMessage paramsProto, APIModel @params, string error);
  }
}