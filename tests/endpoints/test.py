import sys
import unittest

import asyncio

sys.path.append(".")
sys.path.append("kdrpc_gen/gen_py")

import kdrpc
from .kdrpc_gen import interfaces_app


class MyAPITestEndpoint(interfaces_app.TestEndpoint):

    async def TestMethod(self, params: interfaces_app.TestEndpointTestMethodParams):
        pass

    async def TestMethodReturn(self, params: interfaces_app.TestEndpointTestMethodReturnParams) -> kdrpc.Optional[int]:
        return 3

    async def TestMethodModel(self, params: interfaces_app.TestEndpointTestMethodModelParams) -> kdrpc.Optional[
        interfaces_app.AppModel1]:
        return interfaces_app.AppModel1.create(a=33)


class MyAPIServer(kdrpc.BaseAPIServer):
    def __init__(self):
        super().__init__()
        self.register(MyAPITestEndpoint())


srv = MyAPIServer()


class MyAPIClient(interfaces_app.BaseAPIClient):
    async def send_request(self, request: bytes, endpoint_name: str, timeout: int, no_expire: bool,
                           no_ack: bool) -> bytes:
        return await srv.process_message(request)


def async_run(x):
    return asyncio.get_event_loop().run_until_complete(x)


class ParsingTest(unittest.TestCase):
    def test_ep(self):
        ep = MyAPIClient()
        r = async_run(ep.TestEndpoint.TestMethod(2))
        self.assertIsNone(r)

    def test_ep_return(self):
        ep = MyAPIClient()
        r = async_run(ep.TestEndpoint.TestMethodReturn(2))
        self.assertEqual(r, 3)

    def test_ep_model(self):
        m = interfaces_app.AppModel1()
        m.a = 10
        ep = MyAPIClient()
        r = async_run(ep.TestEndpoint.TestMethodModel(m))
        self.assertEqual(r.a, 33)


if __name__ == '__main__':
    unittest.main()
