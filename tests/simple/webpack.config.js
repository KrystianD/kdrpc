var path = require('path');
var webpack = require('webpack');

const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const {CommonsChunkPlugin} = require('webpack').optimize;
const {AngularCompilerPlugin} = require('@ngtools/webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var isTest = false;
var isTestWatch = false;
var isProd = true;

const entryPoints = ["inline", "polyfills", "sw-register", "styles", "vendor", "main"];

module.exports = function makeWebpackConfig() {
  var config = {};

  config.devtool = 'inline-source-map';

  config.entry = {
    'main': './src/main.ts',
    "polyfills": [
      "babel-polyfill",
      "./src/polyfills.ts"
    ],
  };

  config.resolveLoader = {
    "modules": [
      path.resolve(__dirname, 'node_modules/'),
      "./node_modules"
    ],
  };

  config.output = isTest ? {} : {
    path: root('dist'),
    publicPath: isProd ? '/' : 'http://localhost:8080/',
    filename: isProd ? 'js/[name].[hash].js' : 'js/[name].js',
    chunkFilename: isProd ? '[id].[hash].chunk.js' : '[id].chunk.js'
  };

  config.resolve = {
    /*alias: {
        kdrpc: path.resolve(__dirname, '../../ts/src/index.ts'),
    },*/
    extensions: ['.ts', '.js', '.html'],
  };

  config.module = {
    rules: [
      {
        "test": /\.html$/,
        "loader": "raw-loader"
      },
      {
        test: /\.ts$/,
        loaders: [
          'babel-loader',
          '@ngtools/webpack'
        ]
      }
    ]
  };

  config.plugins = [
    new webpack.NoEmitOnErrorsPlugin(),
    new ProgressPlugin(),

    /*new CommonsChunkPlugin({
        "name": [
            "inline"
        ],
        "minChunks": null
    }),
    new CommonsChunkPlugin({
        "name": [
            "vendor"
        ],
        "minChunks": (module) => {
            return module.resource;
        },
        "chunks": [
            "main"
        ]
    }),
    new CommonsChunkPlugin({
        "name": [
            "main"
        ],
        "minChunks": 2,
        "async": "common"
    }),*/
    new HtmlWebpackPlugin({
      "template": "./src/index.html",
      "filename": "./index.html",
      "hash": false,
      "inject": true,
      "compile": true,
      "favicon": false,
      "minify": false,
      "cache": true,
      "showErrors": true,
      "chunks": "all",
      "excludeChunks": [],
      "title": "Webpack App",
      "xhtml": true,
      "chunksSortMode": function sort(left, right) {
        let leftIndex = entryPoints.indexOf(left.names[0]);
        let rightindex = entryPoints.indexOf(right.names[0]);
        if (leftIndex > rightindex) {
          return 1;
        }
        else if (leftIndex < rightindex) {
          return -1;
        }
        else {
          return 0;
        }
      }
    }),

    new AngularCompilerPlugin({
      "mainPath": "main.ts",
      "platform": 0,
      "hostReplacementPaths": {
        "environments/environment.ts": "environments/environment.ts"
      },
      "sourceMap": true,
      "tsConfigPath": "src/tsconfig.json",
      "skipCodeGeneration": true,
      "compilerOptions": {}
    })
  ];

  config.devServer = {
    contentBase: './src/public',
    historyApiFallback: true,
    quiet: false,
    stats: 'normal' // none (or false), errors-only, minimal, normal (or true) and verbose
  };

  return config;
}();

// Helper functions
function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [__dirname].concat(args));
}
