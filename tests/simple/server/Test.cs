﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KDRPC;
using Nancy.Routing.Trie.Nodes;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.Targets;

namespace Test
{
  public class MyRequestContext : IAPIRequestContext { }

  public class MyWebSocketClientHandler : WebSocketTransportClientHandler<MyRequestContext>
  {
    protected override async Task<JToken> PerformHandshake(MyRequestContext ctx, string session, JToken handshakeData)
    {
      SubscribeChannel("test");
      await Task.Delay(500);
      Console.WriteLine("PerformHandshake {0}, |{1}|", handshakeData, handshakeData["token"].ToString());

      if (handshakeData["token"].ToString() != "KD")
        throw new Exception("error");

      return null;
    }
  }

  public class MyHTTPClientHandler : HTTPTransportClientHandler<MyRequestContext>
  {
    protected override async Task BeforeRequest(MyRequestContext ctx)
    {
      Console.WriteLine("BEFORE HTTP REQUEST");
    }
  }

  public class MyWebSocketTransportServer : WebSocketTransportServer<MyRequestContext>
  {
    public MyWebSocketTransportServer(IAPIServer<MyRequestContext> apiServer) : base(apiServer, () => new MyWebSocketClientHandler()) { }
  }

  public class MyHTTPTransportServer : HTTPTransportServer<MyRequestContext>
  {
    public MyHTTPTransportServer(IAPIServer<MyRequestContext> apiServer) : base(apiServer, () => new MyHTTPClientHandler()) { }
  }

  public class MyTestAPIClient : InterfacesApp.Endpoints.BaseAPIClient
  {
    public MyTestAPIServer Srv { get; }

    public MyTestAPIClient(MyTestAPIServer srv)
    {
      Srv = srv;
    }

    public override string GenerateMessageId()
    {
      return KDRPC.Utils.CreateGuid();
    }

    protected override async Task SendRequest(byte[] request, string msgId, string endpointName, string methodName,
                                              int timeout,
                                              bool noExpire, bool noAck)
    {
      MyRequestContext ctx = new MyRequestContext();
      byte[] resp = await Srv.ProcessMessage(request, ctx);
      await Srv.ProcessMessage(resp);
    }
  }


  public class MyTestAPIServer : BaseAPIServer<MyRequestContext>
  {
    public MyTestAPIServer()
    {
      RegisterEndpoint(new MyEp1Endpoint());
    }
  }

  public static class App
  {
    public static MyTestAPIServer apiSrv;
    public static MyTestAPIClient client;

    public static async Task Main()
    {
      string ss = "1489f268-5df1-4c6e-8646-1d5181e5c9b5";
      var t = BaseConverters.UUID_to_proto(Guid.Parse(ss));
      Debug.Assert(t.Value[0] == 0x14);
      Debug.Assert(t.Value[1] == 0x89);
      Debug.Assert(t.Value[2] == 0xf2);
      Debug.Assert(t.Value[3] == 0x68);
      Debug.Assert(t.Value[4] == 0x5d);
      Debug.Assert(t.Value[5] == 0xf1);
      Debug.Assert(t.Value[6] == 0x4c);
      Debug.Assert(t.Value[7] == 0x6e);
      Debug.Assert(t.Value[8] == 0x86);
      Debug.Assert(t.Value[9] == 0x46);
      Debug.Assert(t.Value[10] == 0x1d);
      Debug.Assert(t.Value[11] == 0x51);
      Debug.Assert(t.Value[12] == 0x81);
      Debug.Assert(t.Value[13] == 0xe5);
      Debug.Assert(t.Value[14] == 0xc9);
      Debug.Assert(t.Value[15] == 0xb5);

      var g2 = BaseConverters.UUID_from_proto(t);
      
      Debug.Assert(ss == g2.ToString());

      apiSrv = new MyTestAPIServer();
      client = new MyTestAPIClient(apiSrv);

      var config = new LoggingConfiguration();
      var consoleTarget = new ColoredConsoleTarget();
      config.AddTarget("console", consoleTarget);

      config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, consoleTarget));

      consoleTarget.Layout = @"[${date:format=yyyy-MM-dd HH\:mm\:ss.fff}] [${logger}] ${message} ${parameters} ${exception:format=toString}";
      LogManager.Configuration = config;
      ILogger logProvider = LogManager.LogFactory.GetLogger("root");

      logProvider.Info("RUN");


      apiSrv.SetLogger(logProvider);
      var srv1 = new MyWebSocketTransportServer(apiSrv);
      srv1.SetLogger(logProvider);
      srv1.Run(5001);
      var srv2 = new MyHTTPTransportServer(apiSrv);
      srv2.SetLogger(logProvider);
      srv2.Run(5002);
      while (true) {
        var ev = new InterfacesApp.Event1() {
            Text = "A",
        };
        srv1.PublishEvent("test", ev);

        await Task.Delay(1000);
      }

      //      JsonFormatter fmt = new JsonFormatter(new JsonFormatter.Settings(true,
      //                                                                       TypeRegistry.FromMessages(
      //                                                                           proto_hub_pb2.ProtoHubReflection.Descriptor
      //                                                                               .MessageTypes)));
      //      Console.WriteLine(fmt.Format(msg));
    }
  }
}