﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InterfacesApp;
using InterfacesApp.BaseModels;
using InterfacesApp.Endpoints;
using KDRPC;

namespace Test
{
  public class MyEp1Endpoint : Ep1Endpoint<MyRequestContext>
  {
    public override async Task<Ep1EndpointMethod1Response> Method1(MyRequestContext ctx,
                                                                   Ep1EndpointMethod1Params _params)
    {
      if (_params.V1 == "error")
        throw new UserErrorException("bad");

      //      Console.WriteLine(ctx);
      //      Console.WriteLine(_params.V1);
      //      Console.WriteLine(_params.V4.Format());

      _params.Print();
      await Task.Delay(100);
      return new Ep1EndpointMethod1Response
      {
          R1 = "WQE" + (_params.V3 != null ? _params.V3.Length.ToString() : " no"),
          R4 = BaseTest.InternalEnum.InternalEnumValue1,
          R3 = MyEnum.EnumValue2,
          R5 = _params.V4,
      };
    }

    public override async Task<byte[]> Method2(MyRequestContext ctx, Ep1EndpointMethod2Params _params)
    {
      return new byte[] { 1, 2, 3 };
    }

    public override Task<MyEnum> Method3(MyRequestContext ctx, Ep1EndpointMethod3Params _params)
    {
      throw new NotImplementedException();
    }

    public override async Task<BaseTest.InternalEnum> Method4(MyRequestContext ctx, Ep1EndpointMethod4Params _params)
    {
      return InterfacesApp.Test.InternalEnum.InternalEnumValue1;
    }
  }
}