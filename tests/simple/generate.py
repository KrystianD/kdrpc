import sys
import os

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir + "/../py")

from generator.lib.generator import APIGenerator
from generator.lib.api_registry import APIRegistry


def main():
    reg = APIRegistry()
    reg.add_package(os.path.join(script_dir, "schema", "common"), "common")
    reg.add_package(os.path.join(script_dir, "schema", "app"), "app", depends=["common"])
    reg.add_package(os.path.join(script_dir, "schema", "app2"), "app2")
    reg.finalize()

    gen = APIGenerator(reg)
    out_dir = os.path.join(script_dir, "kdrpc_gen")
    gen.set_proto_out_dir(out_dir)
    gen.set_ts_out_dir(out_dir)
    gen.set_python_out_dir(out_dir)
    gen.set_csharp_out_dir(os.path.join(script_dir, "server", "kdrpc_gen"))
    gen.generate(["app", "app2"])


if __name__ == "__main__":
    main()
