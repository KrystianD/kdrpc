import asyncio
import sys
import os
from typing import Optional

import kdrpc

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(".")
sys.path.append(script_dir + "/../py")
sys.path.append(script_dir + "/kdrpc_gen/gen_py")

from kdrpc_gen import interfaces_app


class MyWebSocketClientHandler(kdrpc.WebSocketTransportClientHandler):

    async def PerformHandshake(self, ctx, handshake_data):
        return handshake_data["token"] == "KD"


class MyAPIEndpoint(interfaces_app.Ep1Endpoint):

    async def Method1(self, ctx, params: interfaces_app.Ep1EndpointMethod1Params) \
            -> Optional[interfaces_app.Ep1EndpointMethod1Response]:
        params.print()
        r = interfaces_app.Ep1EndpointMethod1Response()
        r.r1 = "ASD"
        r.r5 = params.v4
        return r

    async def Method2(self, ctx, params: interfaces_app.Ep1EndpointMethod2Params) -> Optional[bytes]:
        pass

    async def Method3(self, ctx, params: interfaces_app.Ep1EndpointMethod3Params) -> Optional[interfaces_app.MyEnum]:
        pass

    async def Method4(self, ctx, params: interfaces_app.Ep1EndpointMethod4Params) -> Optional[
        interfaces_app.Test.InternalEnum]:
        pass


class MyAPIServer(kdrpc.BaseAPIServer):
    def __init__(self):
        super().__init__()
        self.register(MyAPIEndpoint())


class MyWebSocketTransportServer(kdrpc.WebSocketTransportServer):
    def __init__(self, ioloop, api_server: MyAPIServer):
        super().__init__(ioloop, api_server)

    def create_context(self, *args):
        return None

    def create_client_handler(self):
        return MyWebSocketClientHandler()


async def main():
    api_server = MyAPIServer()
    srv = MyWebSocketTransportServer(asyncio.get_event_loop(), api_server)
    await srv.run(5001)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
    asyncio.get_event_loop().run_forever()
