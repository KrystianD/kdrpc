import { ChangeDetectorRef, Component } from '@angular/core';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/delay';
import * as kdrpc from 'kdrpc';
import { InterfacesApp } from '../kdrpc_gen';
import { List } from "kdlib";
import { Converters_app } from './../kdrpc_gen/gen_ts/app/converters';
import moment from 'moment';
import { UUID } from "kdrpc";
// import proto_app_pb2_json from '../kdrpc_gen/gen_ts/app/proto';
// let proto_app_root = protobuf.Root.fromJSON(proto_app_pb2_json);
import Decimal from 'decimal.js';

const timeout = ms => new Promise(res => setTimeout(res, ms));

class MyRequestContext implements kdrpc.IAPIRequestContext {
}

class MyMockAuthEndpoint extends InterfacesApp.Endpoints.Ep1Endpoint<MyRequestContext> {
  public async Method1(ctx: MyRequestContext, params: InterfacesApp.BaseModels.Ep1EndpointMethod1Params): Promise<InterfacesApp.BaseModels.Ep1EndpointMethod1Response> {
    console.log("Method1");
    await timeout(500);
    let r = new InterfacesApp.BaseModels.Ep1EndpointMethod1Response();
    r.R1 = "ASD";
    r.R2 = 2;
    r.R5 = params.V4;
    return r;
  }

  public async Method2(ctx: MyRequestContext, params: InterfacesApp.BaseModels.Ep1EndpointMethod2Params): Promise<Uint8Array> {
    console.log("Method2");
    return new Uint8Array([1, 2, 3]);
  }

  public async Method3(ctx: MyRequestContext, params: InterfacesApp.BaseModels.Ep1EndpointMethod3Params): Promise<any> {
    console.log("Method3");
    return InterfacesApp.MyEnum.EnumValue1;
  }

  public async Method4(ctx: MyRequestContext, params: InterfacesApp.BaseModels.Ep1EndpointMethod4Params): Promise<any> {
    console.log("Method4");
    return InterfacesApp.Test.InternalEnum.InternalEnumValue1;
  }
}

class MyMockAPIServer extends kdrpc.BaseAPIServer<MyRequestContext> {
  constructor() {
    super();
    this.RegisterEndpoint(new MyMockAuthEndpoint());
  }
}

class MyMockAPIClient extends InterfacesApp.Endpoints.BaseAPIClient {
  constructor(private server: kdrpc.BaseAPIServer<MyRequestContext>) {
    super();
  }

  public GenerateMessageId(): string {
    return "n/a";
  }

  protected SendRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string, timeout: number, noExpire: boolean, noAck: boolean): Observable<void> {
    return this.server.ProcessMessage(request, null)
      .map(apiResponseBytes => {
        setTimeout(() => {
          this.ProcessMessage(apiResponseBytes);
        }, 10);
      });
  }
}

class MyWebSocketAPIClient extends InterfacesApp.Endpoints.BaseAPIClient {
  public ws: kdrpc.WebSocketTransportClient;
  private packetNum = 0;

  constructor(url: string) {
    super();
    this.ws = new kdrpc.WebSocketTransportClient(this, url);

    this.ws.ProcessHandshakeResponseHandler = (handshakeResponse: any) => {
      // console.log("ProcessHandshakeResponse", handshakeResponse);
      if (handshakeResponse !== "OK") {
        // throw new Error("unauthorized");
      }
    };

    this.ws.OnFatalErrorHandler = (error: Error) => {
      console.log("OnFatalError", error);
    };
  }

  public get State() {
    return this.ws.State;
  }

  public Connect(handshakeData: any) {
    this.ws.Connect(handshakeData);
  }

  public Disconnect() {
    this.ws.Disconnect();
  }

  public GenerateMessageId(): string {
    let v = this.packetNum++;
    return `${v}`;
  }

  public BeforeRequest(msgId: string, endpointName: string, methodName: string, paramsProto: protobuf.Message<any>, params: kdrpc.APIModel): void {
    console.debug(`%c[${msgId}] -> ${endpointName}/${methodName}`, "color: green", params.ToObject());
  }

  public AfterRequest(msgId: string, endpointName: string, methodName: string, responseProto: protobuf.Message<any>, response: kdrpc.APIModel): void {
    console.debug(`%c[${msgId}] ${endpointName}/${methodName} ->`, "color: red", response.ToObject());
  }

  protected SendRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string, timeout: number, noExpire: boolean, noAck: boolean): Observable<void> {
    return this.ws.SendAPIRequest(request, msgId, endpointName, methodName, timeout, noExpire, noAck);
  }
}

class MyHTTPAPIClient extends InterfacesApp.Endpoints.BaseAPIClient {
  private transport: kdrpc.HTTPTransportClient;
  private id = 0;

  constructor(url: string) {
    super();
    this.transport = new kdrpc.HTTPTransportClient(this, url);
  }

  public GenerateMessageId(): string {
    this.id++;
    return `${this.id}`;
  }

  public BeforeRequest(msgId: string, endpointName: string, methodName: string, paramsProto: protobuf.Message<any>, params: kdrpc.APIModel): void {
    console.debug(`%c[${msgId}] -> ${endpointName}/${methodName}`, "color: green", params.ToObject());
  }

  public AfterRequest(msgId: string, endpointName: string, methodName: string, responseProto: protobuf.Message<any>, response: kdrpc.APIModel): void {
    console.debug(`%c[${msgId}] ${endpointName}/${methodName} ->`, "color: red", response.ToObject());
  }

  protected OnError(error: Error, msgId: string, endpointName: string, methodName: string) {
    console.log("MY ERROR", error);
  }

  protected SendRequest(request: Uint8Array, msgId: string, endpointName: string, methodName: string, timeout: number, noExpire: boolean, noAck: boolean): Observable<void> {
    return this.transport.SendAPIRequest(request, msgId, endpointName, methodName, timeout, noExpire, noAck);
  }
}

let v = new InterfacesApp.Test();
v.On = true;
v.Dec = new Decimal("12.34");
v.Enum2 = InterfacesApp.Test.InternalEnum.InternalEnumValue2;
v.MapTest = new Map<number, List<number>>();
v.MapTest.set(3, new List<number>(1, 2, 3));
v.Num = 41;
let st1 = new InterfacesApp.Test();
st1.Text = "A1";
v.ListRec = new List<InterfacesApp.Test>(st1);

v.Date = moment.utc("2017-01-01");
v.Datetime = moment.utc("2017-01-01 04:12");
v.Time = moment.utc("2017-01-01 12:34");
v.Text = "beftext";

v.Bson = {
  a: 2,
};

v.Oid = UUID.fromString("12345678-1234-5678-1234-567812345678");
console.log(v.ToObject());

const uuid1 = UUID.fromString("12345678-1234-5678-1234-567812345678");
const uuid2 = UUID.fromString("12345678-1234-5678-1234-567812345678");

console.log("random uuids", UUID.createRandom().toString(), UUID.createRandom().toString(), UUID.createRandom().toString())

// console.log("uuid1.valueOf() == uuid2.valueOf()", uuid1.valueOf() == uuid2.valueOf());
// console.log("uuid1 == uuid2", uuid1 == uuid2);
// console.log("uuid1 === uuid2", uuid1 === uuid2);

// console.log("before", "x.Date", v.Date.format());
// console.log("before", "x.ListRec[0].Text", v.ListRec[0].Text);
console.log("v", v);
let y = v.Clone();
console.log("cloned", y);
// console.log("before", "y.Date", y.Date.format());
// console.log("before", "y.ListRec[0].Text", y.ListRec[0].Text);
// y.Text = "new value";
// y.ListRec[0].Text = "new";
// y.Date.hour(5);
// console.log("after", "x.Date", v.Date.format());
// console.log("before", "x.ListRec[0].Text", v.ListRec[0].Text);
// console.log("after", "y.Date", y.Date.format());
// console.log("before", "y.ListRec[0].Text", y.ListRec[0].Text);


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  public packetData = new Map<number, any>();
  public remoteClient = new MyWebSocketAPIClient("ws://127.0.0.1:5001");
  public remoteHttpClient = new MyHTTPAPIClient("http://127.0.0.1:5002");
  public mockServer = new MyMockAPIServer();
  public mockClient: MyMockAPIClient;

  private num = 0;

  constructor(private ref: ChangeDetectorRef) {
    let num = 0;

    // this.remoteClient.Connect({
    //   "token": "KD",
    // });

    this.remoteClient.EventListener.subscribe(ev => {
      console.log("NEW EVENT", ev);
    });

    this.mockClient = new MyMockAPIClient(this.mockServer);


    let oo = Converters_app.Map__Int32__List__Int32_to_proto(v.MapTest);
    let b = Converters_app.Map__Int32__List__Int32_from_proto(oo);
    b.get(3).kmap(x => 123);

  }

  get state() {
    return this.remoteClient.State;
  }

  get hstate() {
    return this.remoteClient.ws.getHighlevelStateSubject().getValue();
  }

  get packetDataView() {
    let items = [];
    this.packetData.forEach((value, key) => {
      items.push(`${key} ${value}`);
    });
    return items;
  }

  public startTimer() {
    Observable.timer(1000, 2000)
    // Observable.of(null).delay(2000)
      .subscribe(x => {
        this.sendRequest();
      });
  }

  public sendRequest() {

    this.num++;

    let curNum = this.num;

    this.packetData.set(curNum, "...");

    try {
      this.remoteClient.Ep1Endpoint.Builders
        .Method1
        .SetV1("ABCDEF")
        .SetV4(v)
        .Build(10000)
        .subscribe(x => {
            console.log("SRV Login1", x);
            this.packetData.set(curNum, "OK");
            this.ref.detectChanges();

          },
          error => {
            this.packetData.set(curNum, error);
          });
    }
    catch (e) {
      console.log("WE", e);
    }
  }

  public connect(token) {
    this.remoteClient.Connect({ "token": token });
  }

  public disconnect() {
    this.remoteClient.Disconnect();
  }

  public http() {
    this.remoteHttpClient.Ep1Endpoint.Method1("W", "E", null, v).subscribe(x => {
    });

    this.remoteHttpClient.Ep1Endpoint.Method4().subscribe(x => {
    });
  }

  public httpTimeout() {
    this.remoteHttpClient.Ep1Endpoint.Method1("W", "E", null, v, 20).subscribe(x => {
      console.log("WER", x);
    });
  }

  public httpError() {
    this.remoteHttpClient.Ep1Endpoint.Method1("error", "E", null, v)
      .subscribe(
        x => {
          console.log("WER", x);
        },
        x => {
          console.error("USER ERROR", x.message);
        });
  }

  public mock() {
    console.log("MOCK");
    this.mockClient.Ep1Endpoint.Method1("error", "E", null, v, 1000)
      .subscribe(
        x => {
          console.log("Mock response", x);
        },
        x => {
          console.error("USER ERROR", x.message);
        });
  }
}
