import sys
import os

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir + "/../py")

from generator.generator import APIGenerator


def main():
    gen = APIGenerator()

    out_dir = os.path.join(script_dir, "kdrpc_gen")

    gen.set_proto_out_dir(out_dir)
    # gen.set_ts_out_dir(out_dir)
    gen.set_python_out_dir(out_dir)

    gen.add_package(os.path.join(script_dir, "schema", "common"), "common")
    gen.add_package(os.path.join(script_dir, "schema", "app"), "app", depends=["common"])
    gen.generate()


if __name__ == "__main__":
    main()
