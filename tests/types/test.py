import sys
import unittest

sys.path.append(".")
sys.path.append("kdrpc_gen/gen_py")

from .kdrpc_gen import interfaces_app, interfaces_common


class ParsingTest(unittest.TestCase):
    def test_enum(self):
        m = interfaces_app.AppModel1()
        m.c = interfaces_app.AppStatusEnum.Val1
        m.d = interfaces_app.AppModel1.AppNestedStatusEnum.Val1

        p = interfaces_app.model_to_proto(m)
        m2 = interfaces_app.model_from_proto(p, type(m))

        self.assertEqual(repr(m), repr(m2))
        self.assertEqual(m, m2)

    def test_enum_nested(self):
        m = interfaces_app.AppModel2()
        m.a = interfaces_app.AppModel1.AppNestedStatusEnum.Val1
        m.b = interfaces_app.AppNamespace1.AppNamespace2.AppNamespaceEnum.Val2
        m.c = interfaces_app.AppNamespace1.AppNamespace2.AppNamespaceModel1.AppNamespaceNestedEnum.Val1

        p = interfaces_app.model_to_proto(m)
        m2 = interfaces_app.model_from_proto(p, type(m))

        self.assertEqual(repr(m), repr(m2))
        self.assertEqual(m, m2)

    def test_namespace(self):
        m = interfaces_app.AppNamespace1.AppNamespace2.AppNamespaceModel1()

        p = interfaces_app.model_to_proto(m)
        m2 = interfaces_app.model_from_proto(p, type(m))

        self.assertEqual(repr(m), repr(m2))
        self.assertEqual(m, m2)

    def test_different_package(self):
        cm = interfaces_common.CommonModel1()
        cm.id = 4
        m = interfaces_app.AppModel1()
        m.a = 2
        m.b = cm

        p = interfaces_app.model_to_proto(m)
        m2 = interfaces_app.model_from_proto(p, interfaces_app.AppModel1)

        self.assertEqual(repr(m), repr(m2))
        self.assertEqual(m, m2)

    def test_list_map(self):
        m = interfaces_app.AppModel1()
        m.a = None
        m.e = [2, 4, 6543210]
        m.f = {1: "AA", 2: "BB"}

        p = interfaces_app.model_to_proto(m)
        m2 = interfaces_app.model_from_proto(p, type(m))

        self.assertEqual(repr(m), repr(m2))
        self.assertEqual(m, m2)

        self.assertEqual(m2.a, None)
        self.assertEqual(m2.e, [2, 4, 6543210])
        self.assertEqual(m2.f, {1: "AA", 2: "BB"})


if __name__ == '__main__':
    unittest.main()
